import Vue from 'vue';
import App from './App.vue';
import router from './route';
import store from './store';
import vuetify from './plugins/vuetify';
import { Component } from 'vue-property-decorator';
import '@sweetalert2/theme-material-ui/material-ui.css';
import moment from 'moment';
import InfiniteLoading from 'vue-infinite-loading';
Component.registerHooks(['validations']);
Vue.config.productionTip = false;
Vue.use(InfiniteLoading, {
  props: {
    spinner: 'waveDots',
    /* other props need to configure */
  },
  slots: {
    noMore: 'No hay mas elementos a cargar', // you can pass a string value
  },
});
moment.locale('es');
new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
