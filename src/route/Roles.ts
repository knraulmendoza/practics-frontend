export enum Roles {
    ADMIN = 1,
    COORDINATOR = 2,
    TEACHER_PRACTICT = 3,
    TEACHER = 4,
    STUDENT = 5,
}
