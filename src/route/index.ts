import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import paths from './paths';
import { authSer } from '@/services/authService';
import { globalSer } from '@/services/globalService';
import { Roles } from './Roles';
import store from '../store';
Vue.use(VueRouter);
const router = new VueRouter({
  base: process.env.BASE_URL,
  mode: 'history',
  routes: paths as RouteConfig[],
});
router.beforeEach(async (to, from, next) => {
  document.title = 'Practics';
  store.commit('setLoading', { loading: true, msg: '' });
  const { authorize } = to.meta as any;
  const currentUser = await authSer.getUser();
  // const person = await personSer.get();
  const user = globalSer.getCurrentUser();
  if (to.path === '/login' || to.path === '/') {
    if (currentUser.data !== undefined && currentUser.data.state) {
      const careers = JSON.parse(localStorage.getItem('careers') as string) ?? [];
      const roles = JSON.parse(localStorage.getItem('roles') as string) ?? [];
      return roles.length < 2 || careers.length < 2 ? next('/dashborad') : next('/selectRole');
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('careers');
      localStorage.removeItem('roles');
    }
  } else {
    if (!currentUser.data.state) {
      localStorage.removeItem('token');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('careers');
      localStorage.removeItem('roles');
      return next('/');
    } else {
      if (currentUser.data.data.person == null && user.rol !== Roles.ADMIN) {
        authSer.logout();
        return next('/');
      }
      const careers = JSON.parse(localStorage.getItem('careers') as string) ?? [];
      const roles = JSON.parse(localStorage.getItem('roles') as string) ?? [];
      if (to.path === '/selectRole') {
        if (roles.length < 2 && careers.length < 2 && user.rol !== Roles.ADMIN) {
          return next('/dashboard');
        }
      } else {
        if (to.path === '/changePassword') {
          if (currentUser.data.data.user.state_password === '1') {
            return next('/dashboard');
          }
        } else {
          if (currentUser.data.data.user.state_password === '0') {
            return next('/changePassword');
          }
          if ((roles.length > 1 || careers.length > 1) && (!user.rol || !user.career)) {
            return next('/selectRole');
          }
          if (authorize.length !== 0) {
            if (!authorize.includes(user.rol)) {
              return next({ path: '/dashboard' });
            }
          }
        }
      }
    }
  }

  next();
});

router.afterEach(() => {
  // Complete the animation of the route progress bar.
  store.commit('setLoading', { loading: false, msg: '' });
});
export default router;
