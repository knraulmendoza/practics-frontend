import dashboard from '@/components';
import StudentView from '@/views/students';
import { Roles } from '@/route/Roles';
import SelectRole from '@/views/SelectRole';
import CheckView from '@/views/check';
import GroupStudent from '@/views/groupStudent';
import SubjectView from '@/views/subject';
import Escenario from '@/views/escenario';
import TitleView from '@/views/title';
import ModuleView from '@/views/module';
import TeacherView from '@/views/teacher';
import CoordinatorView from '@/views/coordinator';
import QuestionView from '@/views/question';
import AttentionView from '@/views/attention/AttentionView';
import LoginView from '@/views/login';
import CareerView from '@/views/career';
import EventsView from '@/views/events';
import EventView from '@/views/events/event';
import AssignmentView from '@/views/assignment';
import RotationView from '@/views/rotation';
import ChangePassword from '@/views/changePassword';
import ReportsView from '@/views/reports';
import HomeView from '@/views/home';
import PageView from '@/views/pages';
import ConfigView from '@/views/config';
import PatientView from '@/views/patient';

export default [
  {
    path: '*',
    meta: {
      authorize: [],
    },
    redirect: {
      path: '/dashboard',
    },
  },
  {
    path: '/groupStudent',
    component: GroupStudent,
  },
  {
    path: '/selectRole',
    meta: {
      authorize: [],
    },
    component: SelectRole,
  },
  {
    path: '/changePassword',
    meta: {
      authorize: [],
    },
    component: ChangePassword,
  },
  // This  allows you to have pages apart of the app but no rendered inside the dash
  {
    path: '/',
    meta: {
      authorize: [],
    },
    component: HomeView,
    children: [
      {
        path: 'home',
        meta: {
          authorize: [],
        },
        component: HomeView,
      },
    ],
  },
  {
    path: '/login',
    meta: {
      authorize: [],
    },
    component: LoginView,
  },
  {
    path: '/dashboard',
    component: dashboard,
    meta: {
      authorize: [],
    },
    children: [
      {
        path: 'reports',
        name: 'reports',
        meta: {
          name: 'Reportes',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: ReportsView,
      },
      {
        path: 'teacher',
        name: 'teacher',
        meta: {
          name: 'Profesores',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: TeacherView,
      },
      {
        path: 'coordinador',
        name: 'coordinador',
        meta: {
          name: 'Coordinadores',
          authorize: [Roles.ADMIN],
        },
        component: CoordinatorView,
      },
      {
        path: 'modulo',
        name: 'modulo',
        meta: {
          name: 'Módulos',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: ModuleView,
      },
      {
        path: 'atencion',
        name: 'atencion',
        meta: {
          name: 'Pacientes',
          authorize: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER_PRACTICT, Roles.COORDINATOR],
        },
        component: AttentionView,
      },
      {
        path: 'page',
        name: 'page',
        meta: {
          name: 'Página',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: PageView,
      },
      {
        path: 'configuracion',
        name: 'configuracion',
        meta: {
          name: 'Configuración del sistema',
          authorize: [Roles.ADMIN],
        },
        component: ConfigView,
      },
      {
        path: 'estudiante',
        name: 'estudiante',
        meta: {
          name: 'Estudiantes',
          authorize: [Roles.ADMIN, Roles.COORDINATOR, Roles.TEACHER],
        },
        component: StudentView,
      },
      {
        path: 'asignacion',
        name: 'asignacion',
        meta: {
          name: 'Asignaciones',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: AssignmentView,
      },
      {
        path: 'subject',
        name: 'asignatura',
        meta: {
          name: 'Asignaturas',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
          // authorize: [],
        },
        component: SubjectView,
      },
      {
        path: 'events',
        name: 'events',
        meta: {
          name: 'Eventos',
          authorize: [Roles.ADMIN, Roles.COORDINATOR, Roles.STUDENT, Roles.TEACHER_PRACTICT, Roles.TEACHER],
          // authorize: [],
        },
        component: EventsView,
      },
      {
        path: 'event/:id',
        name: 'event',
        meta: {
          name: 'Evento',
          authorize: [Roles.ADMIN, Roles.COORDINATOR, Roles.STUDENT],
          // authorize: [],
        },
        component: EventView,
      },
      {
        path: 'career',
        name: 'career',
        meta: {
          name: 'Carreras',
          authorize: [Roles.ADMIN],
          // authorize: [],
        },
        component: CareerView,
      },
      {
        path: 'preguntas',
        name: 'preguntas',
        meta: {
          name: 'Preguntas',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: QuestionView,
      },
      {
        path: 'chequeo',
        name: 'chequeo',
        meta: {
          name: 'Chequeos',
          authorize: [Roles.ADMIN, Roles.COORDINATOR, Roles.TEACHER_PRACTICT],
        },
        component: CheckView,
      },
      {
        path: 'rotation',
        name: 'rotation',
        meta: {
          name: 'Rotaciones',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: RotationView,
      },
      {
        path: 'grupo-estudiante',
        name: 'grupo-estudiante',
        meta: {
          name: 'Grupos de estudiantes',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: GroupStudent,
      },
      {
        path: 'escenarios',
        name: 'escenarios',
        meta: {
          name: 'Escenarios',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: Escenario,
      },
      {
        path: 'titulos',
        name: 'titulos',
        meta: {
          name: 'Títulos',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: TitleView,
      },
      {
        path: 'patient',
        meta: {
          name: 'Paciente view',
          authorize: [Roles.ADMIN, Roles.COORDINATOR],
        },
        component: PatientView,
      },
    ],
  },
];
