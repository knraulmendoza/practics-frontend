import Vue from 'vue';
import template from './TitleView.vue';
import { Component } from 'vue-property-decorator';
import { ITeacher } from '@/interfaces/interfaces';
import { teacherService } from '@/services/teacherServices';
import { Title } from '@/domain/entities/title/Title';
import { globalSer } from '@/services/globalService';
import { titleService } from '@/services/titleService';
import { Status } from '@/helpers/Status';
import Loading from '@/components/loading.vue';

@Component({
  name: 'titleView',
  mixins: [template],
  components: { Loading },
})
export default class TitleView extends Vue {
  public titles: Title[] = [];
  public title: Title = new Title();
  public editedIndex = -1;
  public dialog = false;
  public dialogAsignar = false;
  public valid = true;
  public lazy = false;

  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nuevo Título' : 'Editar Título';
  }

  public search = '';
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
      value: 'num',
    },
    { text: 'Nombre', value: 'name' },
    { text: 'Descripción', value: 'description' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public teachers: ITeacher[] = [];

  public formTitleValid = {
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    description: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  public async initialize(): Promise<void> {
    this.teachers = (await teacherService.getPeople()).data.data as ITeacher[];
  }

  public test(): void {
    this.$store.commit('setLoading', { loading: true, msg: 'Cargando títulos' });
    setTimeout(() => {
      this.$store.commit('setLoading', { loading: false, msg: '' });
    }, 3000);
  }

  public close(): void {
    this.dialog = false;
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    if (this.editedIndex === -1) {
      const resp = await titleService.post(this.title);
      if (resp.data.state) {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se registro el título correctamente');
        this.titles.push(this.title);
        this.close();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    } else {
      const resp = await titleService.put(this.title);
      if (resp.data.state) {
        // await this.sho();
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se actualizo el título correctamente');
        this.close();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    }
  }
}
