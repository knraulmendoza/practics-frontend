import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './StudentView.vue';
import { studentService } from '@/services/studentService';
import { IStudentResponse } from '@/interfaces/InterfaceResponse';
import { Student } from '@/domain/entities/student/Student';
import Loading from '@/components/loading.vue';
import { Roles } from '@/route/Roles';
import { ISelect } from '@/interfaces/interfaces';
import { personSer } from '@/services/personService';
import student from '@/components/student/student.vue';
import { academicPeriodService } from '@/services/academicPeriodService';
import { practicScenarioService } from '@/services/practicScenarioService';
import { globalSer } from '@/services/globalService';
import { Status } from '@/helpers/Status';
export interface Isearch {
  type: string;
  value: string;
  person_type: Roles;
}
@Component({
  name: 'estudiantesView',
  mixins: [template],
  components: { Loading, student },
})
export default class StudentView extends Vue {
  public valid = true;
  public lazy = false;
  public loading = true;
  public dialogData = false;
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalStudents = 0;
  public student = new Student();
  public dataPractict: any = null;
  public headers = [
    { text: 'Nombre', value: 'person.fullName' },
    { text: 'Dirección', value: 'person.address' },
    { text: 'Teléfono', value: 'person.phone' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public peopleTypes: ISelect[] = [
    { text: 'Documento', value: 'document' },
    { text: 'Nombre', value: 'name' },
    { text: 'Escenario de práctica', value: 'scenario' },
  ];
  public scenarios: ISelect[] = [];
  public academicPeriods: ISelect[] = [];
  public academicPeriod: any = null;
  public search: Isearch = {} as Isearch;
  public stateSearch = false;
  public students: Student[] = [];

  public async created(): Promise<void> {
    this.academicPeriods = await academicPeriodService.getAll();
    this.scenarios = await practicScenarioService.getAllSelect();
    this.showStudents();
  }

  public async showStudents(): Promise<void> {
    this.students = [];
    this.loading = true;
    const resp = await studentService.getAll();
    if (resp.data.state) {
      this.students = (resp.data.data.students as IStudentResponse[]).map((studentAux) => new Student(studentAux));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalStudents = resp.data.data.total;
    }
    this.loading = false;
  }

  public async dataPerson( studentAux, academicPeriod ): Promise<void> {
    try {
      if (studentAux !== undefined && studentAux.id !== undefined ) {
        this.student = studentAux;
      }
      this.dataPractict = null;
      this.$store.commit('setLoading', { loading: true, msg: 'Cargando datos del estudiante' });
      const resp = await studentService.getFullData(this.student.id, academicPeriod);
      if (resp.data.state) {
        this.dialogData = true;
        this.dataPractict = resp.data.data.assignmentClass;
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error('No existe datos del estudiante para este periodo académico');
        }
      }
      this.$store.commit('setLoading', { loading: false, msg: '' });
    } catch (error) {
      console.log(error);
    }
  }

  public getAcademicPeriod({ academicPeriod }: { academicPeriod: any }): void {
    this.dataPerson(this.student, academicPeriod );
  }

  public closeDialog(): void {
    this.dialogData = false;
    this.dataPractict = null;
    this.student = new Student();
  }

  public next(): void {
    if (!this.stateSearch) {
      this.hablePaginate();
    }
  }
  public previous(): void {
    if (!this.stateSearch) {
      this.hablePaginate();
    }
  }
  public async hablePaginate(): Promise<void> {
    this.students = [];
    this.loading = true;
    const resp = await studentService.getAll(this.page);
    if (resp.data.state) {
      this.students = (resp.data.data.students as IStudentResponse[]).map((studentAux) => new Student(studentAux));
    }
    this.loading = false;
  }

  public async searchStudent(): Promise<void> {
    this.students = [];
    this.loading = true;
    this.pageCount = 0;
    this.page = 1;
    if (this.search.type === 'scenario') {
      const resp = await studentService.studentsByScenario(this.search.value as unknown as number, this.page);
      if (resp.data.state) {
        this.students = (resp.data.data.students as IStudentResponse[]).map((studentAux) => new Student(studentAux));
      }
    } else {
      this.search.person_type = Roles.STUDENT;
      const resp = await personSer.search(this.search);
      if (resp.data.state) {
        this.students = (resp.data.data.people as IStudentResponse[]).map((studentAux) => new Student(studentAux));
      }
    }
    this.perPage = 8;
    this.pageCount = Math.ceil(this.students.length / this.perPage);
    this.totalStudents = this.students.length;
    this.loading = false;
    this.stateSearch = true;
  }

  public async clearSearch(): Promise<void> {
    this.stateSearch = false;
    this.search = {} as Isearch;
    await this.showStudents();
  }
}
