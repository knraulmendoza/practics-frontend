import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './subjectView.vue';
import { SubjectSer } from '@/services/subjectService';
import { globalSer } from '@/services/globalService';
import { Subject } from '@/domain/entities/subject/Subject';
import { Status } from '@/helpers/Status';
import { ISubject, ISubjectUpc } from '@/interfaces/interfaces';
import Loading from '@/components/loading.vue';
import { Module } from '@/domain/entities/module/Module';
import { IModuleResponse } from '@/interfaces/InterfaceResponse';
import Snackbar from '@/components/Snackbar.vue';
@Component({
  name: 'subjectView',
  mixins: [template],
  components: { Loading, Snackbar },
})
export default class SubjectView extends Vue {
  public search = '';
  public searchSubjectText = '';
  public editedIndex = -1;
  public subject: Subject = new Subject();
  public subjects: Subject[] = [];
  public subjectsAuxUpc: any[] = [];
  public subjectsUpc: any[] = [];
  public dialog = false;
  public dialogModule = false;
  public loading = true;
  public valid = false;
  public lazy = false;
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
    },
    { text: 'Código', value: 'code' },
    { text: 'Asignatura', value: 'name' },
    { text: 'Módulos', value: 'module' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public modules: Module[] = [];
  public validateSubject = {
    code: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nueva Asignatura' : 'Editar Asignatura';
  }
  get subjectsUpcAux(): any[] {
    return this.subjectsUpc.filter((item) => {
      return item.subject.name.toLowerCase().includes(this.searchSubjectText.toLowerCase());
    });
  }

  public async created(): Promise<void> {
    this.showSubjects();
  }

  public close(): void {
    this.dialog = false;
    this.subject = new Subject();
    (this.$refs.form as any).reset();
  }

  public async comparatorServer(): Promise<void> {
    const subjAux: Subject[] = [];
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const subjectsAux = await SubjectSer.getAllUpc();
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (subjectsAux !== null && subjectsAux !== undefined) {
      if (subjectsAux.data.length) {
        this.subjectsUpc = subjectsAux.data.map((subject) => {
          return { subject: new Subject().fromJson(subject), check: false };
        });
        this.subjectsUpc.forEach((subject) => {
          if (!this.subjects.filter((subjectAux) => subjectAux.code === subject.subject.code).length) {
            subjAux.push(subject);
          }
        });
        this.subjectsUpc = subjAux;
        this.dialog = true;
      } else {
        this.$store.commit('setSnackbar', { msg: 'Este programa no tiene asignatura', color: 'red' });
        setTimeout(() => {
          this.$store.commit('setSnackbar', { msg: '', color: '' });
        }, 4000);
      }
    } else {
      globalSer.error('El servidor de la universidad esta caido');
    }
  }

  /**
   * saveSubject
   */
  public async saveSubject(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    await this.subjectsUpc
      .filter((subject) => subject.check)
      .reduce(async (promise, subject) => {
        await promise;
        const res = await SubjectSer.save(subject.subject);
        if (res.data.state) {
          // this.showSubjects();
          this.subjects.push(subject.subject);
        } else {
          if (res.status === Status.REQUESTVALIDATIONS) {
            globalSer.errorValidation(res.dataError);
          } else {
            globalSer.error(res.data.message);
          }
        }
      }, Promise.resolve());
    this.$store.commit('setLoading', { loading: false, msg: '' });
    this.close();
    globalSer.success('La asignatura se registro de manera exitosa');
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Espere por favor....' });
    const res = await SubjectSer.update(this.subject);
    if (res.data.state) {
      this.showSubjects();
      globalSer.success('La asignatura se modifico de manera exitosa');
    } else {
      if (res.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(res.dataError);
      } else {
        globalSer.error(res.data.message);
      }
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async showSubjects(): Promise<void> {
    this.loading = true;
    const subject = await SubjectSer.getSubjects();
    if (subject.data.state) {
      const subjectsAux = subject.data.data.subjects as ISubject[];
      this.subjects = subjectsAux.map((subjectAux) => new Subject(subjectAux));
    }
    this.loading = false;
  }

  /*public async edit(item: any): Promise<void> {
    this.editedIndex = this.subjects.indexOf(item);
    this.subject = item;
    this.dialog = true;
  }*/

  public edit(subject: Subject): void {
    this.editedIndex = 1;
    // this.dialog = true;
    const sub = Object.assign<unknown, ISubject>({}, subject.toJson());
    this.subject = new Subject(sub);
    this.searchSubject(this.subject);
  }

  public async searchSubject(item: Subject): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const subjectsAux = await SubjectSer.getAsignature(item.code);
    if (subjectsAux !== null && subjectsAux !== undefined) {
      const auxSub = subjectsAux.data as ISubjectUpc;
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (this.subject === null && this.subject === undefined) {
        globalSer.error('No se encontro la asignatura en los servidores de la Universidad');
      } else {
        this.subject = new Subject().fromJson(auxSub);
        console.log(this.subject);

        this.save();
      }
    } else {
      this.$store.commit('setLoading', { loading: false, msg: '' });
      globalSer.error('El servidor de la universidad esta caido');
    }
  }

  public async getModules(subject: string): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const moduleSer = await SubjectSer.getModuleBySubject(subject);
    if (moduleSer.data.state) {
      const modulesAux = moduleSer.data.data.modules as IModuleResponse[];
      this.modules = modulesAux.map((moduleAux) => new Module(moduleAux));
      if (this.modules.length > 0) {
        this.dialogModule = true;
      } else {
        globalSer.error('No hay módulos en esta asignatura');
      }
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
}
