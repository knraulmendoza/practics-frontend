import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './EventsView.vue';
import EventCard from '@/components/EventCard.vue';
import { EventNew } from '@/domain/entities/event/Event';
import { eventService } from '@/services/eventService';
import { IEvent } from '@/interfaces/interfaces';
import { globalSer } from '@/services/globalService';
import { Roles } from '@/route/Roles';
import InfiniteLoading from 'vue-infinite-loading';
import { VueEditor } from 'vue2-editor';
@Component({
  name: 'eventsVue',
  mixins: [template],
  components: { EventCard, InfiniteLoading, VueEditor },
})
export default class EventsView extends Vue {
  public events: EventNew[] = [];
  public event: EventNew = new EventNew();
  public dialog = false;
  public dialogView = false;
  public timePicker = false;
  public img = '';
  public datePicker = false;
  public lazy = false;
  public valid = false;
  public page = 1;
  public formEventValid = {
    title: [(v: any) => !!v || 'Este campo es obligatorio'],
    date: [(v: any) => !!v || 'Este campo es obligatorio'],
    hour: [(v: any) => !!v || 'Este campo es obligatorio'],
    address: [(v: any) => !!v || 'Este campo es obligatorio'],
  };
  public get currentRol(): boolean {
    const rol = globalSer.getCurrentUser().rol;
    return rol === Roles.ADMIN || rol === Roles.COORDINATOR || rol === Roles.TEACHER;
  }

  public async infiniteEvents(state?: any): Promise<void> {
    const resp = await eventService.getAll({ page: this.page });
    if ((resp.data.data.events as IEvent[]).length) {
      this.page += 1;
      (resp.data.data.events as IEvent[]).forEach((event) => this.events.push(new EventNew(event)));
      state.loaded();
    } else {
      state.complete();
    }
  }

  public closePicker(): void {
    this.datePicker = false;
    this.event.hour = '';
  }

  public closeTimePicker(): void {
    this.timePicker = false;
    this.event.hour = '';
  }

  public close(): void {
    this.dialog = false;
    this.img = '';
    this.event = new EventNew();
    (this.$refs.formEvent as any).reset();
  }

  public async save(): Promise<void> {
    if (this.valid && this.event.description !== undefined && this.event.description !== '') {
      const formData = new FormData();
      formData.append('title', this.event.title);
      formData.append('description', this.event.description);
      formData.append('address', this.event.address);
      formData.append('date', this.event.date.toString());
      formData.append('hour', this.event.hour);
      formData.append('image', this.event.image);
      this.$store.commit('setLoading', { loading: true, msg: 'Guardando evento' });
      const resp = await eventService.post(formData);

      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (resp.data.state) {
        globalSer.success('Evento Guardado');
        this.page = 1;
        this.events = [];
        this.close();
      } else {
        globalSer.error('Error al registrar el evento');
      }
    } else {
      globalSer.error('Los datos ingresados no son validos');
    }
  }

  public getImagen(ee: string | Blob): void {
    if (ee) {
      this.event.image = ee as string;
      const img = new FileReader();
      img.onload = (e) => {
        this.img = e.target?.result as string;
      };
      img.readAsDataURL(ee as Blob);
    }
  }
}
