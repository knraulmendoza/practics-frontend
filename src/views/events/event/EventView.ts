import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './EventView.vue';
import { EventNew } from '@/domain/entities/event/Event';
import Loading from '@/components/loading.vue';
import { eventService } from '@/services/eventService';
import { api } from '@/helpers/api';

@Component({
  name: 'eventVue',
  mixins: [template],
  components: { Loading },
})
export default class EventView extends Vue {
  public event = new EventNew();
  public baseUrl = api.baseUrl + '/images/';
  public state = false;
  public get imagenDefault(): boolean {
    return this.event.image.search('default') === -1;
  }
  public async created(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    // eslint-disable-next-line radix
    const resp = await eventService.getEvent(Number.parseInt(this.$route.params.id));
    this.state = resp.data.state;
    if (resp.data.state) {
      this.event = resp.data.data.event;
    } else {
      this.$router.push('/dashboard/events');
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
}
