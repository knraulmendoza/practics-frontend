import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './questionView.vue';
import { ISelect } from '@/interfaces/interfaces';
import { globalSer } from '@/services/globalService';
import { QuestionSer } from '@/services/questionService';
import { Question } from '@/domain/entities/question/Question';
import { Select } from '@/domain/entities/select/Select';
import { CheckType } from '@/domain/entities/question/ChechkType';
import { Type } from '@/domain/entities/question/Type';
import { Status } from '@/helpers/Status';
import Loading from '@/components/loading.vue';
import { IQuestionResponse } from '@/interfaces/InterfaceResponse';

@Component({
  name: 'questionView',
  mixins: [template],
  components: { Loading },
})
export default class QuestionView extends Vue {
  public search = {
    typeQuestion: null,
    checkType: [] as any,
  };
  public menuFilter = false;
  public editedIndex = -1;
  public question: Question = new Question();
  public questions: Question[] = [];
  public checkType: number[] = [];
  public itemsTypeQuestions: ISelect[] = Select.itemsTypeQuestions;
  public itemsTypeChecks: ISelect[] = Select.itemsTypeChecks;
  public dialog = false;
  public questionsSimilarity: Question[] = [];
  public dialogQueSimilarity = false;
  public valid = false;
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalQuestions = 0;
  public lazy = false;
  public loading = true;
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
    },
    { text: 'tipo', value: 'type' },
    { text: 'Pregunta', value: 'question' },
    { text: 'chequeo', value: 'checkTypeText' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public validateSubject = {
    type: [(v: any) => !!v || 'Este campo es obligatorio'],
    question: [(v: any) => !!v || 'Este campo es obligatorio'],
  };

  get formTitle() {
    return this.editedIndex === -1 ? 'Nueva Pregunta' : 'Editar Pregunta';
  }

  public async created(): Promise<void> {
    this.showQuestions();
  }

  public next(): void {
    this.dialogQueSimilarity = false;
    this.dialog = false;
    this.question = new Question();
  }

  public close(): void {
    this.dialog = false;
    this.question = new Question();
    this.editedIndex = -1;
    this.checkType = [];
    (this.$refs.formQuestion as any).reset();
  }

  public async save(): Promise<void> {
    let text = '';
    if (this.checkType.length === 0) {
      this.question.checkType = CheckType.OTHER;
    } else {
      if (this.checkType.length === 1) {
        this.checkType.includes(1)
          ? (this.question.checkType = CheckType.STUDENTS)
          : (this.question.checkType = CheckType.TEACHERS);
      } else {
        this.question.checkType = CheckType.ALL;
      }
    }
    if (this.question.type === Type.ENCUESTA) {
      this.question.checkType = CheckType.OTHER;
    }
    if (this.editedIndex > -1) {
      this.update();
    } else {
      this.$store.commit('setLoading', { loading: true, msg: 'Guardando pregunta ...' });
      const res = await QuestionSer.save(this.question.toJson());
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (res.data.state) {
        await this.showQuestions();
        this.close();
        globalSer.success('La pregunta se registro de manera exitosa');
      } else {
        if (res.status === Status.REQUESTVALIDATIONS) {
          text = globalSer.errorValidation(res.dataError, false);
        } else {
          text = res.data.message;
        }
        globalSer.error(text);
      }
    }
  }

  public async serchQuestion(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Buscando preguntas similares' });
    const resp = await QuestionSer.searchQuestion(this.question.question);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (resp.data.state) {
      this.dialogQueSimilarity = true;
      const questionsAux = resp.data.data.questions as IQuestionResponse[];
      this.questionsSimilarity = questionsAux.map((question) => new Question(question));
    }
  }
  public asignar(question: string): void {
    this.question.question = question;
    this.dialogQueSimilarity = false;
  }

  public async update(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Actualizando pregunta ...' });
    const res = await QuestionSer.update(this.question.toJsonResponse());
    if (res.data.state) {
      this.questions = [];
      this.$store.commit('setLoading', { loading: false, msg: '' });
      await this.showQuestions();
      this.close();
      globalSer.success('La pregunta se actualizo de manera exitosa');
    } else {
      if (res.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(res.dataError);
      } else {
        globalSer.error(res.data.message);
      }
    }
  }
  public async showQuestions(): Promise<void> {
    this.loading = true;
    this.questions = [];
    this.pageCount = 0;
    this.perPage = 1;
    const resp = await QuestionSer.getQuestionsPaginate();
    if (resp.data.state) {
      this.questions = (resp.data.data.questions as IQuestionResponse[]).map((question) => new Question(question));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalQuestions = resp.data.data.total;
    }
    this.loading = false;
  }

  public async filterQuestions(withBtn = true): Promise<void> {
    this.menuFilter = false;
    let checkTypeAux = '';
    if (this.search.typeQuestion === Type.ENCUESTA) {
      checkTypeAux = CheckType.OTHER;
    } else {
      if (this.search.checkType.length === 1) {
        this.search.checkType.includes(1) ? (checkTypeAux = CheckType.STUDENTS) : (checkTypeAux = CheckType.TEACHERS);
      } else {
        checkTypeAux = CheckType.ALL;
      }
    }
    this.loading = true;
    this.questions = [];
    this.pageCount = 0;
    this.perPage = 1;
    this.page = withBtn ? 1 : this.page;
    const resp = await QuestionSer.filterQuestion(
      {
        check_type: checkTypeAux,
        type: this.search.typeQuestion,
      },
      this.page
    );
    if (resp.data.state) {
      this.questions = (resp.data.data.questions as IQuestionResponse[]).map((question) => new Question(question));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalQuestions = resp.data.data.total;
    }
    this.loading = false;
  }

  public async clearFilter(): Promise<void> {
    this.menuFilter = false;
    this.search = {
      typeQuestion: null,
      checkType: [] as any,
    };
    await this.showQuestions();
  }

  public nextPage(): void {
    this.hablePaginate();
  }
  public previous(): void {
    this.hablePaginate();
  }
  public async hablePaginate(): Promise<void> {
    if (this.search.typeQuestion === null) {
      this.questions = [];
      this.loading = true;
      const resp = await QuestionSer.getQuestionsPaginate(this.page);
      if (resp.data.state) {
        this.questions = (resp.data.data.questions as IQuestionResponse[]).map((question) => new Question(question));
      }
      this.loading = false;
    } else {
      this.filterQuestions(false);
    }
  }

  public edit(question: Question): void {
    this.dialogQueSimilarity = false;
    this.editedIndex = 1;
    this.dialog = true;
    const ques = Object.assign<unknown, IQuestionResponse>({}, question.toJsonResponse());
    this.question = new Question(ques);
    this.question.type = Number.parseInt(ques.type.toString(), 10) === Type.CHEQUEO ? Type.CHEQUEO : Type.ENCUESTA;
    this.question.checkType = ques.check_type;
    this.checkType = this.question.checkTypeSelect;
    // this.index = this.modules.findIndex((modAux) => modAux.id === this.module.id);
  }
}
