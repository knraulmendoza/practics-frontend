import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './AttentionView.vue';
import { IPatient, ISelect } from '@/interfaces/interfaces';
import { attentionSer } from '@/services/attentionService';
import { Patient } from '@/domain/entities/patient/Patient';
import moment from 'moment';
export interface Isearch {
  type: number;
  value: string;
}
@Component({
  name: 'attentionview',
  mixins: [template],
})
export default class AttentionView extends Vue {
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
      value: 'num',
    },
    { text: 'Fecha de registro', value: 'date', align: 'start', with: '300' },
    { text: 'Documento', value: 'document', align: 'start' },
    { text: 'Nombre', value: 'name', align: 'start' },
    // { text: 'Procedimientos aplicados', value: 'procedures', align: 'start' },
    // { text: 'Aprobada', value: 'state', sortable: false },
    { text: 'Atenciones', value: 'action', sortable: false },
  ];
  public patients: Patient[] = [];
  public editedIndex = -1;
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalPatients = 0;
  public dialog = false;
  public loading = true;
  public search: Isearch = {} as Isearch;
  public searchTypes: ISelect[] = [
    { text: 'Documento', value: 'document' },
    { text: 'Nombre', value: 'name' },
  ];
  public stateSearch = false;
  public dialogPoll = false;
  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nueva Atencion' : 'Editar Atención';
  }
  public openDialog(): void {
    this.dialog = true;
  }
  public close(): void {
    this.dialog = false;
  }

  public async created(): Promise<void> {
    this.showPatients();
  }

  public async showPatients(): Promise<void> {
    const resp = await attentionSer.getAllpaginate();
    this.loading = false;
    if (resp.data.state) {
      this.patients = (resp.data.data.patients as IPatient[]).map((patient) => new Patient(patient));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalPatients = resp.data.data.total;
    }
  }

  public nextPage(): void {
    this.hablePaginate();
  }
  public previous(): void {
    this.hablePaginate();
  }
  public async hablePaginate(): Promise<void> {
    this.patients = [];
    this.loading = true;
    const resp = await attentionSer.getAllpaginate(this.page);
    if (resp.data.state) {
      this.patients = (resp.data.data.patients as IPatient[]).map((patient) => new Patient(patient));
    }
    this.loading = false;
  }

  public getDate(date: string): string {
    return moment(date).format('YYYY/MM/DD HH:MM');
  }

  public async searchPatient(): Promise<void> {
    this.patients = [];
    this.loading = true;
    this.pageCount = 0;
    this.page = 1;
    const resp = await attentionSer.search(this.search);
    if (resp.data.state) {
      this.patients = (resp.data.data.patients as IPatient[]).map((patient) => new Patient(patient));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalPatients = resp.data.data.total;
    }
    this.loading = false;
    this.stateSearch = true;
  }

  public async clearSearch(): Promise<void> {
    this.stateSearch = false;
    this.search = {} as Isearch;
    await this.showPatients();
  }
}
