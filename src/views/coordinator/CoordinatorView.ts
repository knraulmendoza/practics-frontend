import Vue from 'vue';
import { personSer } from '@/services/personService';
import { DocumentType, DocumentTypeText } from '@/domain/entities/person/DocumentType';
import { Gender, GenderText } from '@/domain/entities/person/Gender';
import { Component } from 'vue-property-decorator';
import template from './CoordinatorView.vue';
import { ISelect } from '@/interfaces/interfaces';
import { Person } from '@/domain/entities/person/Person';
import { User } from '@/domain/entities/user/User';
import { globalSer } from '@/services/globalService';
import { Status } from '@/helpers/Status';
import { ICoordinatorResponse } from '@/interfaces/InterfaceResponse';
import { authSer } from '@/services/authService';
import { Roles } from '@/route/Roles';
import { ResponseApi } from '@/helpers/ResponseApi';
import { coordinatorService } from '@/services/coordinatorService';
import { Coordinator } from '@/domain/entities/coordinator/Coordinator';
import { careerService } from '@/services/careerService';
import Loading from '@/components/loading.vue';
import { StateCoordinator, StateCoordinatorText } from '@/domain/entities/state/StateCoordinator';

@Component({
  name: 'coordinatorview',
  mixins: [template],
  components: { Loading },
})
export default class CoordinatorView extends Vue {
  public birthday = new Date().toISOString().substr(0, 10);
  public modal = false;
  public loading = true;
  public e1 = 1;
  public documentTypes = globalSer.select(DocumentType, DocumentTypeText);
  public responseUserExists: ResponseApi = new ResponseApi();
  public statesCoordinator = globalSer.select(StateCoordinator, StateCoordinatorText);
  public genders = globalSer.select(Gender, GenderText);
  public valid = true;
  public validformUser = true;
  public lazy = false;
  public lazyFormUsre = false;
  public coordinator = new Coordinator();
  public person = new Person();
  public user = new User();
  public careers: ISelect[] = [];
  public dialog = false;
  public dialogUpdate = false;
  public tab = null;
  public search = '';
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
      value: 'num',
    },
    { text: 'Nombre', value: 'person.fullName' },
    { text: 'Programa', value: 'career.name' },
    { text: 'Estado', value: 'state' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public coordinators: Coordinator[] = [];
  public editedIndex = -1;
  public indexRotation = -1;

  public formCoordinatorValid = {
    first_name: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
    first_last_name: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
    second_last_name: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
    document_type: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
    document: [
      (v: unknown): boolean | string => !!v || 'Este campo es obligatorio',
      (v: unknown & number): boolean | string => (v && v >= 0) || 'No puede ingresar números negativo',
      (v: unknown & string): boolean | string =>
        (v && v.length <= 10 && v.length >= 8) || 'La identificación debe tener minímo 8 carácteres',
    ],
    phone: [
      (v: unknown): boolean | string => !!v || 'Este campo es obligatorio',
      (v: unknown & number): boolean | string => (v && v >= 0) || 'No puede ingresar números negativo',
      (v: unknown & string): boolean | string => (v && v.length >= 9) || 'El teléfono debe tener 10 carácteres',
    ],
    gender: [true],
    career_id: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
    birthday: [true],
    address: [(v: unknown): boolean | string => !!v || 'Este campo es obligatorio'],
  };
  public formValidUser = {
    userPlanta: [
      (v: unknown): boolean | string => !!v || 'El usuario es obligatorio',
      (v: unknown & string): boolean | string => (v || '').indexOf(' ') < 0 || 'No se permite espacios',
      (v: unknown & string): boolean | string => (v || '').indexOf('@') < 0 || 'No se permite @',
    ],
  };
  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nuevo Coordinador' : 'Editar Coordinador';
  }

  public created(): void {
    this.initialize();
    // this.careers = await careerService.getSelectCareers();
  }

  public async initialize(): Promise<void> {
    this.showCoordinators();
    this.careers = await careerService.getSelectCareers();
  }

  public async showCoordinators(): Promise<void> {
    this.loading = true;
    this.coordinators = [];
    const responseApi = await coordinatorService.getPersons();
    if (responseApi.data.state) {
      const coordinatorAux = responseApi.data.data.coordinators as ICoordinatorResponse[];
      this.coordinators = coordinatorAux.map((coordinator) => new Coordinator(coordinator));
    }
    this.loading = false;
  }

  public edit(item: Coordinator): void {
    this.dialogUpdate = true;
    this.editedIndex = 1;
    const coord = Object.assign<unknown, ICoordinatorResponse>({}, item.toJsonUpdate());
    this.coordinator = new Coordinator(coord);
    this.indexRotation = this.coordinators.findIndex((coorAux) => coorAux.id === this.coordinator.id);
  }

  public async update(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Actualizando datos del docente' });
    const response = await coordinatorService.put(this.coordinator);
    if (response.data.state) {
      await this.showCoordinators();
      this.dialogUpdate = false;
      this.$store.commit('setLoading', { loading: false, msg: '' });
      globalSer.success(`Se edito el coordinador ${this.coordinator.person.fullName} de manera correcta`);
    } else {
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (response.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(response.dataError);
      } else {
        globalSer.error(response.data.message);
      }
    }
  }

  public close(): void {
    if (this.editedIndex === -1) {
      this.dialog = false;
    } else {
      this.dialogUpdate = false;
    }
    const coord = Object.assign<unknown, ICoordinatorResponse>({}, this.coordinator.toJsonUpdate());
    this.coordinator = new Coordinator(coord);
    this.editedIndex = -1;
    this.e1 = 1;
    (this.$refs.formCoordinator as any).reset();
    (this.$refs.formUser as any).reset();
  }

  public async save(): Promise<void> {
    let responseDataUser: ResponseApi = new ResponseApi();
    if (!this.responseUserExists.data.state) {
      this.$store.commit('setLoading', { loading: true, msg: '' });
      this.user.password = this.user.user;
      this.user.rolId = Roles.COORDINATOR;
      responseDataUser = await authSer.add(this.user);
      this.$store.commit('setLoading', { loading: false, msg: '' });
    } else {
      responseDataUser = this.responseUserExists;
    }
    if (responseDataUser.data.state) {
      const responseUser = responseDataUser.data.data.user as User;
      this.person.userId = responseUser.id as number;
      this.$store.commit('setLoading', { loading: true, msg: 'Creando nuevo coordinador de práctica ...' });
      const responseDataPerson = await personSer.post(this.person);
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (responseDataPerson.data.state) {
        globalSer.success('Registro del coordinador exitoso');
      } else {
        if (responseDataPerson.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(responseDataPerson.dataError);
        } else {
          globalSer.error(responseDataPerson.data.message);
        }
      }
    } else {
      if (responseDataUser.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(responseDataUser.dataError);
      } else {
        globalSer.error(responseDataUser.data.message);
      }
    }
    this.close();
    await this.showCoordinators();
  }

  public async continueNext(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Validando usuario' });
    const resp = await authSer.verifyUserUpc(this.user.user);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (resp) {
      this.responseUserExists = await authSer.findUser(this.user.user);
      if (this.responseUserExists.data.state && this.responseUserExists.data.data.person != null) {
        globalSer.error('Este usuario ya esta registrado');
      } else {
        this.e1 = 2;
      }
    } else {
      globalSer.error('Este usuario no existe');
    }
  }
}
