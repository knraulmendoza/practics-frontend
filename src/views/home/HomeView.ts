import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './HomeView.vue';
import Loading from '@/components/loading.vue';
import Snackbar from '@/components/Snackbar.vue';

@Component({
  name: 'loginview',
  mixins: [template],
  components: { Loading, Snackbar },
})
export default class HomeView extends Vue {}
