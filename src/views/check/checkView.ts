import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './checkView.vue';
import { QuestionSer } from '@/services/questionService';
import { Question } from '@/domain/entities/question/Question';
import { AnswersChecks } from '@/domain/entities/answersChecks/AnswersChecks';
import { Check } from '@/domain/entities/check/Check';
import { checkService } from '@/services/checkService';
import { ICheckResponse, IQuestionResponse } from '@/interfaces/InterfaceResponse';
import { Person } from '@/domain/entities/person/Person';
import { RotationSer } from '@/services/rotationService';
import { ISelect } from '@/interfaces/interfaces';
import { Role } from '@/domain/entities/role/Rol';
import { Roles } from '@/route/Roles';
export interface Isearch {
  type: number;
  value: string;
}
@Component({
  name: 'checkView',
  mixins: [template],
})
export default class CheckView extends Vue {
  public search: Isearch = {} as Isearch;
  public questions: Question[] = [];
  public answersCheck: AnswersChecks[] = [];
  public checks: Check[] = [];
  public lazy = false;
  public loading = true;
  public loadingDialog = false;
  public textLoading = '';
  public page = 1;
  public pageCount = 0;
  public perPage = 1;
  public dialogAnswers = false;
  public currentPerson: Person = new Person();
  public rotations: ISelect[] = [];
  public rotation = -1;
  public evaluator = -1;
  public checksObservation: string[] = [];
  public headers = [
    { text: 'Evaluador', value: 'evaluator.fullName' },
    { text: 'Evaluado', value: 'evaluated.fullName' },
    { text: 'Tipo', value: 'type', sortable: false },
    { text: 'Respuestas', value: 'answers', sortable: false },
  ];
  public checksWithAnswers: Question[] = [];
  public validateCheck = {
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };
  public stateSearch = false;
  public typeCheck: ISelect[] = [
    { text: 'Evaluador', value: 'evaluator' },
    { text: 'Evaluado', value: 'evaluated' },
  ];

  public async created(): Promise<void> {
    this.loading = true;
    const resp = await checkService.getAllPaginate();
    if (resp.data.state) {
      this.checks = (resp.data.data.checks as ICheckResponse[]).map((check) => new Check(check));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      // this.totalChecks = resp.data.data.total;
    }
    this.loading = false;
  }

  public async save(): Promise<void> {
    // const res = await SubjectSer.save(this.subject);
    // if (res.state) {
    //   this.showSubjects();
    //   this.close();
    //   globalSer.alert('La asignatura se registro de manera exitosa', 'success');
    // } else {
    //   const text = `${res.code}<br/>${res.name}`;
    //   globalSer.alert('Opps', 'error', text);
    // }
  }
  public next(): void {
    // if (this.page === this.pageCount) {
    // }
  }

  public async searchChecks(): Promise<void> {
    this.loading = true;
    this.checks = [];
    const resp = await checkService.search(this.search);
    if (resp.data.state) {
      this.checks = (resp.data.data.checks as ICheckResponse[]).map((check) => new Check(check));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      // this.totalChecks = resp.data.data.total;
    }
    this.loading = false;
  }
  public async getQuestions(): Promise<void> {
    const questionsAux = (await QuestionSer.getQuestionsCheck()).data.data.questions as IQuestionResponse[];
    questionsAux.forEach((question) => {
      this.questions.push(new Question(question));
      this.answersCheck.push(new AnswersChecks({ answer: -1, question_id: question.id as number, observation: '' }));
    });
    this.loading = false;
  }

  public getRol(roles: Role[]): string {
    return roles.map((rol) => rol.id as number).includes(Roles.COORDINATOR)
      ? 'Coordinador a Docente'
      : 'Docente a Estudiante';
  }

  public async showAnswers(check): Promise<void> {
    if (this.currentPerson.id !== check.evaluator.id) {
      this.checksObservation = [];
      this.$store.commit('setLoading', { loading: true, msg: 'Cargando chequeos ...' });
      this.currentPerson = check.evaluated;
      this.rotations = await RotationSer.getRotationsByRol(check.evaluated.id);
      this.rotation = this.rotations[0].value as number;
      const resp = await checkService.checksByRotation({ rotation: this.rotation, evaluated: check.evaluated.id });
      this.dialogAnswers = true;
      this.checksWithAnswers = (resp.data.data.questions as IQuestionResponse[]).map(
        (question) => new Question(question)
      );
      this.checksObservation = resp.data.data.checksObservation as string[];
      this.$store.commit('setLoading', { loading: false, msg: '' });
    } else {
      this.dialogAnswers = true;
    }
  }

  public async getChecks(): Promise<void> {
    this.checksObservation = [];
    this.loadingDialog = true;
    this.textLoading = 'Cargando chequeos ...';
    const resp = await checkService.checksByRotation({ rotation: this.rotation, evaluated: this.currentPerson.id });
    this.dialogAnswers = true;
    this.checksWithAnswers = (resp.data.data.questions as IQuestionResponse[]).map(
      (question) => new Question(question)
    );
    this.checksObservation = resp.data.data.checksObservation as string[];
    this.loadingDialog = false;
    this.textLoading = '';
  }
}
