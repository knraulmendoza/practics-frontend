import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './ReportsView.vue';
import Loading from '@/components/loading.vue';
import BarChart from '@/components/charts/BarChart.vue';
import Reports from '@/components/reports/Reports.vue';
import { reportService } from '@/services/reportService';
import { globalSer } from '@/services/globalService';
import { ISelect } from '@/interfaces/interfaces';
import { SubjectSer } from '@/services/subjectService';
import { moduleSer } from '@/services/moduleService';
@Component({
  name: 'reports-view',
  mixins: [template],
  components: { Loading, BarChart, Reports },
})
export default class ReportsView extends Vue {
  public image = '';
  public dialog = false;
  public step = 1;
  public module = null;
  public subject = null;
  public modules: ISelect[] = [];
  public subjects: ISelect[] = [];
  public dialogBoxRotation = false;

  /**
   * openModal
   */

  public openDialog(step: number): void {
    this.dialog = true;
    this.step = step;
  }
  public async donwloadRotation(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Descargando cuadro de rotación' });
    const resp = await reportService.getRotation((this.module as unknown) as number);
    if (resp === null) {
      globalSer.error('Revisar el cuadro de rotaciones este bien para poder descargarlo');
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
  public closeDialog(): void {
    this.dialog = false;
  }

  public async created(): Promise<void> {
    this.subjects = await SubjectSer.getSubjectSelect();
  }

  public async getModules(): Promise<void> {
    this.modules = await moduleSer.getSeletBySubject((this.subject as unknown) as string);
  }
}
