import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './GroupStudentView.vue';
import { studentService, IstudentGroup } from '@/services/studentService';
import { assignStudentGroupSer } from '@/services/assignStudentGroupService';
import { globalSer } from '@/services/globalService';
import { SubjectSer } from '@/services/subjectService';
import { ISelect, IStudent } from '@/interfaces/interfaces';
import { moduleSer } from '@/services/moduleService';
import { Group } from '@/domain/entities/student/Group';
import Rotation from '@/domain/entities/rotation/Rotation';
import { assignmentSer } from '@/services/assignmentService';
import { IGroupResponse, IPersonResponse } from '@/interfaces/InterfaceResponse';
import { groupStudentSer } from '@/services/groupStudentService';
import { Person } from '@/domain/entities/person/Person';
import Loading from '@/components/loading.vue';
import Snackbar from '@/components/Snackbar.vue';
import { RotationSer } from '@/services/rotationService';
import AssignmentStudentGroup from '@/domain/entities/assignment/AssignmentStudentGroup';
import { WorkingDay } from '@/domain/entities/assignment/WorkingDay';
import moment from 'moment';
import { RotationType, RotationTypeText } from '@/domain/entities/rotation/RotationType';
import { Status } from '@/helpers/Status';

export interface IItem {
  start: number;
  maxEnd: number;
  text: string;
  disabled: boolean;
  value: number;
}

export interface IOptionGroups {
  group: number;
  remainingGroup: number;
  studentMax: number;
  studentRemainingMax: number;
}

@Component({
  name: 'groupStudent',
  mixins: [template],
  components: {
    Loading,
    Snackbar,
  },
})
export default class GroupStudentView extends Vue {
  public listStudents: IstudentGroup[] = [];
  public listOptions: IOptionGroups[] = [];
  public listGroupIndex: number[] = [];
  public items: IItem[] = [];
  public rotationsButton = false;
  public row = -1;
  public dialog = false;
  public labelGroup = '';
  public subject = null;
  public subjects: ISelect[] = [];
  public subjectSelected = false;
  public modules: ISelect[] = [];
  public moduleObject = null;
  public assignment = null;
  public group = null;
  public formRotation = false;
  public lazyRotation = false;
  public groupSelect: any[] = [];
  public groups: Group[] = [];
  public dialogRotation = false;
  public rotations: Rotation[] = [];
  public rotationsAux: Rotation[] = [];
  public rotationIndex = 0;
  public assignStudentsGroups: AssignmentStudentGroup[] = [];
  public assignStudentsGroupsArray: any[] = [];
  public validateAssignments: boolean[] = [];
  public validateAssignment = false;
  public withAssign = false;
  public stateEdit = false;
  public rotationTypes: ISelect[] = [];
  public rotationType: any = null;
  public selectedRotation: any = null;
  public workingDays: ISelect[] = [
    {
      value: 1,
      text: WorkingDay.MORNING,
    },
    {
      value: 2,
      text: WorkingDay.AFTERNOON,
    },
  ];
  public studentsListSend: any = [];
  public dialogSaveStudent = {
    listStudent: false,
    createGroups: false,
  };

  get btnEnabled(): boolean {
    return this.items.filter((item) => !item.disabled).length === 0;
  }

  get btnEnabledEditar(): boolean {
    return this.items.every((item) => item.disabled) && this.listStudents.every((item) => !item.edit);
  }

  get selectRotation(): Rotation {
    return this.rotationsAux.find((rot) => rot.id === this.selectedRotation) ?? new Rotation();
  }

  public async showRotationsByRotation($e): Promise<void> {
    this.rotationsAux = this.rotations.filter((rotation) => rotation.type === $e);
    this.assignStudentsGroups.forEach((item) => (item.assignmentId = null));
    this.assignStudentsGroupsArray = [];
    this.rotationIndex = 0;
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const assignments = await assignmentSer.getByModule(this.moduleObject as unknown as number);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    const groupSelectAux = Object.assign([], this.groupSelect);
    this.groupSelect = [];
    groupSelectAux.forEach((group: any) => {
      this.groupSelect.push({
        ...group,
        assignments,
        assignmentsAux: assignments,
      });
    });
    this.validateAssignment = this.rotationIndex === this.rotationType;
  }

  public removeGroup(student: IstudentGroup, i: number): void {
    const index = this.items.findIndex((value) => value.value === student.group.value);
    this.items[index].start--;
    this.items[index].text = `Grupo ${index + 1} (${this.items[index].start}/${this.items[index].maxEnd})`;
    this.items[index].disabled = false;
    this.listGroupIndex[i] = -1;
    this.listStudents[i].group = null;
    if (this.groupSelect.length > 0) {
      this.listStudents[i].edit = true;
    }
  }
  public addNumber(student: IstudentGroup, i: number): void {
    const index = this.items.findIndex((value) => value.value === student.group.value);
    let text = '';
    if (!this.items[index].disabled) {
      if (this.listGroupIndex[i] === -1) {
        this.items[index].start++;
        text = `Grupo ${index + 1} (${this.items[index].start}/${this.items[index].maxEnd})`;
      } else {
        const j = this.items.findIndex((val) => val.value === this.listGroupIndex[i]);
        this.items[j].start--;
        this.items[j].text = `Grupo ${j + 1} (${this.items[j].start}/${this.items[j].maxEnd})`;
        if (this.items[j].start !== this.items[j].maxEnd) {
          this.items[j].disabled = false;
        }
        this.items[index].start++;
        text = `Grupo ${index + 1} (${this.items[index].start}/${this.items[index].maxEnd})`;
      }
      this.listGroupIndex[i] = student.group.value;
      if (this.items[index].start <= this.items[index].maxEnd) {
        this.items[index].text = text;
      }

      if (this.items[index].start === this.items[index].maxEnd) {
        this.items[index].disabled = true;
      }
    }
  }
  public async list(): Promise<void> {
    const opt = ` y ${this.listOptions[this.row].remainingGroup} ${
      this.listOptions[this.row].remainingGroup === 1 ? 'grupo' : 'grupos'
    } de ${this.listOptions[this.row].studentRemainingMax} estudiantes`;
    this.labelGroup = `${this.listOptions[this.row].group} grupos de ${
      this.listOptions[this.row].studentMax
    }  estudiantes ${this.listOptions[this.row].remainingGroup === 0 ? '' : opt}`;
    this.dialog = false;
    this.items = [];
    for (let index = 0; index < this.listOptions[this.row].group; index++) {
      this.items.push({
        text: `Grupo ${index + 1} (0/${this.listOptions[this.row].studentMax})`,
        value: index,
        start: 0,
        maxEnd: this.listOptions[this.row].studentMax,
        disabled: false,
      });
      this.groupSelect.push({ text: `Grupo ${index + 1}`, value: index });
      this.assignStudentsGroups.push(new AssignmentStudentGroup());
    }
    for (
      let index = this.listOptions[this.row].group;
      index < this.listOptions[this.row].remainingGroup + this.listOptions[this.row].group;
      index++
    ) {
      this.items.push({
        text: `Grupo ${index + 1} (0/${this.listOptions[this.row].studentRemainingMax})`,
        value: index,
        start: 0,
        maxEnd: this.listOptions[this.row].studentRemainingMax,
        disabled: false,
      });
      this.groupSelect.push({ text: `Grupo ${index + 1}`, value: index });
      this.assignStudentsGroups.push(new AssignmentStudentGroup());
    }
    for (let index = 0; index < this.listStudents.length; index++) {
      this.listGroupIndex.push(-1);
      this.listStudents[index].group = null;
    }
  }
  public typeRotation(type: number): ISelect {
    let typeRotationObj: ISelect = {} as ISelect;
    switch (type) {
      case RotationType.FULL:
        typeRotationObj = {
          text: RotationTypeText.FULL,
          value: RotationType.FULL,
        };
        break;
      case RotationType.TWO:
        typeRotationObj = {
          text: RotationTypeText.TWO,
          value: RotationType.TWO,
        };
        break;
      case RotationType.THREE:
        typeRotationObj = {
          text: RotationTypeText.THREE,
          value: RotationType.THREE,
        };
        break;
      default:
        typeRotationObj = {
          text: RotationTypeText.FULL,
          value: RotationType.FULL,
        };
        break;
    }
    return typeRotationObj;
  }
  public async created(): Promise<void> {
    const respRotations = await RotationSer.getAll();
    if (!respRotations.data.state) {
      globalSer.error('No hay rotaciones registradas en este semestre.');
      this.$router.push('/dashboard/rotation');
    } else {
      (respRotations.data.data.rotationsType as any[]).forEach((rotationType, i) => {
        this.rotationTypes.push(this.typeRotation(i + 1));
        rotationType.rotations.forEach((rotation) => {
          const rotAux = new Rotation(rotation);
          rotAux.text = `Rotación (${rotation.start_date} a ${rotation.end_date})`;
          this.rotations.push(rotAux);
          if (rotAux.type === this.rotationType) {
            this.rotationsAux.push(rotAux);
          }
        });
      });
    }
    this.showSubjects();
  }
  public labelGroups(item: IOptionGroups): string {
    const opt = ` y ${item.remainingGroup} ${item.remainingGroup === 1 ? 'grupo' : 'grupos'} de ${
      item.studentRemainingMax
    } estudiantes`;
    return `${item.group} ${item.group === 1 ? 'grupo' : 'grupos'} de ${item.studentMax} estudiantes ${
      item.remainingGroup === 0 ? '' : opt
    }`;
  }

  public async showStudents($e): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Buscando estudiantes ...' });
    this.listStudents = await studentService.getAllUpc($e);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (this.listStudents.length === 0) {
      globalSer.error('El servidor esta caído y no carga los estudiantes');
    } else {
      for (let index = 2; index <= 6; index++) {
        let groupsAux = 0;
        let remaining = 0;
        let groups = 0;
        if (this.listStudents.length % 2 === 0) {
          if (this.listStudents.length % index === 0) {
            groups = this.listStudents.length / index;
          }
        } else {
          groupsAux = Math.floor(this.listStudents.length / index);
          remaining = this.listStudents.length - groupsAux * index;
          groups = groupsAux - remaining;
        }
        if (
          this.listOptions.find((opt) => opt.group === groups && opt.remainingGroup === remaining) == null &&
          groups > 0
        ) {
          this.listOptions.push({
            group: groups,
            studentMax: index,
            remainingGroup: remaining,
            studentRemainingMax: index + 1,
          });
        }
      }
      for (let index = 0; index < this.listStudents.length; index++) {
        this.listGroupIndex.push(-1);
      }
    }
  }
  public getPerson(person: IPersonResponse): Person {
    return new Person(person);
  }
  public async showSubjects(): Promise<void> {
    this.subjects = await SubjectSer.getSubjectSelect();
  }
  public close(): void {
    this.listStudents = [];
    this.listGroupIndex = [];
    this.listOptions = [];
    this.moduleObject = null;
    this.dialog = false;
    this.labelGroup = '';
    this.items = [];
    this.dialogRotation = false;
  }
  public async showModules(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Buscando estudiantes' });
    this.modules = [];
    this.moduleObject = null;
    this.row = -1;
    this.labelGroup = '';
    await this.showStudents(this.subject);
    if (this.listStudents.length) {
      this.modules = await moduleSer.getSeletBySubject(this.subject as unknown as string);
      this.$store.commit('setLoading', { loading: false, msg: '' });
    }
  }

  public async showAssignments($e): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Buscando grupos' });
    this.row = -1;
    this.stateEdit = false;
    this.items = [];
    this.groupSelect = [];
    this.validateAssignments = [];
    this.assignStudentsGroups = [];
    const assignments = await assignmentSer.getByModule($e);
    if (!assignments.length) {
      globalSer.error('Este módulo no tiene asignaciones asociadas en este semestre');
      this.$store.commit('setLoading', { loading: false, msg: '' });
      this.withAssign = true;
      return;
    }
    const resp = await groupStudentSer.groupsByModule($e);
    this.listStudents.forEach((student) => {
      student.group = null;
    });
    if (resp.data.state) {
      const groups = Math.ceil(resp.data.data.groups.length / 2);
      if (assignments.length < groups) {
        globalSer.error(
          `Debe crear ${groups - assignments.length} asignaciones para asociarlas a los grupos de prácticas`
        );
        this.$store.commit('setLoading', { loading: false, msg: '' });
        this.withAssign = true;
        return;
      }
      // const opt = ` y ${this.listOptions[this.row].remainingGroup}
      //  ${this.listOptions[this.row].remainingGroup === 1 ? 'grupo' : 'grupos'}
      //  de ${this.listOptions[this.row].studentRemainingMax} estudiantes`;
      // // this.labelGroup = `${this.listOptions[this.row].group} grupos de ${this.listOptions[this.row].studentMax}
      //  estudiantes ${
      //   this.listOptions[this.row].remainingGroup === 0 ? '' : opt
      // }`;
      let showAssign = true;
      this.stateEdit = true;
      this.items = [];
      (resp.data.data.groups as IGroupResponse[]).forEach((group) => {
        if (group.assign != null) {
          showAssign = false;
          this.withAssign = true;
        }
        this.items.push({
          text: `${group.name} (${group.students.length}/${group.students.length})`,
          // eslint-disable-next-line radix
          value: Number.parseInt(group.code as string),
          start: group.students.length,
          maxEnd: group.students.length,
          disabled: true,
        });

        this.groupSelect.push({
          text: group.name,
          // eslint-disable-next-line radix
          value: Number.parseInt(group.code as string),
          assignments,
          assignmentsAux: assignments,
        });
        this.validateAssignments.push(false);
        this.assignStudentsGroups.push(new AssignmentStudentGroup());
        group.students.forEach((student) => {
          const indexStudent = this.listStudents.findIndex(
            (studentAux) => studentAux.student.person.document === student.person.document
          );
          this.listStudents[indexStudent].group = this.items.find(
            // eslint-disable-next-line radix
            (item) => item.value === Number.parseInt(group.code as string)
          );
        });
      });
      this.dialogRotation = showAssign;
      this.rotationsButton = showAssign;
    } else {
      this.withAssign = false;
      // this.list();
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public searchAssign(): AssignmentStudentGroup[] {
    // const rotaAux = this.rotationsAux[this.rotationIndex].id;
    // if (this.assignStudentsGroups.every((assign) => assign.rotationId === rotaAux)) {
    return this.assignStudentsGroups.filter((item, index, array) => {
      if (item.assignmentId != null) {
        return (
          array.findIndex(
            (assign) =>
              assign.rotationId === item.rotationId &&
              assign.assignmentId.value === item.assignmentId.value &&
              assign.workingDay.value === item.workingDay.value
          ) === index
        );
      }
      return false;
    });
    // }
    // return [];
  }

  public getDate(date: string): string {
    return moment(date).format('dddd, DD [de] MMMM [del] YYYY');
  }

  public assignmentGroup($assignment, group: number, index: number): void {
    // const rotationId = this.rotationsAux[this.rotationIndex].id;
    if (this.assignStudentsGroups[index].groupId !== undefined) {
      this.assignStudentsGroups[index] = new AssignmentStudentGroup();
    }
    this.assignStudentsGroups[index].groupId = group;
    this.assignStudentsGroups[index].rotationId = this.selectedRotation as number;
    this.assignStudentsGroups[index].assignmentId = $assignment;
  }

  public async saveAssignmentRotation(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Creando el cuadro de rotaciones' });
    const resp = await assignStudentGroupSer.post(this.assignStudentsGroupsArray);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (resp.data.state) {
      this.dialogRotation = false;
      globalSer.success('Se registro correctamente');
      this.close();
    } else {
      if (resp.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(resp.dataError);
      } else {
        globalSer.error('No se pudo registrar su cuadro de rotación intente nuevamente');
      }
    }
  }

  public addAssignmentGroup(): void {
    const arrayAux = this.searchAssign();
    if (arrayAux.length !== this.assignStudentsGroups.length) {
      this.$store.commit('setSnackbar', {
        msg: 'No debe repetir asignaciones con jornada a varios grupos',
        color: 'error',
      });
      setTimeout(() => {
        this.$store.commit('setSnackbar', { msg: '', color: '' });
      }, 4000);
    } else {
      // const rotationId = this.rotationsAux[this.rotationIndex].id;
      const auxArray = this.assignStudentsGroups.filter(
        (assignment) => assignment.rotationId === this.selectedRotation
      );
      this.assignStudentsGroupsArray.push({ assignStudentsGroups: auxArray.map((assign) => assign.toJson()) });
      auxArray.forEach((item, index) => {
        this.groupSelect[index].assignments = this.groupSelect[index].assignments.filter(
          (assignment) => assignment.value !== item.assignmentId.value
        );
      });
      this.assignStudentsGroups.forEach((group) => {
        group.assignmentId = null;
      });
      this.rotationIndex++;
      if (this.rotationIndex === this.rotationType) {
        this.validateAssignment = true;
      }
    }
  }

  public getGroup(groupAux: number): void {
    return this.groupSelect.find((group) => group.value === groupAux)?.text;
  }

  public getAssignment(index: number, assignmentAux: any): string {
    return this.groupSelect[index].assignmentsAux.find((assignment) => assignment.value === assignmentAux)?.text;
  }

  public getAssignmentRotations(rotationId: number): AssignmentStudentGroup[] {
    return this.assignStudentsGroups.filter((assignment) => assignment.assignmentId === rotationId);
  }

  public async saveStudents(students): Promise<boolean> {
    /*await this.listStudents.reduce(async (promise, student) => {
      await promise;
      const addresses = student.student.person.address?.split('@')[0] as string;
      let responseUser = await authSer.findUser(addresses, Roles.STUDENT);
      if (!responseUser.data.state) {
        // globalSer.loading('Creando usuario');
        // this.$store.commit('setLoading', 'Creando usuario');
        const user = new User();
        user.user = addresses;
        user.password = user.user;
        user.rolId = Roles.STUDENT;
        responseUser = await authSer.add(user);
        // this.$store.commit('setLoading');
      }
      if (responseUser.data.state) {
        const user = responseUser.data.data.user as User;
        student.student.person.user_id = user.id as number;
        // this.$store.commit('setLoading', 'Creando el docente');
        await personSer.post(new Person(student.student.person));
      }
    }, Promise.resolve());*/
    // globalSer.loading('Validando información de los estudiantes');
    // this.$store.commit('setLoading', 'Validando información');
    const res = await studentService.saveStudents(students);
    return res.data.state;
    // if (res.data.state) {
    //   // globalSer.success('La información se valido de manera exitosa');
    // } else {
    //   if (res.status === Status.REQUESTVALIDATIONS) {
    //     globalSer.errorValidation(res.dataError);
    //   } else {
    //     globalSer.error(res.data.message);
    //   }
    // }
    // this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async editGroup(): Promise<void> {
    const studentsByEdit = this.listStudents.filter((student) => student.edit);
    if (studentsByEdit.length > 0) {
      this.groups = this.groupSelect.map((item, i) => {
        const students = studentsByEdit.filter((studentGroup) => studentGroup.group.value === item.value);
        return new Group({
          students: students.map((student) => student.student),
          name: `Grupo ${i + 1}`,
          module_id: this.moduleObject as unknown as number,
          code: item.value,
          assign: {},
        });
      });
      const resp = await groupStudentSer.put(this.groups);
      if (resp.data.state) {
        this.$store.commit('setSnackbar', { color: 'green', msg: 'Grupos creados exitosamente' });
        setTimeout(() => {
          this.$store.commit('setSnackbar', { color: 'green', msg: '' });
        }, 4000);
      } else {
        this.$store.commit('setSnackbar', { color: 'red', msg: 'Error al crear los grupos' });
      }
      setTimeout(() => {
        this.$store.commit('setSnackbar', { color: 'green', msg: '' });
      }, 4000);
    }
  }

  public async saveAll() {
    this.dialogSaveStudent.listStudent = true;
    const countStudent = Math.ceil(this.listStudents.length / 10);
    this.studentsListSend = [];
    for (let index = 0; index < countStudent; index++) {
      const divide = index * 10;
      this.studentsListSend.push({ students: this.listStudents.slice(divide, divide + 10), send: false, error: false });
    }
    for (const student of this.studentsListSend) {
      student.send = await this.saveStudents(student.students);
      student.error = !student.send;
    }
    this.dialogSaveStudent.createGroups = this.studentsListSend.every((data) => data.send);
    if (this.dialogSaveStudent.createGroups) {
      await this.addGroup();
    }
  }

  public async addGroup(): Promise<void> {
    // this.$store.commit('setLoading', { loading: true, msg: 'Creando grupos' });
    // await this.saveStudents();
    this.groups = this.items.map((item, i) => {
      const students = this.listStudents.filter((studentGroup) => studentGroup.group.value === item.value);
      return new Group({
        students: students.map((student) => student.student),
        name: `Grupo ${i + 1}`,
        module_id: this.moduleObject as unknown as number,
        assign: {},
      });
    });
    const resp = await groupStudentSer.post(this.groups);
    if (resp.data.state) {
      const assignments = await assignmentSer.getByModule(this.moduleObject as unknown as number);
      resp.data.data.groups.forEach((group, g) => {
        this.groupSelect[g].text = group.name;
        // eslint-disable-next-line radix
        this.groupSelect[g].value = Number.parseInt(group.code as string);
        this.groupSelect[g].assignments = assignments;
        this.groupSelect[g].assignmentsAux = assignments;
      });
      this.rotationsButton = true;
      this.dialogRotation = true;
    }
    this.dialogSaveStudent.listStudent = false;
  }

  public randomGroup(): void {
    let indexStudent = 0;
    this.items.forEach((group, g) => {
      for (let index = 0; index < group.maxEnd; index++) {
        this.listStudents[indexStudent].group = group;
        indexStudent++;
      }
      group.disabled = true;
      group.text = `grupo ${g + 1} (${group.maxEnd}/${group.maxEnd})`;
    });
  }
}
