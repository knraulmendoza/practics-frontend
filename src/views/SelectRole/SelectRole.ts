import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './SelectRole.vue';
import { globalSer, ICurrentUSer } from '@/services/globalService';
import { authSer } from '@/services/authService';
import { careerService } from '@/services/careerService';
import { ICareer, IFaculty } from '@/interfaces/interfaces';
import Loading from '@/components/loading.vue';
@Component({
  name: 'selectRoleView',
  mixins: [template],
  components: { Loading },
})
export default class SelectRole extends Vue {
  public roles: any = [];
  public careers: any = [];
  public faculties: IFaculty[] = [];
  public rol: any = null;
  public career: any = null;
  public faculty: any = null;
  public isSelectRole = false;
  public user: ICurrentUSer = {} as ICurrentUSer;
  public step = 1;

  public get title(): string {
    return this.roles.length > 1
      ? '¿Seleccione el tipo de usuario que va a ingresar al sistema?'
      : this.careers.length > 1
      ? '¿Seleccione una carrera para ingresar al sistema?'
      : '';
  }

  public created(): void {
    this.getFaculties();
    this.roles = localStorage.getItem('roles');
    this.roles = JSON.parse(this.roles);
    this.user = globalSer.getCurrentUser();
    this.rol = this.user.rol || null;
    this.career = this.user.career || null;
  }

  public async getFaculties(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Autenticandose' });
    this.faculties = [];
    const resp = await careerService.getAllFaculties();
    this.faculties = resp.data.data.faculties as IFaculty[];
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async continuar(): Promise<void> {
    if (this.step === 1 && this.faculty) {
      this.step = 2;
      const careersAux = JSON.parse(localStorage.getItem('careers') as string) as ICareer[];
      if (careersAux) {
        this.careers = careersAux.filter((career) => career.faculty_id === this.faculty.id);
      } else {
        const resp = await careerService.getAllByFaculty(this.faculty.code);
        this.careers = resp.data.data.careers as ICareer[];
      }
    }
    if (this.step === 2 && this.career) {
      if (this.roles.length === 1) {
        this.start();
      }
    }
    if (this.step === 3 && this.rol) {
      this.start();
    }
    // if (this.careers === undefined) {
    //   this.user.rol = (this.rol as unknown) as number;
    //   localStorage.setItem('currentUser', JSON.stringify(this.user));
    //   this.$router.push('/dashboard');
    // } else {
    //   this.isSelectRole = true;
    // }
  }

  public start(): void {
    const careerId = this.career as unknown as number;
    this.user.career = this.careers.find((career) => career.id === careerId);
    this.user.rol = this.rol as unknown as number;
    localStorage.setItem('currentUser', JSON.stringify(this.user));
    this.$router.push('/dashboard');
  }

  public async logout(): Promise<void> {
    await authSer.logout();
    this.$router.push('/');
  }
}
