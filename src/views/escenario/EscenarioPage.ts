import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './EscenarioPage.vue';
import Loading from '@/components/loading.vue';
import Snackbar from '@/components/Snackbar.vue';
import AreaByScenario from '@/components/areas/AreaByScenario.vue';
import scenarios from '@/components/scenarios/scenarios.vue';
import areas from '@/components/areas/Areas.vue';

@Component({
  name: 'escenariosVue',
  mixins: [template],
  components: { Loading, Snackbar, AreaByScenario, scenarios, areas },
})
export default class Escenarios extends Vue {
  public tab = null;
}
