import Vue from 'vue';
import Component from 'vue-class-component';
import template from './careerView.vue';
import Loading from '@/components/loading.vue';
import { Career } from '@/domain/entities/career/Career';
import { globalSer } from '@/services/globalService';
import { Status } from '@/helpers/Status';
import { ICareer, ICareerUpc, ISelect } from '@/interfaces/interfaces';
import { careerService } from '@/services/careerService';
import Snackbar from '@/components/Snackbar.vue';

@Component({
  name: 'careerView',
  mixins: [template],
  components: { Loading, Snackbar },
})
export default class CareerView extends Vue {
  public search = '';
  public editedIndex = -1;
  public career: Career = new Career();
  public careers: Career[] = [];
  public careersUpc: Career[] = [];
  public faculties: ISelect[] = [];
  public faculty: any = null;
  public dialog = false;
  public valid = false;
  public lazy = false;
  public loading = true;
  public msm =
    'Debe volver a ingresar para asociar una carrera a la sesión ' +
    'y poder realizar los procesos que dependen de está';
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
    },
    { text: 'Código', value: 'code' },
    { text: 'Programa', value: 'name' },
    { text: 'Facultad', value: 'faculty' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public validateCareer = {
    code: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  public created(): void {
    this.initCareer();
  }

  public async initCareer(): Promise<void> {
    await this.getFaculties();
    if (!this.faculties.length) {
      globalSer.error('No hay facultades, debe registrarlo primero');
      this.$router.push('/dashboard/configuracion');
    } else {
      await this.showCareers();
    }
  }

  public async getFaculties(): Promise<void> {
    this.faculties = [];
    this.faculties = await careerService.getAllFacultiesSelect();
  }

  public close(): void {
    this.dialog = false;
    this.career = new Career();
    this.careersUpc = [];
    this.faculty = null;
  }

  public async comparatorServer(): Promise<void> {
    const careerAux: Career[] = [];
    this.careersUpc = [];
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const careersAux = await careerService.getAllUpc(this.faculty.value);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (careersAux !== null && careersAux !== undefined) {
      if (careersAux.data.length) {
        this.careersUpc = careersAux.data.map((career) => new Career().fromJsonUpc(career));
        this.careersUpc.forEach((career) => {
          if (!this.careers.find((careerAuxi) => careerAuxi.code === career.code)) {
            careerAux.push(career);
          }
        });
        this.careersUpc = careerAux;
      } else {
        this.$store.commit('setSnackbar', { msg: 'Esta facultad no tiene programas', color: 'red' });
        setTimeout(() => {
          this.$store.commit('setSnackbar', { msg: '', color: '' });
        }, 4000);
      }
    } else {
      globalSer.error('El servidor de la universidad esta caido');
    }
  }

  /**
   * savecareer
   */
  public async saveCareer(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    let text = '';
    await this.careersUpc.reduce(async (promise, career) => {
      await promise;
      const res = await careerService.post(career, this.faculty.value);
      if (res.data.state) {
        this.showCareers();
        // career.faculty = this.faculty.text;
        // this.careers.push(career);
      } else {
        if (res.status === Status.REQUESTVALIDATIONS) {
          text = globalSer.errorValidation(res.dataError, false);
        } else {
          text = res.data.message;
        }
      }
    }, Promise.resolve());
    this.$store.commit('setLoading', { loading: false, msg: '' });
    this.close();
    if (text === '') {
      globalSer.success('Los programas se registraron de manera exitosa. ' + this.msm);
    } else {
      globalSer.error(text);
    }
  }
  public async showCareers(): Promise<void> {
    this.loading = true;
    const careers = await careerService.getAll();
    if (careers.data.state) {
      this.careers = (careers.data.data.careers as ICareer[]).map((career) => new Career(career));
    }
    this.loading = false;
  }
  public async searchCareer(item: Career): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const careerAux = await careerService.getCareer(item.code);

    if (careerAux !== null && careerAux !== undefined) {
      const auxProg = careerAux.data as ICareerUpc;
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (this.career === null && this.career === undefined) {
        globalSer.error('No se encontro el programa académico en los servidores de la Universidad');
      } else {
        this.career = new Career().fromJsonUpc(auxProg);
        this.update();
      }
    } else {
      this.$store.commit('setLoading', { loading: false, msg: '' });
      globalSer.error('El servidor de la universidad esta caido');
    }
  }

  public edit(careerAux: Career): void {
    this.editedIndex = careerAux.id === undefined ? -1 : careerAux.id;
    // this.dialog = true;
    const sub = Object.assign<unknown, ICareer>({}, careerAux.toJson());
    this.career = new Career(sub);
    this.searchCareer(this.career);
  }

  public async update(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Espere por favor....' });
    const res = await careerService.update(this.career, this.editedIndex);
    if (res.data.state) {
      this.showCareers();
      globalSer.success('El programa se modifico de manera exitosa');
    } else {
      if (res.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(res.dataError);
      } else {
        globalSer.error(res.data.message);
      }
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
}
