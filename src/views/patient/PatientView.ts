import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './PatientView.vue';
import { IPatient } from '@/interfaces/interfaces';
import { patientService } from '@/services/patientService';
import { Patient } from '@/domain/entities/patient/Patient';
import Loading from '@/components/loading.vue';
@Component({
  name: 'patientView',
  mixins: [template],
  components: { Loading },
})
export default class PatientView extends Vue {
  public patients: Patient[] = [];
  public lazy = false;
  public valid = false;
  public search = '';
  public loadingPacientes = true;

  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
      value: 'num',
    },
    { text: 'Documento', value: 'document' },
    { text: 'Nombre', value: 'name' },
    { text: 'Acción', value: 'action', sortable: false },
  ];

  public async created(): Promise<void> {
    const resp = await patientService.getAll();
    if (resp.data.state) {
      this.patients = (resp.data.data.patients as IPatient[]).map((patient) => new Patient(patient));
    }
    this.loadingPacientes = false;
  }

  public async initialize(): Promise<void> {
    this.patients = await (await patientService.getAll()).data.data;
    this.loadingPacientes = false;
  }
}
