import { Page } from '@/domain/entities/page/Page';
import { IPage, ISelect } from '@/interfaces/interfaces';
import { globalSer } from '@/services/globalService';
import { pageSer } from '@/services/pageService';
import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './PageView.vue';
@Component({
  name: 'pageview',
  mixins: [template],
})
export default class PageView extends Vue {
  public search = '';
  public loading = true;
  public pages: Page[] = [];
  public pagesSelect: ISelect[] = [];
  public page: Page = new Page();
  public subject = '';
  public formTitle = 'Páginas del sistema';
  public editedIndex = -1;
  public headers = [
    { text: 'Página', value: 'title' },
    { text: 'Estado', value: 'state' },
    { text: 'grupo', value: 'parentPage', with: 100 },
  ];

  public created(): void {
    this.initialize();
  }

  public async initialize(): Promise<void> {
    const resp = await pageSer.getAll();
    if (resp.data.state) {
      // this.pages = (resp.data.data.pages as IPage[]).map((page) => new Page(page));
      resp.data.data.pages.forEach((data) => {
        if (data.subGroup) {
          data.subGroup.forEach((item) => {
            this.pages.push(new Page(item));
          });
        } else {
          this.pages.push(new Page(data));
        }
      });
      this.pagesSelect = (resp.data.data.pages as IPage[]).map((page) => {
        return {
          text: page.title,
          value: page.id,
          state: page.state
        } as ISelect;
      });
      this.pagesSelect.unshift({
        text: 'Ninguno',
        value: '',
      });
    }
  }

  public async update(): Promise<void> {
    this.pages.reduce(async (promise, page) => {
      await promise;
      const res = await pageSer.updatePage(page);
      if (!res.data.state) {
        globalSer.error('error');
      }
    }, Promise.resolve());
  }
}
