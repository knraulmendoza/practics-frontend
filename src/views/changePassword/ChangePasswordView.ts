import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './ChangePasswordView.vue';
import { authSer } from '@/services/authService';
import Snackbar from '@/components/Snackbar.vue';
import Loading from '@/components/loading.vue';
import { ISelect } from '@/interfaces/interfaces';
import { globalSer } from '@/services/globalService';

@Component({
  name: 'changePasswordView',
  mixins: [template],
  components: {
    Snackbar,
    Loading,
  },
})
export default class ChangePassword extends Vue {
  public newPassword = '';
  public repeatPassword = '';
  public answer = '';
  public question = null;
  public questions: ISelect[] = [];
  public validForm = true;
  public lazyForm = false;
  public showPassword = false;

  public validate = {
    newPassword: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    repeatPassword: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    questions: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    answer: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  public async logout(): Promise<void> {
    await authSer.logout();
    this.$router.push('/');
  }

  public async created(): Promise<void> {
    this.questions = await authSer.securityQuestions();
  }

  public async changeNewPassword(): Promise<void> {
    if (this.newPassword === this.repeatPassword) {
      const resp = await authSer.updatedPassword(this.newPassword, this.answer, (this.question as unknown) as number);
      if (resp.data.state) {
        this.$router.push('/dashborad');
      } else {
        globalSer.error('Error del sevidor intentelo mas tarde');
      }
    } else {
      this.$store.commit('setSnackbar', { msg: 'Las contraseñas no coinciden', color: 'red' });
      setTimeout(() => {
        this.$store.commit('setSnackbar', {});
      }, 2000);
    }
  }
}
