import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './ModuleView.vue';
import { ISelect } from '@/interfaces/interfaces';
import { moduleSer } from '@/services/moduleService';
import { Module } from '@/domain/entities/module/Module';
import { IModuleResponse } from '@/interfaces/InterfaceResponse';
import { globalSer } from '@/services/globalService';
import { SubjectSer } from '@/services/subjectService';
import Loading from '@/components/loading.vue';
import { Status } from '@/helpers/Status';
import ProcedureComponent from '@/components/procedure';

@Component({
  name: 'moduleview',
  mixins: [template],
  components: { Loading, ProcedureComponent },
})
export default class ModuleView extends Vue {
  public search = '';
  public loading = true;
  public subject = '';
  public subjectId = [1];
  public editedIndexMod = -1;
  public index = 0;
  public alert = false;
  public subjects: ISelect[] = [];
  public headers = [
    { text: 'Práctica', value: 'name', width: 400 },
    { text: 'Descripción', value: 'description', width: 400 },
    { text: 'Asignatura', value: 'subject', with: 100 },
    { text: 'Procedimiento', value: 'procedure', sortable: false, align: 'center' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public modules: Module[] = [];
  public dialogModule = false;
  public dialogProcedure = false;
  public module: Module = new Module();
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalModules = 0;
  public valid = false;
  public lazy = false;

  public validateModule = {
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    code: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    subject: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  get formTitle(): string {
    return this.editedIndexMod === -1 ? 'Nueva práctica' : 'Editar práctica';
  }
  public closeDialogPro(): void {
    this.dialogProcedure = false;
    this.module = new Module();
  }

  public async openProcedure(mod: Module): Promise<void> {
    this.module = mod;
    this.dialogProcedure = true;
    // this.showProcedures(mod.id as number);
    // this.module = mod;
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Procesando información del módulo' });
    if (this.editedIndexMod === -1) {
      const resp = await moduleSer.post(this.module);
      if (resp.data.state) {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se registro el módulo correctamente');
        this.module.subject = this.subjects.find((subject) => subject.value === this.module.subjectCode)
          ?.text as string;
        this.modules.push(new Module(resp.data.data.module as IModuleResponse));
        this.close();
      } else {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        let text = '';
        if (resp.status === Status.REQUESTVALIDATIONS) {
          text = globalSer.errorValidation(resp.dataError, false);
        } else {
          text = resp.data.message;
        }
        globalSer.error(text);
      }
    } else {
      const resp = await moduleSer.put(this.module);
      if (resp.data.state) {
        await this.showModules();
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se actualizo el módulo correctamente');
        this.close();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    }
  }

  public editModule(module: Module): void {
    this.editedIndexMod = 1;
    this.dialogModule = true;
    const mod = Object.assign<unknown, IModuleResponse>({}, module.toJsonResponse());
    this.module = new Module(mod);
    this.index = this.modules.findIndex((modAux) => modAux.id === this.module.id);
  }
  public async showModules(): Promise<void> {
    const resp = await moduleSer.getAllPaginate();

    if (resp.data.state) {
      this.modules = (resp.data.data.modules as IModuleResponse[]).map((module) => new Module(module));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalModules = resp.data.data.total;
    }
    this.loading = false;
  }
  public close(): void {
    this.dialogModule = false;
    this.module = new Module();
    this.editedIndexMod = -1;
    this.index = 0;
    (this.$refs.formModule as any).reset();
  }

  public async created(): Promise<void> {
    this.subjects = await SubjectSer.getSubjectSelect();
    this.showModules();
  }

  public nextPage(): void {
    this.hablePaginate();
  }
  public previous(): void {
    this.hablePaginate();
  }
  public async hablePaginate(): Promise<void> {
    this.modules = [];
    this.loading = true;
    const resp = await moduleSer.getAllPaginate(this.page);
    if (resp.data.state) {
      this.modules = (resp.data.data.modules as IModuleResponse[]).map((module) => new Module(module));
    }
    this.loading = false;
  }
}
