import { personSer } from '@/services/personService';
import { DocumentType, DocumentTypeText } from '@/domain/entities/person/DocumentType';
import { Gender, GenderText } from '@/domain/entities/person/Gender';
import { Component, Vue } from 'vue-property-decorator';
import template from './TeacherView.vue';
import { ISelect } from '@/interfaces/interfaces';
import { teacherService } from '@/services/teacherServices';
import { Teacher } from '@/domain/entities/teacher/Teacher';
import { Person } from '@/domain/entities/person/Person';
import { User } from '@/domain/entities/user/User';
import { globalSer } from '@/services/globalService';
import { Status } from '@/helpers/Status';
import { ITeacherResponse } from '@/interfaces/InterfaceResponse';
import { authSer } from '@/services/authService';
import { Roles } from '@/route/Roles';
import { ResponseApi } from '@/helpers/ResponseApi';
import { StateTeacher, StateTeacherText } from '@/domain/entities/state/StateTeacher';
import { titleService } from '@/services/titleService';
import { Title } from '@/domain/entities/title/Title';
import Loading from '@/components/loading.vue';
import { TypeTextTeacher } from '@/domain/entities/teacher/TypeTeacher';
import { practicScenarioService } from '@/services/practicScenarioService';
export interface Isearch {
  type: string;
  value: string;
  person_type: Roles;
}
@Component({
  name: 'TeacherView',
  mixins: [template],
  components: { Loading },
})
export default class TeacherView extends Vue {
  public birthday = new Date().toISOString().substr(0, 10);
  public rolId = 0;
  public modal = false;
  public loading = true;
  public e1 = 1;
  public documentTypes = globalSer.select(DocumentType, DocumentTypeText);
  public responseUserExists: ResponseApi = new ResponseApi();
  public typesTeacher = [
    { value: Roles.TEACHER, text: TypeTextTeacher.PLANTA },
    { value: Roles.TEACHER_PRACTICT, text: TypeTextTeacher.OCASIONAL },
  ];
  public statesTeacher = globalSer.select(StateTeacher, StateTeacherText);
  public genders = globalSer.select(Gender, GenderText);
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalTeacher = 0;
  public valid = true;
  public validTitle = true;
  public validformUser = true;
  public lazy = false;
  public lazyTitle = false;
  public lazyFormUsre = false;
  public teacher: Teacher = new Teacher();
  public defaulTeacher: Teacher = new Teacher();
  public person: Person = new Person();
  public user: User = new User();
  public careers: ISelect[] = [];
  public dialog = false;
  public dialogUpdate = false;
  public dialogTitle = false;
  public tab = null;
  public teachers: Teacher[] = [];
  public editedIndex = -1;
  public titles: ISelect[] = [];
  public title: Title = new Title();

  public peopleTypes: ISelect[] = [
    { text: 'Documento', value: 'document' },
    { text: 'Nombre', value: 'name' },
    { text: 'Escenario de práctica', value: 'scenario' },
  ];
  public scenarios: ISelect[] = [];
  public search: Isearch = {} as Isearch;
  public stateSearch = false;
  public headers = [
    {
      text: '',
      align: 'left',
      sortable: false,
      value: 'num',
    },
    { text: 'Nombre', value: 'person.fullName' },
    { text: '# de Documento', value: 'person.document' },
    { text: 'Tipo', value: 'typeTextTeacher' },
    { text: 'Estado', value: 'state' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public formDocenteValid = {
    first_name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    first_last_name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    second_last_name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    document_type: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    document: [
      (v: unknown): string | boolean => !!v || 'Este campo es obligatorio',
      (v: unknown & number): string | boolean => (v && v >= 0) || 'No puede ingresar números negativo',
      (v: unknown & string): string | boolean =>
        (v && v.length <= 10 && v.length >= 8) || 'La identificación debe tener minímo 8 caracteres',
    ],
    phone: [
      (v: unknown): string | boolean => !!v || 'Este campo es obligatorio',
      (v: unknown & number): string | boolean => (v && v >= 0) || 'No puede ingresar números negativo',
      (v: unknown & string): string | boolean => (v && v.length >= 10) || 'La número debe tener 10 dígitos',
    ],
    gender: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    birthday: [true],
    address: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    title: [true],
  };
  public formValidUser = {
    userPlanta: [
      (v: unknown): string | boolean => !!v || 'El usuario es obligatorio',
      (v: unknown & string): string | boolean => (v || '').indexOf(' ') < 0 || 'No se permite espacios',
      (v: unknown & string): string | boolean => (v || '').indexOf('@') < 0 || 'No se permite @',
    ],
    password: [(v: unknown): string | boolean => !!v || 'La contraseña es obligatoria'],
    type: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };

  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nuevo Docente' : 'Editar Docente';
  }

  public formTitleValid = {
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    description: [true],
  };

  public created(): void {
    this.user = new User();
    this.initialize();
  }
  public rol($e: number | undefined): void {
    this.user.rolId = $e;
  }

  public getTitles($e: number[] | undefined): void {
    this.teacher.titleId = $e;
  }

  public async addTitle(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    if (this.editedIndex === -1) {
      const resp = await titleService.post(this.title);
      if (resp.data.state) {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se registro el título correctamente');
        this.titles = await titleService.getSelectTitles();
        this.dialogTitle = false;
        this.title = new Title();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    } else {
      const resp = await titleService.put(this.title);
      if (resp.data.state) {
        // await this.sho();
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.success('Se actualizo el título correctamente');
        this.dialogTitle = false;
        this.title = new Title();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    }
  }

  public async initialize(): Promise<void> {
    this.scenarios = await practicScenarioService.getAllSelect();
    this.defaulTeacher.person = new Person();
    this.titles = await titleService.getSelectTitles();
    this.showTeacher();
  }
  public async showTeacher(): Promise<void> {
    this.teachers = [];
    const resp = await teacherService.getPeoplePaginate();
    if (resp.data.state) {
      const teacherAuxs = resp.data.data.teachers as ITeacherResponse[];
      this.teachers = teacherAuxs.map((teacherAux) => new Teacher(teacherAux));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      this.totalTeacher = resp.data.data.total;
    }
    this.loading = false;
  }

  public edit(item: Teacher): void {
    this.dialogUpdate = true;
    const teacher = Object.assign<unknown, ITeacherResponse>({}, item.toJsonUpdate());
    this.teacher = new Teacher(teacher);
    this.editedIndex = this.teachers.findIndex((teacherAux) => teacherAux.id === this.teacher.id);
  }

  public async update(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Actualizando datos del docente' });
    const response = await teacherService.put(this.teacher);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (response.data.state) {
      globalSer.success(`Se edito el docente ${this.teacher.person.fullName} de manera correcta`);
      this.dialogUpdate = false;
      this.teachers[this.editedIndex] = this.teacher;
      this.teacher = new Teacher();
      this.editedIndex = -1;
    } else {
      if (response.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(response.dataError);
      } else {
        globalSer.error('Ops, error al modificar el docente');
      }
    }
    this.close();
  }

  public close(): void {
    if (this.editedIndex === -1) {
      this.dialog = false;
    } else {
      this.dialogUpdate = false;
    }
    this.teacher = new Teacher();
    this.user = new User();
    (this.$refs.formUser as any).reset();
    (this.$refs.formPerson as any).reset();
    this.editedIndex = -1;
    this.e1 = 1;
  }

  public async clearSearch(): Promise<void> {
    this.stateSearch = false;
    this.search = {} as Isearch;
    await this.showTeacher();
  }

  public async searchTeacher(): Promise<void> {
    this.teachers = [];
    this.loading = true;
    this.pageCount = 0;
    this.page = 1;
    if (this.search.type === 'scenario') {
      const resp = await teacherService.teacherByScenario(this.search.value as unknown as number, this.page);
      if (resp.data.state) {
        this.teachers = (resp.data.data.teachers as ITeacherResponse[]).map((teacherAux) => new Teacher(teacherAux));
      }
    } else {
      this.search.person_type = Roles.TEACHER;
      const resp = await personSer.search(this.search);
      if (resp.data.state) {
        this.teachers = (resp.data.data.people as ITeacherResponse[]).map((teacherAux) => new Teacher(teacherAux));
      }
    }
    this.perPage = 8;
    this.pageCount = Math.ceil(this.teachers.length / this.perPage);
    this.totalTeacher = this.teachers.length;
    this.loading = false;
    this.stateSearch = true;
  }

  public async save(): Promise<void> {
    let responseDataUser: ResponseApi = new ResponseApi();
    if (!this.responseUserExists.data.state) {
      this.$store.commit('setLoading', { loading: false, msg: 'Creando usuario' });
      this.user.password = this.user.user;
      responseDataUser = await authSer.add(this.user);
      this.$store.commit('setLoading', { loading: false, msg: '' });
    } else {
      responseDataUser = this.responseUserExists;
    }
    if (responseDataUser.data.state) {
      const responseUser = responseDataUser.data.data.user as User;
      this.person.userId = responseUser.id as number;
      this.$store.commit('setLoading', { loading: true, msg: 'Creando el docente' });
      const responseDataPerson = await personSer.post(this.person, this.teacher.titleId);
      if (responseDataPerson.data.state) {
        this.teacher.person = this.person;
        // this.teacher.id = (responseDataPerson.data.data as IPerson).id as number;
        // this.teachers.push(new Teacher(responseDataPerson.data.data.table_data as ITeacherResponse))
        await this.showTeacher();
        this.dialogUpdate = false;
        this.teacher = new Teacher();
        globalSer.success('Registro del docente exitoso');
        this.close();
      } else {
        if (responseDataPerson.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(responseDataPerson.dataError);
        } else {
          globalSer.error(responseDataPerson.data.message);
        }
      }
    } else {
      if (responseDataUser.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(responseDataUser.dataError);
      } else {
        globalSer.error(responseDataUser.data.message);
      }
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async continueNext(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Validando usuario' });
    const resp = await authSer.verifyUserUpc(this.user.user);
    if (resp) {
      this.responseUserExists = await authSer.findUser(this.user.user, this.user.rolId);
      if (this.responseUserExists.data.state && this.responseUserExists.data.data.person != null) {
        globalSer.success('Este usuario ya esta registrado');
      } else {
        this.e1 = 2;
      }
    } else {
      globalSer.error('Este usuario no existe');
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public nextPage(): void {
    this.hablePaginate();
  }
  public previous(): void {
    this.hablePaginate();
  }
  public async hablePaginate(): Promise<void> {
    this.teachers = [];
    this.loading = true;
    const resp = await teacherService.getPeoplePaginate(this.page);
    if (resp.data.state) {
      this.teachers = (resp.data.data.teachers as ITeacherResponse[]).map((teacherAux) => new Teacher(teacherAux));
    }
    this.loading = false;
  }
}
