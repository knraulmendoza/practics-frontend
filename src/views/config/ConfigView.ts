import Vue from 'vue';
import Component from 'vue-class-component';
import template from './ConfigView.vue';
import Loading from '@/components/loading.vue';
import Snackbar from '@/components/Snackbar.vue';
import { authSer } from '@/services/authService';
import { IAcademicPeriod, ISelect } from '@/interfaces/interfaces';
import sweetalert from 'sweetalert2';
import { academicPeriodService } from '@/services/academicPeriodService';
import { careerService } from '@/services/careerService';
import { Status } from '@/helpers/Status';
import { globalSer } from '@/services/globalService';
import { StateAcademicPeriod } from '@/domain/entities/academicPeriod/State';
import { AcademicPeriod } from '@/domain/entities/academicPeriod/AcademicPeriod';
import { AcademicPeriodCareer } from '@/domain/entities/academicPeriod/AcademicPeriodCareer';
import { ResponseApi } from '@/helpers/ResponseApi';
@Component({
  name: 'loginview',
  mixins: [template],
  components: { Loading, Snackbar },
})
export default class ConfigView extends Vue {
  public loading = {
    loadingQuestion: true,
    loadingFaculties: true,
    loadingAcademicPeriod: true,
  };
  public academicPeriodAux = new AcademicPeriodCareer();
  public dialogAcademicPeriod = false;
  public dialog = false;
  public questions: ISelect[] = [];
  public faculties: ISelect[] = [];
  public academicPeriods: ISelect[] = [];
  public valid = false;
  public question: ISelect = {} as ISelect;
  public editedIndex = -1;
  public academicPeriodByDate = new AcademicPeriod();
  public datePickerStart = false;
  public datePickerEnd = false;
  public existPeriodAcademicCareer = false;
  public headers = {
    question: [
      {
        text: 'item',
        align: 'left',
        sortable: false,
        width: 50,
      },
      { text: 'Pregunta', value: 'text' },
      { text: 'Acción', value: 'action', sortable: false },
    ],
    faculty: [
      {
        text: 'item',
        align: 'left',
        value: 'item',
        sortable: false,
        width: 50,
      },
      { text: 'Facultad', value: 'text' },
    ],
  };

  public get academicPeriod(): IAcademicPeriod {
    return this.$store.state.academicPeriod;
  }

  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nueva pregunta de seguridad' : 'Editar pregunta de seguridad';
  }
  public async created(): Promise<void> {
    await this.getQuestions();
    await this.getFaculties();
    await this.getPeriodAcademic();
    await this.getAllPeriods();
  }

  public async getAllPeriods(): Promise<void> {
    this.academicPeriods = await academicPeriodService.getAll();
    if (!this.academicPeriod) {
      const resp = await academicPeriodService.get();
      if (resp.data.state) {
        this.$store.commit('setAcademicPeriod', resp.data.data.academicPeriod as IAcademicPeriod);
        this.$store.commit('setEndDate', resp.data.data.academicPeriodCareer.end_date);
      }
    }
  }

  public async getFaculties(): Promise<void> {
    this.loading.loadingFaculties = true;
    this.faculties = [];
    this.faculties = await careerService.getAllFacultiesSelect();
    this.loading.loadingFaculties = false;
  }

  public async getPeriodAcademic(): Promise<void> {
    this.loading.loadingAcademicPeriod = true;
    const resp = await academicPeriodService.getAcademicPeriodByDate();
    this.loading.loadingAcademicPeriod = false;
    if (resp.data.state) {
      this.academicPeriodByDate = new AcademicPeriod(resp.data.data.academicPeriod as IAcademicPeriod);
      this.existPeriodAcademicCareer = resp.data.data.academicPeriodCareer === null;
    }
  }

  public async activeAcademicPeriod(): Promise<void> {
    if (this.academicPeriodAux.period && this.academicPeriodAux.period !== -1) {
      this.$store.commit('setLoading', { loading: true, msg: 'Activando.......' });
      const resp = await academicPeriodService.activeAcademicPeriod(this.academicPeriodAux.period);
      if (resp.data.state) {
        globalSer.success('Se activo el periodo académico de forma exitosa');
        const response = await academicPeriodService.get();
        if (response.data.state) {
          this.$store.commit('setAcademicPeriod', response.data.data.academicPeriod as IAcademicPeriod);
          this.$store.commit('setEndDate', response.data.data.academicPeriodCareer.end_date);
          this.academicPeriodAux.period = -1;
        } else {
          if (resp.status === Status.REQUESTVALIDATIONS) {
            globalSer.errorValidation(resp.dataError);
          } else {
            globalSer.error(resp.data.message);
          }
        }
      }
      this.$store.commit('setLoading', { loading: false, msg: '' });
    } else {
      globalSer.error('Debe seleccionar un periodo académico');
    }
  }

  public async saveFaculties(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const facultiesAuxUpc = await careerService.getAllFacultiesUpc();
    if (facultiesAuxUpc != null) {
      const facultiesAux: ISelect[] = [];
      facultiesAuxUpc.data.forEach((faculty) => {
        if (!this.faculties.filter((facultyAuxi) => facultyAuxi.value === faculty.id).length) {
          facultiesAux.push({
            text: faculty.facultad,
            value: faculty.id,
          });
        }
      });
      if (facultiesAux.length) {
        await facultiesAux.reduce(async (promise, faculty) => {
          await promise;
          const res = await careerService.postFaculties(faculty);
          if (res.data.state) {
            // this.showcareers();
            this.faculties.push(faculty);
          } else {
            if (res.status === Status.REQUESTVALIDATIONS) {
              globalSer.errorValidation(res.dataError);
            } else {
              globalSer.error(res.data.message);
            }
          }
        }, Promise.resolve());
        this.$store.commit('setLoading', { loading: false, msg: '' });
        // this.close();
        globalSer.success('Las facultades se registraron de manera exitosa');
      } else {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        // this.close();
        globalSer.success('No hay facultades nuevas');
      }
    } else {
      this.$store.commit('setLoading', { loading: false, msg: '' });
      // this.close();
      globalSer.error('Error de servidor: No hubo respuesta del servidor');
    }
  }

  public async getQuestions(): Promise<void> {
    this.loading.loadingQuestion = true;
    this.questions = [];
    this.questions = await authSer.securityQuestions();
    this.loading.loadingQuestion = false;
  }

  public async getAcademicPeriod(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Cargando periodo actual ...' });
    const respPeriodAcademic = await academicPeriodService.getAllUpc();
    if (respPeriodAcademic && Number(respPeriodAcademic.status) === Status.OK) {
      const getActAcademicPeriod = await academicPeriodService.get();
      const academicPeriod = {
        period: Number.parseInt(respPeriodAcademic.data.periodo, 10),
        year: Number.parseInt(respPeriodAcademic.data.anio, 10),
        start_date: respPeriodAcademic.data.fecha_inicio.split(' ')[0],
        end_date: respPeriodAcademic.data.fecha_finclase.split(' ')[0],
        state: StateAcademicPeriod.ACTIVE,
      };
      if (
        getActAcademicPeriod.data &&
        getActAcademicPeriod.data.data &&
        getActAcademicPeriod.data.data.academicPeriod.id
      ) {
        const academicPeriodAux = getActAcademicPeriod.data.data.academicPeriod;
        let respAdd: ResponseApi;
        if (
          Number.parseInt(academicPeriodAux.period, 10) === academicPeriod.period &&
          Number.parseInt(academicPeriodAux.year, 10) === academicPeriod.year
        ) {
          respAdd = await academicPeriodService.updateAcademicPeriod(academicPeriod, academicPeriodAux.id);
          if (respAdd.data.state) {
            this.$store.commit('setAcademicPeriod', respAdd.data.data.academicPeriod as IAcademicPeriod);
            this.academicPeriodByDate = new AcademicPeriod(respAdd.data.data.academicPeriod as IAcademicPeriod);
          } else {
            const msg =
              respAdd.data !== null && respAdd.data !== undefined ? respAdd.data.message : 'Error al actualizar';
            globalSer.error(msg);
          }
        } else {
          await this.addPeriod(academicPeriod);
        }
      } else {
        await this.addPeriod(academicPeriod);
      }
    } else {
      globalSer.error('Error de servidor: No hubo respuesta del servidor');
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async addPeriod(academicPeriod: any): Promise<void> {
    const respAdd = await academicPeriodService.addAcademicPeriod(academicPeriod);
    if (respAdd.data.state) {
      this.$store.commit('setAcademicPeriod', respAdd.data.data.academicPeriod as IAcademicPeriod);
      this.academicPeriodByDate = new AcademicPeriod(respAdd.data.data.academicPeriod as IAcademicPeriod);
      await this.getAllPeriods();
    } else {
      globalSer.error(respAdd.data.message);
    }
  }

  public edit(question: ISelect): void {
    this.dialog = true;
    this.question = question;
    this.editedIndex = this.questions.findIndex((questionAux) => questionAux.value === question.value);
  }
  public async deleteQuestion(question: ISelect): Promise<void> {
    sweetalert
      .fire({
        title: 'Deseas eliminar esta pregunta ?',
        text: 'Al eliminarla no podrás recuperarla',
        icon: 'error',
        showCancelButton: true,
        confirmButtonText: 'Yes, Eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then(async (result) => {
        if (result.isConfirmed) {
          this.$store.commit('setLoading', { loading: true, msg: '' });
          const resp = await authSer.deleteSecurityQuestions(question.value as number);
          this.$store.commit('setLoading', { loading: false, msg: '' });
          if (resp.data.state) {
            const index = this.questions.findIndex((questionAux) => questionAux.value === question.value);
            this.questions.splice(index, 1);
            sweetalert.fire('Eliminado!', 'Your file has been deleted.', 'success');
          }
        }
      });
  }

  public async updateAcademicPeriodCareer(): Promise<void> {
    if (this.academicPeriodAux.period && this.academicPeriodAux.period !== -1) {
      this.$store.commit('setLoading', { loading: true, msg: 'Modificando.......' });
      const res = await academicPeriodService.updDates(this.academicPeriodAux.toJson(), this.academicPeriodAux.period);
      if (res.data.state) {
        globalSer.success('Se modificó el periodo académico de forma exitosa');
      } else {
        if (res.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(res.dataError);
        } else {
          globalSer.error(res.data.message);
        }
      }
      this.$store.commit('setLoading', { loading: false, msg: '' });
      this.closeDialogAcademicPeriod();
    } else {
      globalSer.error('Debe seleccionar un periodo académico');
    }
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    if (this.editedIndex === -1) {
      const resp = await authSer.addSecurityQuestions(this.question.text);
      if (resp.data.state) {
        this.dialog = false;
        this.question = {} as ISelect;
        this.questions.push({
          text: resp.data.data.security_question.question,
          value: resp.data.data.security_question.id,
        });
      }
    } else {
      const resp = await authSer.updateSecurityQuestions(this.question.text, this.question.value as number);
      if (resp.data.state) {
        this.dialog = false;
        this.question = {} as ISelect;
        this.questions[this.editedIndex] = {
          text: resp.data.data.security_question.question,
          value: resp.data.data.security_question.id,
        };
      }
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
    this.editedIndex = -1;
  }
  public cancel(): void {
    this.dialog = false;
    this.question = {} as ISelect;
  }

  public openDialogAcademicPeriod(): void {
    if (this.academicPeriodAux.period) {
      this.dialogAcademicPeriod = true;
    } else {
      globalSer.error('Debe seleccionar un periodo académico');
    }
  }

  public closeDialogAcademicPeriod(): void {
    this.dialogAcademicPeriod = false;
    this.academicPeriodAux.startDate = '';
    this.academicPeriodAux.endDate = '';
  }

  public closePickerStart(): void {
    this.datePickerStart = false;
    this.academicPeriodAux.startDate = '';
  }

  public closePickerEnd(): void {
    this.datePickerEnd = false;
    this.academicPeriodAux.endDate = '';
  }
}
