import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './AssignmentView.vue';
import { IAssignment, ISelect } from '@/interfaces/interfaces';
import { SubjectSer } from '@/services/subjectService';
import { moduleSer } from '@/services/moduleService';
import { practicScenarioService } from '@/services/practicScenarioService';
import Assignment from '@/domain/entities/assignment/Assignment';
import { teacherService } from '@/services/teacherServices';
import { assignmentSer } from '@/services/assignmentService';
import { IAssignmentResponse } from '@/interfaces/InterfaceResponse';
import Loading from '@/components/loading.vue';
import { Status } from '@/helpers/Status';
import { globalSer } from '@/services/globalService';

@Component({
  name: 'assignmentView',
  mixins: [template],
  components: { Loading },
})
export default class AssignmentView extends Vue {
  public search = '';
  public loading = true;
  public editedIndex = -1;
  public subjects: ISelect[] = [];
  public headers = [
    { text: 'Módulo', value: 'module.name' },
    { text: 'Profesor', value: 'teacher.person.fullName' },
    { text: 'Escenario de práctica', value: 'scenario.name' },
    { text: 'Área', value: 'scenario.areas[0].name' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public modules: ISelect[] = [];
  public teachers: ISelect[] = [];
  public scenarios: ISelect[] = [];
  public areas: ISelect[] = [];
  public subject = '';
  public assignment = new Assignment();
  public assignmentInterface = {} as IAssignment;
  public assignments: Assignment[] = [];
  public dialog = false;
  public valid = false;
  public lazy = false;

  public validateAssignment = {
    subject: [(v: any) => !!v || 'Este campo es obligatorio'],
    module: [(v: any) => !!v || 'Este campo es obligatorio'],
    scenario: [(v: any) => !!v || 'Este campo es obligatorio'],
    area: [(v: any) => !!v || 'Este campo es obligatorio'],
    teacher: [(v: any) => !!v || 'Este campo es obligatorio'],
  };

  get formTitle(): string {
    return this.editedIndex === -1 ? 'Nueva asignación' : 'Editar Asignación';
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Creando asignación' });
    if (this.editedIndex === -1) {
      const resp = await assignmentSer.post(this.assignment);
      if (resp.data.state) {
        globalSer.success('Se creo la asignación correctamente');
        this.loading = true;
        this.showAssigment();
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          globalSer.errorValidation(resp.dataError);
        } else {
          globalSer.error(resp.data.message);
        }
      }
    } else {
      this.updateAssignment();
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
    this.close();
  }

  public async updateAssignment(): Promise<void> {
    const resp = await assignmentSer.update(this.assignment);
    if (resp.data.state) {
      globalSer.success('Se actualizó la asignación correctamente');
      this.loading = true;
      this.showAssigment();
    } else {
      if (resp.status === Status.REQUESTVALIDATIONS) {
        globalSer.errorValidation(resp.dataError);
      } else {
        globalSer.error(resp.data.message);
      }
    }
  }

  public close(): void {
    this.assignment = new Assignment();
    this.dialog = false;
    (this.$refs.formAssignment as any).reset();
  }

  public async edit(assignment: Assignment): Promise<void> {
    this.editedIndex = 1;
    const assign = Object.assign<unknown, IAssignmentResponse>({}, assignment.toJsonResponse());
    this.assignment = new Assignment(assign);
    this.subject = this.assignment.module.subjectCode as string;
    await this.getModules(this.subject, false);
    await this.getScenariosByModules(this.assignment.moduleId, false);
    this.assignment.scenarioId = assign.scenario.id as number;
    await this.getAreas(this.assignment.scenario.id);
    this.dialog = true;
  }

  public async getModules($e, select = true): Promise<void> {
    if ($e !== this.assignment.moduleId && select) {
      this.assignment.scenarioId = -1;
      this.assignment.areaId = -1;
      this.scenarios = [];
      this.areas = [];
    }
    this.$store.commit('setLoading', { loading: true, msg: '' });
    this.modules = await moduleSer.getSeletBySubject($e);
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async getScenariosByModules($e, select = true): Promise<void> {
    this.assignment.scenarioId = null;
    if ($e !== this.assignment.scenarioId && select) {
      this.assignment.areaId = -1;
      this.areas = [];
    }
    this.$store.commit('setLoading', { loading: true, msg: '' });
    this.scenarios = await practicScenarioService.getSelectScenariosByModule($e);
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async getAreas($e): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    this.areas = await practicScenarioService.getSelectAreas($e, this.assignment.moduleId);
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async showAssigment(): Promise<void> {
    this.assignments = [];
    const resp = await assignmentSer.getAll();
    if (resp.data.state) {
      this.assignments = (resp.data.data.assignments as IAssignmentResponse[]).map(
        (assignmentAux) => new Assignment(assignmentAux)
      );
    }
    this.loading = false;
  }

  public async created(): Promise<void> {
    this.subjects = await SubjectSer.getSubjectSelect();
    this.teachers = await teacherService.getAllSelect();
    this.showAssigment();
  }
}
