import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './RotationView.vue';
import Loading from '@/components/loading.vue';
import Rotation from '@/domain/entities/rotation/Rotation';
import moment from 'moment';
import { RotationSer } from '@/services/rotationService';
import { IRotation } from '@/interfaces/interfaces';
import { globalSer } from '@/services/globalService';
import { RotationType } from '@/domain/entities/rotation/RotationType';
export interface IrotationsType {
  rotations: IRotation[];
}
@Component({
  name: 'rotationView',
  mixins: [template],
  components: { Loading },
})
export default class RotationView extends Vue {
  public rotationsType: IrotationsType[] = [];
  public rotationsEnd: IrotationsType[] = [];
  public rotationsPush: Rotation[] = [];
  public rotation = new Rotation();
  public dialog = false;
  public datePickerStart = false;
  public datePickerEnd = false;
  public lastDate = '';
  public lastEndDate = '';
  public index = -1;
  public loading = true;
  public rotationsUpdate: Rotation[] = [];
  public dialogUpdate = false;
  public typeUpdate = 0;
  public headers = [
    { text: 'Nombre', value: 'name' },
    { text: 'Fecha inicial', value: 'startDate' },
    { text: 'Fecha Final', value: 'endDate' },
    { text: 'Acción', value: 'action', sortable: false },
  ];

  public get formTitle(): string {
    return this.index === -1 ? 'Nueva rotación' : 'Actualizar rotación';
  }

  public get endPeriodAcademic(): string {
    return this.$store.state.endDate;
  }

  /**
   * openDialogUpdate
   */
  public openDialogUpdate(type: number): void {
    this.dialogUpdate = true;
    this.typeUpdate = type;
  }

  public pushRotationUpdate(): void {
    if (this.rotationsUpdate.length === this.typeUpdate) {
      globalSer.error(
        `Solo se puede registrar ${this.typeUpdate} ${
          this.typeUpdate === RotationType.FULL ? 'rotación' : 'rotaciones'
        }`
      );
      return;
    }
    this.rotation.type = this.typeUpdate;
    this.rotationsUpdate.push(this.rotation);
    const rotationObject = Object.assign<unknown, IRotation>({}, this.rotation.toJson());
    this.rotation = new Rotation(rotationObject);
    this.addRotation();
  }

  /**
   * closeDialogUpdate
   */
  public closeDialogUpdate(): void {
    this.dialogUpdate = false;
    this.clearRotations();
  }

  public async updateRotations(): Promise<void> {
    if (this.rotationsUpdate.length !== this.typeUpdate) {
      globalSer.error(
        `Solo se puede registrar ${this.typeUpdate} ${
          this.typeUpdate === RotationType.FULL ? 'rotación' : 'rotaciones'
        }`
      );
      return;
    }
    this.$store.commit('setLoading', {
      loading: true,
      msg: 'Actualizando rotaciones',
    });
    const resp = await RotationSer.update(this.rotationsUpdate);
    if (resp.data.state) {
      this.rotationsType.splice(this.typeUpdate - 1, 1);
      this.rotationsType.push({ rotations: this.rotationsUpdate.map((rota) => rota.toJson()) });
      this.rotationsUpdate = [];
      this.dialogUpdate = false;
    } else {
      globalSer.error(resp.data.message);
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public getDateText(date: string): string {
    return moment(date).format('dddd, DD [de] MMMM [del] YYYY');
  }

  public getDateTimeFrom(dateStart: string, dateEnd: string): number {
    return moment(dateStart).diff(dateEnd, 'days');
  }

  public async created(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: '' });
    const resp = await RotationSer.getAll();
    if (resp.data.state) {
      this.rotationsType = resp.data.data.rotationsType as IrotationsType[];
      this.rotationsEnd = [...this.rotationsType];
    }
    this.lastDate = moment().subtract(1, 'd').utc().local().format();
    this.rotation.startDate = moment().utc().local().format().substr(0, 10);
    this.lastEndDate = moment(this.lastDate).add(1, 'd').utc().local().format();

    this.loading = !this.loading;
    if (moment(this.rotation.startDate).day() === 0) {
      this.rotation.startDate = moment(this.rotation.startDate).add(1, 'd').utc().local().format().substr(0, 10);
    }
    if (moment(this.rotation.startDate).day() === 6) {
      this.rotation.startDate = moment(this.rotation.startDate).add(2, 'd').utc().local().format().substr(0, 10);
    }
    this.rotation.endDate = moment(this.rotation.startDate).add(1, 'd').utc().local().format().substr(0, 10);
    if (moment(this.rotation.endDate).day() === 6) {
      this.rotation.endDate = moment(this.rotation.endDate).add(2, 'd').utc().local().format().substr(0, 10);
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public allowedDates(date: string): boolean {
    let stateDate = false;
    const dateLocal = moment(date).utc().local().format();
    switch (moment(dateLocal).day()) {
      case 0:
        stateDate = false;
        break;
      case 6:
        stateDate = false;
        break;
      default:
        stateDate = true;
        break;
    }
    // stateDate = (new Date(date).getDay() !== 5 || new Date(date).getDay() !== 6) ? true : false;
    return stateDate;
    // return parseInt(date.split('-')[2], 10)  === 0;
  }

  public clearRotations(): void {
    this.rotationsEnd = [];
    this.rotationsUpdate = [];
    if (this.rotationsType.length) {
      this.rotationsEnd = [...this.rotationsType];
    }
    const rotationObject = Object.assign<unknown, IRotation>({}, this.rotation.toJson());
    this.rotation = new Rotation(rotationObject);
    this.lastDate = moment().subtract(1, 'd').utc().local().format();
    this.rotation.startDate = moment().utc().local().format().substr(0, 10);
    this.lastEndDate = moment(this.lastDate).add(1, 'd').utc().local().format();
    this.rotation.endDate = moment(this.lastEndDate).utc().local().format().substr(0, 10);
  }

  /**
   * validRotation
   */
  public validRotation(rotationType: RotationType): boolean {
    let band = false;
    if (this.rotationsEnd.length === rotationType) {
      if (this.rotationsEnd[rotationType - 1].rotations.length === rotationType) {
        globalSer.error(
          `Solo se puede asignar ${rotationType} ${
            rotationType === RotationType.FULL ? 'rotación en este grupo' : 'rotaciones en este grupo'
          }`
        );
        band = true;
      }
    }
    return band;
  }

  public validRotationType(rotationType: RotationType): boolean {
    let band = false;
    if (this.rotationsEnd.length === rotationType) {
      if (this.rotationsEnd[rotationType - 1].rotations.length !== rotationType) {
        globalSer.error(`Debe tener ${rotationType} rotaciones el grupo`);
        band = true;
      }
    }
    return band;
  }

  public pushRotationType(): void {
    if (this.rotationsEnd.length < 3) {
      const ret: boolean[] = [];
      // ret.push(this.validRotationType(RotationType.FULL));
      ret.push(this.validRotationType(RotationType.TWO));
      ret.push(this.validRotationType(RotationType.THREE));
      if (ret.includes(true)) {
        return;
      }
      this.rotationsEnd.push({ rotations: [] });
      this.lastDate = moment(this.rotationsEnd[0].rotations[0].start_date).subtract(1, 'd').utc().local().format();
      this.rotation.startDate = moment(this.rotationsEnd[0].rotations[0].start_date)
        .utc()
        .local()
        .format()
        .substr(0, 10);
      this.rotation.endDate = moment(this.rotation.startDate).add(1, 'd').utc().local().format().substr(0, 10);
      this.lastEndDate = moment(this.lastDate).add(1, 'd').utc().local().format();
    } else {
      globalSer.error('Solo se pueden registrar 3 grupos de rotaciones');
    }
  }
  public pushRotation(): void {
    if (this.rotationsEnd.length) {
      const ret: boolean[] = [];
      ret.push(this.validRotation(RotationType.FULL));
      ret.push(this.validRotation(RotationType.TWO));
      ret.push(this.validRotation(RotationType.THREE));
      if (ret.includes(true)) {
        return;
      }
      this.rotationsEnd[this.rotationsEnd.length - 1].rotations.push(this.rotation.toJson());
    } else {
      this.rotationsEnd.push({ rotations: [this.rotation.toJson()] });
    }
    this.rotation.type = this.rotationsEnd.length;
    this.rotationsPush.push(this.rotation);
    const rotationObject = Object.assign<unknown, IRotation>({}, this.rotation.toJson());
    this.rotation = new Rotation(rotationObject);
    this.addRotation();
  }

  public opendDialog(): void {
    if (this.rotationsType.length < 3) {
      this.dialog = true;
    } else {
      globalSer.error('Solo se pueden registrar 3 grupos de rotaciones');
    }
  }

  public addRotation(): void {
    this.lastDate = moment(this.rotation.endDate).add(1, 'd').utc().local().format();
    this.rotation.startDate = moment(this.lastDate).utc().local().format().substr(0, 10);
    if (moment(this.rotation.startDate).day() === 0) {
      this.rotation.startDate = moment(this.rotation.startDate).add(1, 'd').utc().local().format().substr(0, 10);
    }
    if (moment(this.rotation.startDate).day() === 6) {
      this.rotation.startDate = moment(this.rotation.startDate).add(2, 'd').utc().local().format().substr(0, 10);
    }
    this.rotation.endDate = moment(this.rotation.startDate).add(1, 'd').utc().local().format().substr(0, 10);
    if (moment(this.rotation.endDate).day() === 0) {
      this.rotation.endDate = moment(this.rotation.endDate).add(1, 'd').utc().local().format().substr(0, 10);
    }
    if (moment(this.rotation.endDate).day() === 6) {
      this.rotation.endDate = moment(this.rotation.endDate).add(2, 'd').utc().local().format().substr(0, 10);
    }
    this.lastEndDate = moment(this.rotation.endDate).add(1, 'd').utc().local().format();
  }

  public async save(): Promise<void> {
    this.$store.commit('setLoading', {
      loading: true,
      msg: 'Guardando rotaciones',
    });
    const resp = await RotationSer.save(this.rotationsPush);
    if (resp.data.state) {
      this.rotationsType = [...this.rotationsEnd];
      this.rotationsPush = [];
      this.dialog = false;
    } else {
      globalSer.error(resp.data.message);
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public closePickerStart(): void {
    this.datePickerStart = false;
    this.rotation.startDate = '';
  }

  public closePickerEnd(): void {
    this.datePickerEnd = false;
    this.rotation.endDate = '';
  }

  /**
   * deletingRotations
   */
  public async deletingRotations(): Promise<void> {
    this.$store.commit('setLoading', {
      loading: true,
      msg: 'Eliminando rotaciones',
    });
    const resp = await RotationSer.deletingRotations();
    if (resp.data.state) {
      this.rotationsType = [];
      this.clearRotations();
    } else {
      globalSer.error(resp.data.message);
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
}
