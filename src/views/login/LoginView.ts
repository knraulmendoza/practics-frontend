import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './LoginView.vue';
import { authSer } from '@/services/authService';
import { IRole, ICareer, ISelect } from '@/interfaces/interfaces';
import { ILoginResponse, IUserResponse } from '@/interfaces/InterfaceResponse';
import { ICurrentUSer, globalSer } from '@/services/globalService';
import { User } from '@/domain/entities/user/User';
import Loading from '@/components/loading.vue';
import Snackbar from '@/components/Snackbar.vue';

@Component({
  name: 'loginview',
  mixins: [template],
  components: { Loading, Snackbar },
})
export default class LoginView extends Vue {
  public user: User = new User();
  public userRecovery = '';
  public document = '';
  public showPassword = false;
  public securityAnswer = '';
  public dialog = false;
  public newPassword = '';
  public repeatPassword = '';
  public questions: ISelect[] = [];
  public question = null;
  public valid = false;
  public lazy = false;
  public validVerify = false;
  public lazyVerify = false;
  public lazyPassword = false;
  public validPassword = false;

  public validate = {
    user: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    password: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };
  public validateVerify = {
    user: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    questions: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    securityAnswer: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };
  public validatePassword = {
    newPassword: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
    repeatPassword: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };
  public async login(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Autenticandose' });
    const resp = await authSer.authenticate(this.user);
    if (resp.data.state) {
      const responseLogin = resp.data.data as ILoginResponse;
      const currentUser: ICurrentUSer = {} as ICurrentUSer;
      currentUser.id = responseLogin.user.id as number;
      currentUser.user = responseLogin.user.user;
      if ((responseLogin.user.roles as IRole[]).length) {
        if ((responseLogin.user.roles as IRole[]).length === 1) {
          currentUser.rol = (responseLogin.user.roles as IRole[])[0].id as number;
        }
        localStorage.setItem('roles', JSON.stringify(responseLogin.user.roles as IRole[]));
      }
      if (responseLogin.careers.length) {
        if (responseLogin.careers.length === 1) {
          currentUser.career = responseLogin.careers[0];
        }
        localStorage.setItem('careers', JSON.stringify(responseLogin.careers as ICareer[]));
      }
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      localStorage.setItem('token', responseLogin.token);
      this.$router.push(
        `${
          (responseLogin.user.roles as IRole[]).length > 1 || responseLogin.careers.length > 1
            ? '/selectRole'
            : '/dashborad'
        }`
      );
      // while (this.loading) {
      //   if(this.$router.currentRoute.path != '/login') this.loading = false;
      // }
    } else {
      globalSer.error('Datos erroneos');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('token');
    }
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }

  public async verifyUser(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Enviando información' });
    const resp = await authSer.verifyUser(
      this.userRecovery,
      this.document,
      this.question as unknown as number,
      this.securityAnswer
    );
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (resp.data.state) {
      this.user = new User(resp.data.data.user as IUserResponse);
    } else {
      this.$store.commit('setSnackbar', {
        msg: 'El usuario con el documento no coinciden inténtelo nuevamente',
        color: 'red',
      });
      setTimeout(() => {
        this.$store.commit('setSnackbar', { msg: '', color: '' });
      }, 4000);
    }
  }
  public async created(): Promise<void> {
    this.questions = await authSer.securityQuestions();
  }
  public async changeNewPassword(): Promise<void> {
    if (this.newPassword === this.repeatPassword) {
      this.$store.commit('setLoading', { loading: true, msg: 'Cambiando contraseña ...' });
      const resp = await authSer.updatedPassword(
        this.newPassword,
        this.securityAnswer,
        this.question as unknown as number,
        this.user.id
      );
      this.$store.commit('setLoading', { loading: false, msg: '' });
      if (resp.data.state) {
        this.close();
        this.user.password = this.newPassword;
        this.login();
      } else {
        globalSer.error('Error del servidor inténtelo mas tarde');
      }
    } else {
      this.$store.commit('setSnackbar', { msg: 'Las contraseñas no coinciden', color: 'red' });
      setTimeout(() => {
        this.$store.commit('setSnackbar', { msg: '', color: '' });
      }, 2000);
    }
  }

  public close(): void {
    this.dialog = false;
    this.document = '';
    this.question = null;
    this.securityAnswer = '';
  }
}
