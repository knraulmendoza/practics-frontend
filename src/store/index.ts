import { IAcademicPeriod } from '@/interfaces/interfaces';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    msg: '',
    snackbar: false,
    colorSnackbar: 'green',
    endDate: '',
    academicPeriod: {} as IAcademicPeriod,
    pages: [],
    faculties: [],
  },
  mutations: {
    setLoading(state, { loading, msg }) {
      state.loading = loading;
      state.msg = msg;
    },
    setSnackbar(state, { msg, color }) {
      state.snackbar = !state.snackbar;
      state.msg = msg;
      state.colorSnackbar = color;
    },
    setEndDate(state, date) {
      state.endDate = date;
    },
    setAcademicPeriod(state, academicPeriod) {
      state.academicPeriod = academicPeriod;
    },
    setFaculties(state, faculties) {
      state.faculties = faculties;
    },
  },
  actions: {},
  modules: {},
});
