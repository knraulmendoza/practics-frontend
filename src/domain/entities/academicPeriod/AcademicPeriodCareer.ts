import { Entity } from '@/domain/base/BaseEntity';
import { IAcademicPeriodCareer } from '@/interfaces/interfaces';
import { StateAcademicPeriod } from './State';

export class AcademicPeriodCareer extends Entity {
  private _period: number;
  private _career: number;
  private _startDate: string;
  private _endDate: string;
  private _state: StateAcademicPeriod;

  public get period(): number {
    return this._period;
  }

  public set period(period: number) {
    this._period = period;
  }

  public get career(): number {
    return this._career;
  }

  public set career(career: number) {
    this._career = career;
  }

  public get startDate(): string {
    return this._startDate;
  }

  public set startDate(startDate: string) {
    this._startDate = startDate;
  }

  public get endDate(): string {
    return this._endDate;
  }

  public set endDate(endDate: string) {
    this._endDate = endDate;
  }

  public get state(): StateAcademicPeriod {
    return this._state;
  }

  public set state(state: StateAcademicPeriod) {
    this._state = state;
  }

  constructor(academicPeriod: IAcademicPeriodCareer = {} as IAcademicPeriodCareer) {
    super(academicPeriod.id);
    this._period = academicPeriod.academic_period_id;
    this._career = academicPeriod.career_id;
    this._startDate = academicPeriod.start_date;
    this._endDate = academicPeriod.end_date;
    this._state = academicPeriod.state;
  }

  public toJson(): IAcademicPeriodCareer {
    return {
      career_id: this._career,
      academic_period_id: this._period,
      start_date: this._startDate,
      end_date: this._endDate,
      state: this._state,
    };
  }
}
