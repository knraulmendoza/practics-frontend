import { Entity } from '@/domain/base/BaseEntity';
import { IAcademicPeriod } from '@/interfaces/interfaces';
import { StateAcademicPeriod } from './State';

export class AcademicPeriod extends Entity {
  private _year: number;
  private _period: number;
  private _startDate: string;
  private _endDate: string;
  private _state: StateAcademicPeriod;

  public get year(): number {
    return this._year;
  }

  public set year(year: number) {
    this._year = year;
  }

  public get period(): number {
    return this._period;
  }

  public set period(period: number) {
    this._period = period;
  }

  public get startDate(): string {
    return this._startDate;
  }

  public set starDate(starDate: string) {
    this._startDate = starDate;
  }

  public get endDate(): string {
    return this._endDate;
  }

  public set endDate(endDate: string) {
    this._endDate = endDate;
  }

  public get state(): StateAcademicPeriod {
    return this._state;
  }

  public set state(state: StateAcademicPeriod) {
    this._state = state;
  }

  constructor(academicPeriod: IAcademicPeriod = {} as IAcademicPeriod) {
    super(academicPeriod.id);
    this._year = academicPeriod.year;
    this._period = academicPeriod.period;
    this._startDate = academicPeriod.start_date;
    this._endDate = academicPeriod.end_date;
    this._state = academicPeriod.state;
  }

  public toJson(): IAcademicPeriod {
    return {
      year: this.year,
      period: this.period,
      start_date: this.starDate,
      end_date: this.endDate,
      state: this.state,
    };
  }
}
