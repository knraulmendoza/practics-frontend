import { Entity } from '@/domain/base/BaseEntity';
import { IAssignmentStudentGroupResponse } from '@/interfaces/InterfaceResponse';
import { IAssignmentStudentGroup } from '@/interfaces/interfaces';
import Rotation from '../rotation/Rotation';
import { Group } from '../student/Group';
import Assignment from './Assignment';
import { WorkingDay } from './WorkingDay';

export default class AssignmentStudentGroup extends Entity {
  private _rotationId?: number;
  private _rotation: Rotation;
  private _assignmentId?: any;
  private _assignment: Assignment;
  private _groupId?: number;
  private _group: Group;
  private _workingDay: any;

  public get rotation(): Rotation {
    return this._rotation;
  }

  public get assignment(): Assignment {
    return this._assignment;
  }

  public get group(): Group {
    return this._group;
  }

  public get rotationId(): number {
    return this._rotationId as number;
  }

  public set rotationId(rotationId: number) {
    this._rotationId = rotationId;
  }

  public get assignmentId(): any {
    return this._assignmentId;
  }

  public set assignmentId(assignmentId: any) {
    this._assignmentId = assignmentId;
  }

  public get groupId(): number {
    return this._groupId as number;
  }

  public set groupId(groupId: number) {
    this._groupId = groupId;
  }

  public get workingDay(): any {
    return this._workingDay;
  }

  public set workingDay(workingDay: any) {
    this._workingDay = workingDay;
  }

  constructor(assignmentStudentGroupResponse: IAssignmentStudentGroupResponse = {} as IAssignmentStudentGroupResponse) {
    super(assignmentStudentGroupResponse.id);
    this._assignment = new Assignment(assignmentStudentGroupResponse.assignment);
    this._rotation =
      assignmentStudentGroupResponse.rotation !== undefined
        ? new Rotation(assignmentStudentGroupResponse.rotation)
        : new Rotation();
    this._group =
      assignmentStudentGroupResponse.group !== undefined
        ? new Group(assignmentStudentGroupResponse.group)
        : new Group();
    this._assignmentId = null;
    this._workingDay = { value: 1, text: WorkingDay.MORNING };
  }

  public toJson(): IAssignmentStudentGroup {
    return {
      assignment_id: this.assignmentId.value,
      group_module_id: this.groupId,
      rotation_id: this.rotationId,
      working_day: this.workingDay.value === 1 ? WorkingDay.MORNING : WorkingDay.AFTERNOON,
    };
  }

  //   public toJsonResponse(): IAssignmentStudentGroupResponse {
  //     return {
  //       rotation: this.rotation.toJson(),
  //       assignment: this.assignment.toJsonResponse(),
  //       group: this.group.toJson(),
  //       id: this.id as number,
  //     };
  //   }
}
