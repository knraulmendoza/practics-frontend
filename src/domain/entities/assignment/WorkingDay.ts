export enum WorkingDay {
  MORNING = 'mañana',
  AFTERNOON = 'tarde',
  NOCHE = 'noche'
}
