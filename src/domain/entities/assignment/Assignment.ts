import { Entity } from '@/domain/base/BaseEntity';
import { Module } from '../module/Module';
import { Teacher } from '../teacher/Teacher';
import { IAssignmentResponse } from '@/interfaces/InterfaceResponse';
import { IAssignment } from '@/interfaces/interfaces';
import { PracticeScenario } from '../practiceScenario/PracticeScenario';
import { Area } from '../practiceScenario/Area';

export default class Assignment extends Entity {
  private _areaId?: number;
  private _module: Module;
  private _moduleId?: number;
  private _teacher: Teacher;
  private _teacherId?: number;
  private _scenario: PracticeScenario;
  private _scenarioId?: number | null;

  public get module(): Module {
    return this._module;
  }

  public get teacher(): Teacher {
    return this._teacher;
  }

  public get scenario(): PracticeScenario {
    return this._scenario;
  }

  public get areaId(): number {
    return this._areaId as number;
  }

  public set areaId(areaId: number) {
    this._areaId = areaId;
  }

  public get moduleId(): number {
    return this._moduleId as number;
  }

  public set moduleId(moduleId: number) {
    this._moduleId = moduleId;
  }

  public get teacherId(): number {
    return this._teacherId as number;
  }

  public set teacherId(teacherId: number) {
    this._teacherId = teacherId;
  }

  public get scenarioId(): number | null {
    return this._scenarioId || null;
  }

  public set scenarioId(scenarioId: number | null) {
    this._scenarioId = scenarioId;
  }

  constructor(assignmentResponse: IAssignmentResponse = {} as IAssignmentResponse) {
    super(assignmentResponse.id);
    this._module = new Module(assignmentResponse.module);
    this._teacher = new Teacher(assignmentResponse.teacher);
    this._scenario = new PracticeScenario(assignmentResponse.scenario);
    this._teacherId = assignmentResponse.teacher?.id;
    this._moduleId = assignmentResponse.module?.id;
    this._areaId = (assignmentResponse.scenario?.areas as Area[])[0].id;
    this._scenarioId = assignmentResponse.scenario?.id;
  }

  public toJson(): IAssignment {
    return {
      area_id: this.areaId,
      module_id: this.moduleId,
      teacher_id: this.teacherId,
      scenario_id: this.scenarioId,
    };
  }

  public toJsonResponse(): IAssignmentResponse {
    return {
      module: this.module.toJsonResponse(),
      scenario: this.scenario.toJson(),
      teacher: this.teacher.toJsonUpdate(),
      id: this.id as number,
    };
  }
}
