import { Entity } from '@/domain/base/BaseEntity';
import { Type, TypeText } from './Type';
import { CheckType, CheckTypeText } from './ChechkType';
import { IQuestion } from '@/interfaces/interfaces';
import { AnswersChecks } from '../answersChecks/AnswersChecks';
import { IQuestionResponse } from '@/interfaces/InterfaceResponse';

export class Question extends Entity {
  private _type: Type;
  private _question: string;
  private _checkType: CheckType;
  private _answers?: AnswersChecks[];

  public get type(): Type {
    return this._type;
  }

  public set type(type: Type) {
    this._type = type;
  }

  public get question(): string {
    return this._question;
  }

  public set question(question: string) {
    this._question = question;
  }

  public get checkType(): CheckType {
    return this._checkType;
  }

  public set checkType(checkType: CheckType) {
    this._checkType = checkType;
  }

  public get answers(): AnswersChecks[] | undefined {
    return this._answers;
  }

  constructor(question: IQuestionResponse = {} as IQuestionResponse) {
    super(question.id);
    this._type = question.type;
    this._question = question.question;
    this._checkType = question.check_type;
    if (question.answers !== undefined) {
      this._answers = question.answers.map((answer) => new AnswersChecks(answer));
    }
  }

  public toJson(): IQuestion {
    return {
      type: this.type,
      check_type: this.checkType,
      question: this.question,
    };
  }

  public toJsonResponse(): IQuestionResponse {
    return {
      id: this.id,
      type: this.type,
      check_type: this.checkType,
      question: this.question.toLowerCase(),
      answers: [],
    };
  }

  public get checkTypeText(): CheckTypeText {
    switch (this.checkType) {
      case CheckType.STUDENTS:
        return CheckTypeText.STUDENTS;
      case CheckType.TEACHERS:
        return CheckTypeText.TEACHERS;
      case CheckType.ALL:
        return CheckTypeText.ALL;
      case CheckType.OTHER:
        return CheckTypeText.OTHER;
    }
  }

  public get checkTypeSelect(): number[] {
    switch (this.checkType) {
      case CheckType.STUDENTS:
        return [1];
      case CheckType.TEACHERS:
        return [2];
      case CheckType.ALL:
        return [1, 2];
      case CheckType.OTHER:
        return [];
    }
  }

  public get typeText(): TypeText {
    switch (this.type) {
      case Type.CHEQUEO:
        return TypeText.CHEQUEO;
      case Type.ENCUESTA:
        return TypeText.ENCUESTA;
    }
  }
}
