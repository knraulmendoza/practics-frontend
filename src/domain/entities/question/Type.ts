export enum Type {
  CHEQUEO = 1,
  ENCUESTA = 2,
}

export enum TypeText {
  CHEQUEO = 'Chequeo',
  ENCUESTA = 'Encuesta paciente',
}
