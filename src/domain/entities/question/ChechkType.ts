export enum CheckType {
  STUDENTS = 'students',
  TEACHERS = 'teachers',
  ALL = 'students,teachers',
  OTHER = '',
}

export enum CheckTypeText {
  STUDENTS = 'Estudiantes',
  TEACHERS = 'Profesores',
  ALL = 'Estudiantes y profesores',
  OTHER = '',
}
