import { Entity } from '@/domain/base/BaseEntity';
import Module from '@/views/module';
import { IProcedure } from '@/interfaces/interfaces';

export class Procedure extends Entity {
  private _name: string;
  private _description?: string;
  private _moduleId: number;
  private _module?: Module;

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get description(): string | undefined {
    return this._description;
  }

  public set description(description: string | undefined) {
    this._description = description;
  }

  public get moduleId(): number {
    return this._moduleId;
  }

  public set moduleId(moduleId: number) {
    this._moduleId = moduleId;
  }

  public get module(): Module | undefined {
    return this._module;
  }

  public set module(module: Module | undefined) {
    this._module = module;
  }

  constructor(procedure: IProcedure = {} as IProcedure) {
    super(procedure.id);
    this._name = procedure.name;
    this._description = procedure.description;
    this._moduleId = procedure.module_id;
    // this._module = new Module(procedure.module);
  }

  public toJson(): IProcedure {
    return {
      name: this.name,
      description: this.description,
      module_id: this.moduleId,
    };
  }
  public toJsonResponse(): IProcedure {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      module_id: this.moduleId,
    };
  }
}
