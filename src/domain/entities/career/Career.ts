import { ICareer, ICareerUpc } from '@/interfaces/interfaces';
import { Entity } from '@/domain/base/BaseEntity';

export class Career extends Entity {
  private _name: string;
  private _code: string;
  private _facultyId?: number;
  private _faculty: string;
  private _description?: string;
  private _facultyCode?: string;

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get code(): string {
    return this._code;
  }

  public set code(code: string) {
    this._code = code;
  }

  public get description(): string | undefined {
    return this._description;
  }

  public set description(description: string | undefined) {
    this._description = description;
  }

  public get facultyId(): number | undefined {
    return this._facultyId;
  }

  public set facultyId(facultyId: number | undefined) {
    this._facultyId = facultyId;
  }

  public get faculty(): string {
    return this._faculty;
  }

  public set faculty(faculty: string) {
    this._faculty = faculty;
  }

  public get facultyCode(): string | undefined {
    return this._facultyCode;
  }

  public set facultyCode(facultyCode: string | undefined) {
    this._facultyCode = facultyCode;
  }

  constructor(career: ICareer = {} as ICareer) {
    super(career.id);
    this._name = career.name;
    this._code = career.code;
    this._description = career.description;
    this._faculty = career.faculty !== undefined ? (career.faculty.name as string) : '';
    this._facultyCode = career.faculty_code !== undefined ? career.faculty_code : '';
  }

  public toJson(): ICareer {
    return {
      id: this.id,
      name: this.name,
      code: this.code,
      description: this.description,
      faculty_id: this.facultyId,
    };
  }
  public fromJson(career: ICareer): Career {
    return new Career({
      id: career.id,
      name: career.name,
      code: career.code,
    });
  }
  public fromJsonUpc(career: ICareerUpc): Career {
    return new Career({
      name: career.nombre,
      code: career.id,
    });
  }
}
