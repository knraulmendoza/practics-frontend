import { User } from '../user/User';
import { DocumentType, DocumentTypeText } from './DocumentType';
import { Gender } from './Gender';
import moment from 'moment';
import { Entity } from '../../base/BaseEntity';
import { IPerson } from '@/interfaces/interfaces';
import { IPersonResponse } from '@/interfaces/InterfaceResponse';

export class Person extends Entity {
  private _document: string;
  private _documentType: DocumentType;
  private _firstName: string;
  private _secondName?: string;
  private _firstLastName: string;
  private _secondLastName: string;
  private _phone?: string;
  private _birthDay?: Date;
  private _address?: string;
  private _gender?: Gender;
  private _user?: User;
  private _userId: number;
  private _careerId?: number;

  public get document(): string {
    return this._document;
  }

  public set document(document: string) {
    this._document = document;
  }

  public get documentType(): DocumentType {
    return this._documentType;
  }

  public set documentType(documentType: DocumentType) {
    this._documentType = documentType;
  }

  public get firstName(): string {
    return this._firstName;
  }

  public set firstName(firstName: string) {
    this._firstName = firstName;
  }

  public get secondName(): string | undefined {
    return this._secondName;
  }

  public set secondName(secondName: string | undefined) {
    this._secondName = secondName;
  }

  public get firstLastName(): string {
    return this._firstLastName;
  }

  public set firstLastName(firstLastName: string) {
    this._firstLastName = firstLastName;
  }

  public get secondLastName(): string {
    return this._secondLastName;
  }

  public set secondLastName(secondLastName: string) {
    this._secondLastName = secondLastName;
  }

  public get phone(): string | undefined {
    return this._phone;
  }

  public set phone(phone: string | undefined) {
    this._phone = phone;
  }

  public get birthDay(): Date | undefined {
    return this._birthDay;
  }

  public set birthDay(birthDay: Date | undefined) {
    this._birthDay = birthDay;
  }

  public get address(): string | undefined {
    return this._address;
  }

  public set address(address: string | undefined) {
    this._address = address;
  }

  public get gender(): Gender | undefined {
    return this._gender;
  }

  public set gender(gender: Gender | undefined) {
    this._gender = gender;
  }

  public get user(): User | undefined {
    return this._user;
  }

  public set user(user: User | undefined) {
    this._user = user;
  }

  public get userId(): number {
    return this._userId;
  }

  public set userId(userId: number) {
    this._userId = userId;
  }

  public get careerId(): number | undefined {
    return this._careerId;
  }

  public set careerId(careerId: number | undefined) {
    this._careerId = careerId;
  }

  constructor(person: IPersonResponse = {} as IPersonResponse) {
    super(person.id);
    this._document = person.document;
    this._documentType = Number.parseInt(person.document_type, 10);
    this._firstName = person.first_name;
    this._secondName = person.second_name;
    this._firstLastName = person.first_last_name;
    this._secondLastName = person.second_last_name;
    this._phone = person.phone;
    this._birthDay = person.birthday;
    this._address = person.address;
    this._gender = Number.parseInt(person.gender as string, 10);
    this._userId = person.user_id;
    if (person.user !== undefined) {
      this._user = new User(person.user);
    }
  }

  public toJson(): IPerson {
    return {
      document: this.document,
      document_type: this.documentType,
      first_name: this.firstName,
      second_name: this.secondName,
      first_last_name: this.firstLastName,
      second_last_name: this.secondLastName,
      phone: this.phone,
      birthday: this.birthDay,
      address: this.address,
      gender: this.gender,
      user_id: this.userId,
      career_id: this.careerId,
    };
  }

  public fromJson(): IPerson {
    return {
      document: this._document,
      document_type: this._documentType,
      first_name: this._firstName,
      second_name: this._secondName,
      first_last_name: this._firstLastName,
      second_last_name: this._secondLastName,
      phone: this._phone,
      birthday: this._birthDay,
      address: this.address,
      gender: this._gender as number,
      user_id: this._userId,
    };
  }

  public fromJsonResponse(): IPersonResponse {
    return {
      document: this._document,
      document_type: this._documentType.toString(),
      first_name: this._firstName,
      second_name: this._secondName,
      first_last_name: this._firstLastName,
      second_last_name: this._secondLastName,
      phone: this._phone,
      birthday: this._birthDay,
      address: this.address,
      gender: (this._gender as number).toString(),
      user_id: this._userId,
    };
  }

  public get fullName(): string {
    return `${this.firstName} ${this.secondName != null ? this.secondName + ' ' : ''}${this.firstLastName} ${
      this.secondLastName
    }`;
  }

  public get age(): number {
    return Math.floor(moment().diff(this.birthDay, 'days') / 365);
  }

  public get documentTypeText(): string {
    switch (this.documentType) {
      case DocumentType.CEDULA:
        return DocumentTypeText.CEDULA;
      case DocumentType.TARJETA_IDENTIDAD:
        return DocumentTypeText.TARJETA_IDENTIDAD;
      case DocumentType.CEDULA_EXTRANJERA:
        return DocumentTypeText.CEDULA_EXTRANJERA;
    }
    return '';
  }
}
