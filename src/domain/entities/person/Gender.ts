export enum Gender {
    MASCULINE = 1,
    FEMININE = 2,
    OTHER = 3,
}

export enum GenderText {
    MASCULINE = 'Masculino',
    FEMININE = 'Femenino',
    OTHER = 'otro',
}
