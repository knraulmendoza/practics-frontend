export enum DocumentType {
  CEDULA = 1,
  TARJETA_IDENTIDAD = 2,
  CEDULA_EXTRANJERA = 3,
}

export enum DocumentTypeText {
  CEDULA = 'Cédula',
  TARJETA_IDENTIDAD = 'Tarjeta de Identidad',
  CEDULA_EXTRANJERA = 'Cédula extranjera',
}
