export enum LevelComplexityType {
    LEVEL1 = 1,
    LEVEL2 = 2,
    LEVEL3 = 3,
    LEVEL4 = 4,
}

export enum LevelComplexityTypeText {
    LEVEL1 = 'Nivel 1',
    LEVEL2 = 'Nivel 2',
    LEVEL3 = 'Nivel 3',
    LEVEL4 = 'Nivel 4',
}
