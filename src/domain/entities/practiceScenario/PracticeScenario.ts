import { Entity } from '../../base/BaseEntity';
import { IPracticeScenario } from '@/interfaces/interfaces';
import { Area } from './Area';

export class PracticeScenario extends Entity {
  private _name: string;
  private _description?: string;
  private _complexityLevel?: number;
  private _sceneType: number;
  private _entity: number;
  private _quotas: number;
  private _areas?: Area[];
  private _areasList?: Area[];

  public get name(): string {
    return this._name;
  }
  public set name(name: string) {
    this._name = name;
  }

  public get description(): string | undefined {
    return this._description;
  }
  public set description(description: string | undefined) {
    this._description = description;
  }

  public get complexityLevel(): number | undefined {
    return this._complexityLevel;
  }
  public set complexityLevel(complexityLevel: number | undefined) {
    this._complexityLevel = complexityLevel;
  }

  public get sceneType(): number {
    return this._sceneType;
  }
  public set sceneType(sceneType: number) {
    this._sceneType = sceneType;
  }

  public get entity(): number {
    return this._entity;
  }
  public set entity(entity: number) {
    this._entity = entity;
  }

  public get areas(): Area[] | undefined {
    return this._areas;
  }
  public set areas(areas: Area[] | undefined) {
    this._areas = areas;
  }

  public get areasList(): Area[] | undefined {
    return this._areasList;
  }
  public set areasList(areas: Area[] | undefined) {
    this._areasList = areas;
  }
  public get quotas(): number {
    return this._quotas;
  }
  public set quotas(quotas: number) {
    this._quotas = quotas;
  }

  constructor(scenario: IPracticeScenario = {} as IPracticeScenario) {
    super(scenario.id);
    this._name = scenario.name;
    this._description = scenario.description;
    this._complexityLevel = scenario.complexity_level;
    this._sceneType = scenario.scene_type;
    this._entity = scenario.entity;
    this._quotas = scenario.quotas;
    this.conversionStringToNumber(scenario);
    if (scenario.areas !== undefined) {
      this._areas = scenario.areas.map((area) => new Area(area));
    }
  }

  public conversionStringToNumber(scenario: IPracticeScenario): void {
    if (typeof scenario.complexity_level === 'string') {
      // eslint-disable-next-line radix
      this._complexityLevel = Number.parseInt(scenario.complexity_level);
    }
    if (typeof scenario.scene_type === 'string') {
      // eslint-disable-next-line radix
      this._sceneType = Number.parseInt(scenario.scene_type);
    }
    if (typeof scenario.entity === 'string') {
      // eslint-disable-next-line radix
      this._entity = Number.parseInt(scenario.entity);
    }
  }

  public toJson(): IPracticeScenario {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      complexity_level: this.complexityLevel,
      scene_type: this.sceneType,
      entity: this.entity,
      areas: this.areas,
      quotas: this.quotas,
    };
  }

  public toJsonUpdate(): IPracticeScenario {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      complexity_level: this.complexityLevel,
      scene_type: this.sceneType,
      entity: this.entity,
      quotas: this.quotas,
    };
  }
}
