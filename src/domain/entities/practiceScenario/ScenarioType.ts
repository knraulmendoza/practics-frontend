export enum ScenarioType {
  HOSPITAL = 1,
  CLINIC = 2,
  IE = 3,
  NEIGHBORHOOD = 4,
  OTRO = 5,
}

export enum ScenarioTypeText {
  HOSPITAL = 'HOSPITAL',
  CLINIC = 'CLÍNICA',
  IE = 'INSTITUCIÓN EDUCATIVA',
  NEIGHBORHOOD = 'BARRIO',
  OTRO = 'OTRAS ENTIDADES',
}
