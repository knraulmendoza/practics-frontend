import {Entity} from '../../base/BaseEntity';
import { IArea } from '@/interfaces/interfaces';

export class Area extends Entity {
    private _name: string;
    private _description?: string ;
    private _moduleId?: number;

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get description(): string| undefined {
        return this._description;
    }

    public set description(description: string| undefined) {
        this._description = description;
    }

    public get moduleId(): number| undefined {
        return this._moduleId;
    }

    public set moduleId(moduleId: number| undefined) {
        this._moduleId = moduleId;
    }

    constructor(area: IArea = {} as IArea ) {
        super(area.id);
        this._name = area.name;
        this._description = area.description;
        this._moduleId = area.moduleId;
    }

    public toJson(): IArea {
        return{
            name: this.name,
            description: this.description,
            moduleId: this.moduleId,
        };
    }
}
