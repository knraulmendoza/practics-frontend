import { Entity } from '../../base/BaseEntity';
import { Person } from '../person/Person';
import { ICoordinatorResponse } from '@/interfaces/InterfaceResponse';
import { Career } from '../career/Career';

export class Coordinator extends Entity {
  private _person: Person;
  private _career?: Career;
  private _state: number;

  public get person(): Person {
    return this._person;
  }

  public set person(person: Person) {
    this._person = person;
  }

  public get career(): Career | undefined {
    return this._career;
  }

  public set career(career: Career | undefined) {
    this._career = career;
  }

  public get state(): number {
    return this._state;
  }

  public set state(state: number) {
    this._state = state;
  }

  constructor(coordinator: ICoordinatorResponse = {} as ICoordinatorResponse) {
    super(coordinator.id);
    this._person = new Person(coordinator.person);
    this._career = coordinator.career == null ? new Career() : new Career(coordinator.career);
    this._state = Number.parseInt(coordinator.state, 10);
  }

  public toJsonUpdate(): ICoordinatorResponse {
    return {
      id: this.id,
      person: this.person.fromJsonResponse(),
      state: this.state.toString(),
    };
  }
}
