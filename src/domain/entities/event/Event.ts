import { Entity } from '@/domain/base/BaseEntity';
import { IEvent } from '@/interfaces/interfaces';

export class EventNew extends Entity {
  private _title: string;
  private _date: Date;
  private _description: string;
  private _hour: string;
  private _address: string;
  private _state: number;
  private _image: string;

  public get title(): string {
    return this._title;
  }

  public set title(title: string) {
    this._title = title;
  }

  public get date(): Date {
    return this._date;
  }

  public set date(date: Date) {
    this._date = date;
  }

  public get description(): string {
    return this._description;
  }

  public set description(description: string) {
    this._description = description;
  }

  public get hour(): string {
    return this._hour;
  }

  public set hour(hour: string) {
    this._hour = hour;
  }

  public get address(): string {
    return this._address;
  }

  public set address(address: string) {
    this._address = address;
  }

  public get state(): number {
    return this._state;
  }

  public set state(state: number) {
    this._state = state;
  }

  public get image(): string {
    return this._image;
  }

  public set image(image: string) {
    this._image = image;
  }
  constructor(event: IEvent = {} as IEvent) {
    super(event.id);
    this._title = event.title;
    this._date = event.date;
    this._description = event.description;
    this._hour = event.hour;
    this._address = event.address;
    this._state = event.state;
    this._image = event.image;
  }

  public toJson(): IEvent {
    return {
      date: this.date,
      description: this.description,
      title: this._title,
      hour: this._hour,
      address: this.address,
      state: this.state,
      image: this.image,
    };
  }
}
