export enum State {
  PENDIENTE = 1,
  REALIZADO = 2,
  POSPUESTO = 3,
}

export enum StateText {
  PENDIENTE = 'Pendiente',
  REALIZADO = 'Realizado',
  POSPUESTO = 'Pospuesto',
}
