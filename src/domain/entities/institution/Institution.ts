import { Entity } from '@/domain/base/BaseEntity';
import { IInstitucion } from '@/interfaces/interfaces';
import { ClassInstitutions } from './ClassInstitutions';
import { TypesInstitutions } from './TypesInstitutions';

export class Institution extends Entity {
    private _code: string;
    private _name: string;
    private _type: TypesInstitutions;
    private _institutionClass: ClassInstitutions;
    private _description?: string | undefined;

    public get code(): string {
        return this._code;
    }

    public set code(code: string) {
        this._code = code;
    }

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get type(): TypesInstitutions {
        return this._type;
    }

    public set type(type: TypesInstitutions) {
        this._type = type;
    }

    public get institutionClass(): ClassInstitutions {
        return this._institutionClass;
    }

    public set institutionClass(institutionClass: ClassInstitutions) {
        this._institutionClass = institutionClass;
    }

    public get description(): string | undefined {
        return this._description;
    }

    public set description(description: string | undefined) {
        this._description = description;
    }

    constructor(institution: IInstitucion = {} as IInstitucion) {
        super(institution.id);
        this._code = institution.code;
        this._name = institution.name;
        this._type = institution.type;
        this._institutionClass = institution.class;
        this._description = institution.description;
    }

    public toJson(): IInstitucion {
        return {
            name: this.name,
            code: this.code,
            description: this.description,
            class: this.institutionClass,
            type: this.type,
        };
    }
}

