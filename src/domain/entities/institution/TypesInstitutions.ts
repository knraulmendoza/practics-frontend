export enum TypesInstitutions {
    SCHOOL = 1,
    CLINIC = 2,
    HOSPITAL = 3,
}

export enum TypesInstitutionsText {
    SCHOOL = 'Colegio',
    CLINIC = 'Clinica',
    HOSPITAL = 'Hospital',
}
