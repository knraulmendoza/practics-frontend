export enum ClassInstitutions {
    PUBLIC = 1,
    PRIVATE = 2,
}

export enum ClassInstitutionsText {
    PUBLIC = 'publica',
    PRIVATE = 'privada',
}
