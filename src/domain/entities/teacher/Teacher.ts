import { Entity } from '../../base/BaseEntity';
import { Person } from '../person/Person';
import { ITeacher } from '@/interfaces/interfaces';
import { ITeacherResponse } from '@/interfaces/InterfaceResponse';
import { TypeTextTeacher } from './TypeTeacher';
import { Roles } from '@/route/Roles';
import { StateTeacher } from '../state/StateTeacher';
import { Title } from '../title/Title';

export class Teacher extends Entity {
  private _person: Person;
  private _types: number[];
  private _state: StateTeacher;
  private _titles: Title[];
  private _titleId?: number[];

  public get person(): Person {
    return this._person;
  }

  public set person(person: Person) {
    this._person = person;
  }

  public get types(): number[] {
    return this._types;
  }

  public set types(types: number[]) {
    this._types = types;
  }

  public get state(): StateTeacher {
    return this._state;
  }

  public set state(state: StateTeacher) {
    this._state = state;
  }

  public get titles(): Title[] {
    return this._titles;
  }

  public set titles(titles: Title[]) {
    this._titles = titles;
  }

  public get titleId(): number[] | undefined {
    return this._titleId;
  }

  public set titleId(titleId: number[] | undefined) {
    this._titleId = titleId;
  }

  constructor(teacher: ITeacherResponse = {} as ITeacherResponse) {
    super(teacher.id);
    this._person = new Person(teacher.person);
    const typesAux: number[] = [];
    if (teacher.types != null) {
      teacher.types.forEach((type) => {
        typesAux.push(type);
      });
    }
    this._types = typesAux;
    const titlesAux: Title[] = [];
    if (teacher.titles != null) {
      teacher.titles.forEach((type) => {
        titlesAux.push(new Title(type));
      });
    }
    this._titles = titlesAux;
    this.titleId = titlesAux.map((title) => title.id as number);
    this._state = Number.parseInt(teacher.state, 10);
  }

  public toJson(): ITeacher {
    return {
      person: this._person.toJson(),
      titles: this.titleId as number[],
    };
  }

  public fromJson(): ITeacher {
    return {
      id: this.id,
      person: this.person.fromJson(),
      state: this.state,
      types: this.types,
      titles: this.titleId as number[],
    };
  }

  public fromJsonResponse(): ITeacherResponse {
    return {
      id: this.id,
      person: this.person.fromJsonResponse(),
      state: this.state.toString(),
      types: this.types,
      titles: this.titles,
    };
  }

  public toJsonUpdate(): ITeacherResponse {
    // this.titleId = this.titles.map((title) => title.id as number);
    return {
      id: this.id,
      person: this.person.fromJsonResponse(),
      state: this.state.toString(),
      types: this.types,
      titles: this.titles,
      titles_id: this.titleId,
    };
  }

  public get typeTextTeacher(): string {
    return this.types.length === 0
      ? ''
      : this.types.length > 1
      ? `${TypeTextTeacher.PLANTA}, ${TypeTextTeacher.OCASIONAL}`
      : this.types[0] === Roles.TEACHER
      ? TypeTextTeacher.PLANTA
      : TypeTextTeacher.OCASIONAL;
  }
}
