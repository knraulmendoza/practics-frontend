export enum TypeTeacher {
  PLANTA = 1,
  OCASIONAL = 2,
}

export enum TypeTextTeacher {
  PLANTA = 'Docente Coordinador',
  OCASIONAL = 'Docente de práctica',
}
