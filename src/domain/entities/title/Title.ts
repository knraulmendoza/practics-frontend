import {Entity} from '../../base/BaseEntity';
import { ITitle } from '@/interfaces/interfaces';
import { ITitleResponse } from '@/interfaces/InterfaceResponse';

export class Title extends Entity {
    private _name: string;
    private _description?: string ;

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get description(): string| undefined {
        return this._description;
    }

    public set description(description: string| undefined) {
        this._description = description;
    }

    constructor(title: ITitleResponse = {} as ITitleResponse) {
        super(title.id);
        this._name = title.name;
        this._description = title.description;
    }

    public toJson(): ITitle {
        return{
            name: this._name,
            description: this._description,
        };
    }
}
