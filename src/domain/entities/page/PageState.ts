export enum PageState {
  ENABLE = 1,
  DISABLE = 2,
}

export enum PageStateText {
  ENABLE = 'Disponible',
  DISABLE = 'No disponible',
}
