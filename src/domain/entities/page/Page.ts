import { Entity } from '../../base/BaseEntity';
import { IPage } from '@/interfaces/interfaces';
import { Type } from './PageType';
import { PageState } from './PageState';

export class Page extends Entity {
  private _title: string;
  private _path: string;
  private _icon: string;
  private _type: Type;
  private _state: PageState;
  private _subGroup: Page[];
  private _parentPage?: any;

  public get title(): string {
    return this._title;
  }

  public set title(title: string) {
    this._title = title;
  }

  public get path(): string {
    return this._path;
  }

  public set path(path: string) {
    this._path = path;
  }

  public get icon(): string {
    return this._icon;
  }

  public set icon(icon: string) {
    this._icon = icon;
  }

  public get type(): Type {
    return this._type;
  }

  public set type(type: Type) {
    this._type = type;
  }

  public get state(): PageState {
    return this._state;
  }

  public set state(state: PageState) {
    this._state = state;
  }

  public get subGroup(): Page[] {
    return this._subGroup;
  }

  public set subGroup(subGroup: Page[]) {
    this._subGroup = subGroup;
  }

  public get parentPage(): any | undefined {
    return this._parentPage;
  }

  public set parentPage(parentPage: any | undefined) {
    this._parentPage = parentPage;
  }

  constructor(page: IPage = {} as IPage) {
    super(page.id);
    this._title = page.title;
    this._path = `/dashboard${page.path}`;
    this._icon = page.icon;
    this._type = page.type;
    this._state = page.state;
    this._subGroup = page.subGroup === undefined ? [] : page.subGroup.map((pageAux) => new Page(pageAux));
    this._parentPage = page.parent_page;
  }

  public toJson(): IPage {
    const pathAux = this.path.split('/');
    return {
      title: this.title,
      path: `/${pathAux[pathAux.length - 1]}`,
      icon: this.icon,
      state: this.state,
      type: this.type,
      subGroup: this.subGroup,
      parent_page: this.parentPage,
    };
  }
}
