export enum Type {
  WEB = 1,
  MOBILE = 2,
}

export enum TypeText {
  WEB = 'Página Web',
  MOBILE = 'Móvil',
}
