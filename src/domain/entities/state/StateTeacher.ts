export enum StateTeacher {
    ACTIVO = 1,
    INACTIVO = 0,
}

export enum StateTeacherText {
    ACTIVO = 'Activo',
    INACTIVO = 'Inactivo',
}
