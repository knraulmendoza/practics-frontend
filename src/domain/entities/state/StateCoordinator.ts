export enum StateCoordinator {
  ACTIVO = 1,
  INACTIVO = 0,
}

export enum StateCoordinatorText {
  ACTIVO = 'Activo',
  INACTIVO = 'Inactivo',
}
