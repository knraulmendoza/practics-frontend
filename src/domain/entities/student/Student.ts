import { Entity } from '../../base/BaseEntity';
import { Person } from '../person/Person';
import { IStudent, IStudentUpc } from '@/interfaces/interfaces';
import { IStudentResponse } from '@/interfaces/InterfaceResponse';

export class Student extends Entity {
  private _person: Person;

  public get person(): Person {
    return this._person;
  }

  public set person(person: Person) {
    this._person = person;
  }

  constructor(student: IStudentResponse = {} as IStudentResponse) {
    super(student.id);
    this._person = new Person(student.person);
  }
  public fromJson(studentUpc: IStudentUpc): Student {
    return new Student({
      person: {
        document: studentUpc.identificacion,
        document_type: studentUpc.tipo_documento.toString(),
        first_name: studentUpc.primer_nombre,
        second_name: studentUpc.segundo_nombre,
        first_last_name: studentUpc.primer_apellido,
        second_last_name: studentUpc.segundo_apellido,
        gender: studentUpc.genero?.toString(),
        address: studentUpc.correo,
        user_id: -1,
      },
    });
  }

  public toJson(): IStudent {
    return {
      person: this._person.toJson(),
    };
  }
}
