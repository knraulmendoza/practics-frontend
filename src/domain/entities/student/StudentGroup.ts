import { IGroupStudent, IGroup } from '@/interfaces/interfaces';
import { Group } from './Group';
import { Entity } from '@/domain/base/BaseEntity';
import { IGroupStudentResponse } from '@/interfaces/InterfaceResponse';

export class StudentGroup extends Entity {
  private _moduleId: number;
  private _groups: Group[];

  public get moduleId(): number {
    return this._moduleId;
  }

  public set moduleId(moduleId: number) {
    this._moduleId = moduleId;
  }

  public get groups(): Group[] {
    return this._groups;
  }

  public set groups(groups: Group[]) {
    this._groups = groups;
  }

  constructor(groupStudent: IGroupStudentResponse = {} as IGroupStudentResponse) {
    super(groupStudent.id);
    this._moduleId = groupStudent.module_id;
    const groupStudents: Group[] = [];
    groupStudent.groups.forEach((group) => {
      groupStudents.push(new Group(group));
    });
    this._groups = groupStudents;
  }

  public toJson(): IGroupStudent {
    const groupInterface: IGroup[] = [];
    this.groups.forEach((group) => {
      groupInterface.push(group.toJson());
    });
    return {
      groups: groupInterface,
      module_id: this.moduleId,
    };
  }
}
