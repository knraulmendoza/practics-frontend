import { Entity } from '@/domain/base/BaseEntity';
import { IGroupResponse } from '@/interfaces/InterfaceResponse';
import { IGroup } from '@/interfaces/interfaces';
import { Student } from './Student';

export class Group extends Entity {
  private _code?: string;
  private _name: string | null;
  private _moduleId: number;
  private _studentsDocument?: string[];
  private _students: Student[];

  public get code(): string | undefined {
    return this._code;
  }

  public set code(code: string | undefined) {
    this._code = code;
  }

  public get name(): string | null {
    return this._name;
  }

  public set name(name: string | null) {
    this._name = name;
  }

  public get moduleId(): number {
    return this._moduleId;
  }

  public set moduleId(moduleId: number) {
    this._moduleId = moduleId;
  }

  public get studentsDocument(): string[] | undefined {
    return this._studentsDocument;
  }

  public set studentsDocument(studentsDocument: string[] | undefined) {
    this._studentsDocument = studentsDocument;
  }

  public get students(): Student[] {
    return this._students;
  }
  constructor(group: IGroupResponse = {} as IGroupResponse) {
    super(group.id);
    this._code = group.code;
    this._name = group.name;
    this._moduleId = group.module_id;
    this._students = group.students !== undefined ? group.students.map((student) => new Student(student)) : [];
  }

  public toJson(): IGroup {
    return {
      students: this.students.map((student) => student.person.document) as string[],
      name: this.name as string,
      module_id: this.moduleId,
    };
  }

  public toJsonUpdate(): IGroup {
    return {
      students: this.students.map((student) => student.person.document) as string[],
      name: this.name as string,
      module_id: this.moduleId,
      code: this.code,
    };
  }
  // public toJsonResponse(): IGroupResponse {
  //     return {
  //         students: this.students.map((student) => {
  //             return {student.}
  //         }),
  //         name: this.name as string,
  //         module_id: this.moduleId,
  //     };
  // }
}

// tslint:disable-next-line:max-classes-per-file
export class Groups {
  private _groups: Group[];

  public get groups(): Group[] {
    return this._groups;
  }

  public set groups(groups: Group[]) {
    this._groups = groups;
  }
  constructor(groups: Group[] = []) {
    this._groups = groups;
  }

  public toJson(): IGroup[] {
    const groupInterface: IGroup[] = [];
    this.groups.forEach((group) => {
      groupInterface.push(group.toJson());
    });
    return groupInterface;
  }
}
