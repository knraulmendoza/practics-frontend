import { Entity } from '@/domain/base/BaseEntity';
import { IAnswersChecks } from '@/interfaces/interfaces';

export class AnswersChecks extends Entity {
  private _answer: number;
  private _question?: string;
  private _questionId: number;
  private _observation: string;

  public get answer(): number {
    return this._answer;
  }

  public set answer(answer: number) {
    this._answer = answer;
  }

  public get question(): string | undefined {
    return this._question;
  }

  public set question(question: string | undefined) {
    this._question = question;
  }

  public get questionId(): number {
    return this._questionId;
  }

  public set questionId(questionId: number) {
    this._questionId = questionId;
  }

  public get observation(): string {
    return this._observation;
  }

  constructor(ansWerCheck: IAnswersChecks = {} as IAnswersChecks) {
    super(ansWerCheck.id);
    this._answer = ansWerCheck.answer;
    this.conversionStringToNumber(ansWerCheck.answer);
    this._observation = ansWerCheck.observation;
    this._question = ansWerCheck.question;
    this._questionId = ansWerCheck.question_id;
  }

  public conversionStringToNumber(answer: string | number): void {
    if (typeof answer === 'string') {
      // eslint-disable-next-line radix
      this._answer = Number.parseInt(answer);
    }
  }

  public toJson(): IAnswersChecks {
    return {
      answer: this.answer,
      question: this.question,
      question_id: this.questionId,
      observation: this.observation,
    };
  }
}
