import { Entity } from '@/domain/base/BaseEntity';
import { IRotation } from '@/interfaces/interfaces';
import { RotationType } from './RotationType';

export default class Rotation extends Entity {
  private _startDate: string;
  private _endDate: string;
  private _type: number;
  private _text: string | undefined;

  public get startDate(): string {
    return this._startDate;
  }

  public set startDate(startDate: string) {
    this._startDate = startDate;
  }

  public get endDate(): string {
    return this._endDate;
  }

  public set endDate(endDate: string) {
    this._endDate = endDate;
  }

  public get type(): number {
    return this._type;
  }

  public set type(type: number) {
    this._type = type;
  }

  public get text(): string | undefined {
    return this._text;
  }

  public set text(text: string | undefined) {
    this._text = text;
  }

  constructor(rotation: IRotation = {} as IRotation) {
    super(rotation.id);
    this._startDate = rotation.start_date;
    this._endDate = rotation.end_date;
    this._type =
      rotation.type === undefined ? RotationType.THREE : Number.parseInt(rotation.type as unknown as string, 10);
    this._text = rotation.text;
  }

  public toJson(): IRotation {
    return {
      id: this.id,
      start_date: this.startDate,
      end_date: this.endDate,
      type: this.type,
    };
  }
}
