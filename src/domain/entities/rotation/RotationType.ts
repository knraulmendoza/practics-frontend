export enum RotationType {
  FULL = 1,
  TWO = 2,
  THREE = 3,
}

export enum RotationTypeText {
  FULL = 'Rotación completa',
  TWO = 'Dos rotaciones',
  THREE = 'Tres rotaciones',
}
