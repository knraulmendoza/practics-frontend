import { Entity } from '../../base/BaseEntity';
import { IRole } from '@/interfaces/interfaces';

export class Role extends Entity {
    private _name: string;
    private _description?: string;

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get description(): string| undefined {
        return this._description;
    }

    public set description(description: string| undefined) {
        this._description = description;
    }

    constructor(role: IRole = {} as IRole) {
        super(role.id);
        this._name = role.name;
        this._description = role.description;
    }

    public toJson(): IRole {
        return{
            name: this._name,
            description: this._description,
        };
    }
}
