import { Entity } from '../../base/BaseEntity';
import { Role } from '../role/Rol';
import { IUser, IRole } from '@/interfaces/interfaces';
import { IUserResponse } from '@/interfaces/InterfaceResponse';

export class User extends Entity {
  private _user: string;
  private _password: string;
  private _userParent?: number;
  private _roles: Role[];
  private _rolId?: number;
  private _statePassword: number;

  public get user(): string {
    return this._user;
  }

  public set user(user: string) {
    this._user = user;
  }

  public get password(): string {
    return this._password;
  }

  public set password(password: string) {
    this._password = password;
  }

  public get userParent(): number | undefined {
    return this._userParent;
  }

  public set userParent(userParent: number | undefined) {
    this._userParent = userParent;
  }

  public get roles(): Role[] {
    return this._roles;
  }

  public set roles(roles: Role[]) {
    this._roles = roles;
  }

  public get rolId(): number | undefined {
    return this._rolId;
  }

  public set rolId(rolId: number | undefined) {
    this._rolId = rolId;
  }

  public get statePassword(): number {
    return this._statePassword;
  }

  constructor(user: IUserResponse = {} as IUserResponse) {
    super(user.id);
    this._user = user.user;
    this._password = user.password;
    this._userParent = user.user_parent;
    this._statePassword = user.state_password;
    this._roles = [];
    if (user.roles !== undefined) {
      this._roles = (user.roles as IRole[]).map((role: IRole) => new Role(role));
    }
  }

  public toJson(): IUser {
    return {
      user: this._user,
      password: this._password,
      rol: this._rolId as number,
    };
  }
}
