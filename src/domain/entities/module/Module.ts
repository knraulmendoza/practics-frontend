import { Entity } from '@/domain/base/BaseEntity';
import { IModule } from '@/interfaces/interfaces';
import { IModuleResponse } from '@/interfaces/InterfaceResponse';

export class Module extends Entity {
  private _name: string;
  private _code: string;
  private _descrption?: string;
  private _subjectCode?: string;
  private _subjectId?: number;
  private _subject: string;

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get code(): string {
    return this._code;
  }

  public set code(code: string) {
    this._code = code;
  }

  public get description(): string | undefined {
    return this._descrption;
  }

  public set description(description: string | undefined) {
    this._descrption = description;
  }

  public get subjectCode(): string | undefined {
    return this._subjectCode;
  }

  public set subjectCode(subjectCode: string | undefined) {
    this._subjectCode = subjectCode;
  }

  public get subject(): string {
    return this._subject;
  }

  public set subject(subject: string) {
    this._subject = subject;
  }

  constructor(module: IModuleResponse = {} as IModuleResponse) {
    super(module.id);
    this._code = module.code;
    this._name = module.module || (module.name as string);
    this._descrption = module.description;
    this._subjectCode = module.subject_code;
    this._subject = module.subject as string;
  }

  public toJson(): IModule {
    return {
      name: this.name,
      code: this.code,
      description: this.description,
      subject_code: this.subjectCode as string,
    };
  }

  public toJsonResponse(): IModuleResponse {
    return {
      id: this.id,
      module: this.name,
      code: this.code,
      description: this.description as string,
      subject_code: this.subjectCode as string,
      subject: this.subject as string,
    };
  }
}
