export enum AcademicLevel {
    PRIMARYSCHOOL = 1,
    HIGHSCHOOL = 2,
    UNDERGRADUATE = 3,
}

export enum AcademicLevelText {
    PRIMARYSCHOOL = 'primaria',
    HIGHSCHOOL = 'secundaria',
    UNDERGRADUATE = 'pregrado',
}
