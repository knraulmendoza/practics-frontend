import { Entity } from '../../base/BaseEntity';
import { DocumentType, DocumentTypeText } from '../person/DocumentType';
import { Gender } from '../person/Gender';
import { IPatient } from '@/interfaces/interfaces';
import { Attention } from '../attention/Attention';

export class Patient extends Entity {
  private _document: string;
  private _documentType: DocumentType;
  private _name: string;
  private _phone?: string;
  private _age?: number;
  private _status?: number;
  private _gender?: Gender;
  private _academicLevel?: number;
  private _employmentSituation?: number;
  private _origin?: string;
  private _attentions: Attention[];
  private _date: Date;

  public get document(): string {
    return this._document;
  }

  public set document(document: string) {
    this._document = document;
  }

  public get documentType(): DocumentType {
    return this._documentType;
  }

  public set documentType(documentType: DocumentType) {
    this._documentType = documentType;
  }

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get phone(): string | undefined {
    return this._phone;
  }

  public set phone(phone: string | undefined) {
    this._phone = phone;
  }

  public get age(): number | undefined {
    return this._age;
  }

  public set age(age: number | undefined) {
    this._age = age;
  }

  public get gender(): Gender | undefined {
    return this._gender;
  }

  public set gender(gender: Gender | undefined) {
    this._gender = gender;
  }

  public get status(): number | undefined {
    return this._status;
  }

  public set status(status: number | undefined) {
    this._status = status;
  }

  public get academicLevel(): number | undefined {
    return this._academicLevel;
  }

  public set academicLevel(academicLevel: number | undefined) {
    this._academicLevel = academicLevel;
  }

  public get employmentSituation(): number | undefined {
    return this._employmentSituation;
  }

  public set employmentSituation(employmentSituation: number | undefined) {
    this._employmentSituation = employmentSituation;
  }

  public get origin(): string | undefined {
    return this._origin;
  }

  public set origin(origin: string | undefined) {
    this._origin = origin;
  }

  public get attentions(): Attention[] {
    return this._attentions;
  }

  public set attentions(attentions: Attention[]) {
    this._attentions = attentions;
  }

  public get date(): Date {
    return this._date;
  }

  public set date(date: Date) {
    this._date = date;
  }

  constructor(patient: IPatient = {} as IPatient) {
    super(patient.id);
    this._document = patient.document;
    this._documentType = patient.document_type;
    this._name = patient.name;
    this._age = patient.age;
    this._phone = patient.phone;
    this._status = patient.status;
    this._academicLevel = patient.academic_level;
    this._employmentSituation = patient.employment_situation;
    this._gender = patient.gender;
    this._attentions = [];
    if (patient.attentions !== undefined) {
      this._attentions = patient.attentions.map((attention) => new Attention(attention));
    }
    this._date = patient.created_at;
  }

  //   public toJson(): IPatient {
  //     return {
  //       document: this.document,
  //       document_type: this.documentType,
  //       name: this.name,
  //       academic_level: this.academicLevel,
  //       age: this.age,
  //       employment_situation: this.employmentSituation,
  //       gender: this.gender,
  //       origin: this.origin,
  //       phone: this.phone,
  //       status: this.status,
  //     };
  //   }

  public get documentTypeText(): string {
    switch (this.documentType) {
      case DocumentType.CEDULA:
        return DocumentTypeText.CEDULA;
      case DocumentType.TARJETA_IDENTIDAD:
        return DocumentTypeText.TARJETA_IDENTIDAD;
      case DocumentType.CEDULA_EXTRANJERA:
        return DocumentTypeText.CEDULA_EXTRANJERA;
    }
    return '';
  }
}
