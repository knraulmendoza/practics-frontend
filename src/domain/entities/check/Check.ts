import { Entity } from '@/domain/base/BaseEntity';
import { Person } from '../person/Person';
import { ICheck } from '@/interfaces/interfaces';
import { ICheckResponse } from '@/interfaces/InterfaceResponse';

export class Check extends Entity {
  private _date: Date;
  private _evaluatedId?: number;
  private _evaluated: Person;
  private _evalutorId?: number;
  private _evaluator: Person;
  private _observation: string;

  public get date(): Date {
    return this._date;
  }

  public set date(date: Date) {
    this._date = date;
  }

  public get evaluatedId(): number | undefined {
    return this._evaluatedId;
  }

  public set evaluatedId(evaluatedId: number | undefined) {
    this._evaluatedId = evaluatedId;
  }

  public get evaluatorId(): number | undefined {
    return this._evalutorId;
  }

  public set evaluatorId(evaluatorId: number | undefined) {
    this._evalutorId = evaluatorId;
  }

  public get evaluated(): Person {
    return this._evaluated;
  }

  public set evaluated(evaluated: Person) {
    this._evaluated = evaluated;
  }

  public get evaluator(): Person {
    return this._evaluator;
  }

  public set ealuator(evaluator: Person) {
    this._evaluator = evaluator;
  }

  public get observation(): string {
    return this._observation;
  }

  public set observation(observation: string) {
    this._observation = observation;
  }
  constructor(check: ICheckResponse = {} as ICheckResponse) {
    super(check.id);
    this._date = check.date;
    this._evaluated = new Person(check.evaluated);
    this._evaluator = new Person(check.evaluator);
    this._observation = check.observation;
  }

  public toJson(): ICheck {
    return {
      date: this.date,
      evaluated_id: this.evaluatedId as number,
      evaluator_id: this.evaluatorId as number,
      observation: this.observation,
      // evaluated = this.evaluated?.toJson(),
    };
  }
}
