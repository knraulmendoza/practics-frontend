import { IReportMetaResponse } from '@/interfaces/InterfaceResponse';
import { IReportMeta } from '@/interfaces/interfaces';

export class ReportMeta {
  private _key: string;
  private _value: number;
  private _tabla?: string;

  public get key(): string {
    return this._key;
  }

  public set key(key: string) {
    this._key = key;
  }

  public get value(): number {
    return this._value;
  }

  public set value(value: number) {
    this._value = value;
  }

  public get tabla(): string | undefined {
    return this._tabla;
  }

  public set tabla(tabla: string | undefined) {
    this._tabla = tabla;
  }

  constructor(reportMeta: IReportMetaResponse = {} as IReportMetaResponse) {
    this._key = reportMeta.key;
    this._value = reportMeta.value;
    this._tabla = reportMeta.tabla;
  }

  public toJson(): IReportMeta {
    return {
      key: this.key,
      value: this.value,
      tabla: this.tabla as string,
    };
  }
}
