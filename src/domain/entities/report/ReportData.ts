import { IReportDataResponse } from '@/interfaces/InterfaceResponse';

export class ReportData {
  private _name: string;
  private _total: number;
  private _percentage: number;

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get total(): number {
    return this._total;
  }

  public set total(total: number) {
    this._total = total;
  }

  public get percentage(): number {
    return this._percentage;
  }

  public set percentage(percentage: number) {
    this._percentage = percentage;
  }

  constructor(reportMeta: IReportDataResponse = {} as IReportDataResponse) {
    this._name = reportMeta.name;
    this._total = reportMeta.total;
    this._percentage = reportMeta.percentage;
  }

  public toJson(): IReportDataResponse {
    return {
      name: this.name,
      total: this.total,
      percentage: this.percentage,
    };
  }
}
