export enum TypeReport {
  ATTENTIONSBYSUBJECT = 1,
  ATTENTIONSBYMODULE = 2,
  ASSISTENCESBYSUBJECT = 3,
  CHECKSBYROL = 4,
  ASSISTENCESBYSCENARIO = 5,
  PATIENTPOLLS = 6,
}

export enum TypeReportText {
  ATTENTIONSBYSUBJECT = 'Atenciones por asignatura',
  ATTENTIONSBYMODULE = 'Atenciones por módulo',
  ASSISTENCESBYSUBJECT = 'Asistencias por asignatura',
  CHECKSBYROL = 'Chequeos por rol',
  ASSISTENCESBYSCENARIO = 'Atenciones por Escenario de práctica',
  PATIENTPOLLS = 'Encuestas realizadas a pacientes',
}
