import { Entity } from '@/domain/base/BaseEntity';
import { IReportResponse } from '@/interfaces/InterfaceResponse';
import { ReportData } from './ReportData';
import { ReportMeta } from './ReportMeta';
import { TypeChart } from './TypeChart';

export class Report extends Entity {
  private _title: string;
  private _description?: string;
  private _image: string;
  private _reportCategory: number;
  private _reportMetas?: ReportMeta[];
  private _reportDatas?: ReportData[];
  private _academicPeriod?: string;
  private _academicPeriodId?: number;
  private _chart?: TypeChart;

  public get title(): string {
    return this._title;
  }

  public set title(title: string) {
    this._title = title;
  }

  public get description(): string | undefined {
    return this._description;
  }

  public set description(description: string | undefined) {
    this._description = description;
  }

  public get image(): string {
    return this._image;
  }

  public set image(image: string) {
    this._image = image;
  }

  public get reportCategory(): number {
    return this._reportCategory;
  }

  public set reportCategory(reportCategory: number) {
    this._reportCategory = reportCategory;
  }

  public get reportMetas(): ReportMeta[] | undefined {
    return this._reportMetas;
  }

  public set reportMetas(reportMetas: ReportMeta[] | undefined) {
    this._reportMetas = reportMetas;
  }

  public get reportDatas(): ReportData[] | undefined {
    return this._reportDatas;
  }

  public set reportDatas(reportDatas: ReportData[] | undefined) {
    this._reportDatas = reportDatas;
  }

  public get academicPeriod(): string | undefined {
    return this._academicPeriod;
  }

  public get academicPeriodId(): number | undefined {
    return this._academicPeriodId;
  }

  public get chart(): TypeChart | undefined {
    return this._chart;
  }

  public set chart(chart: TypeChart | undefined) {
    this._chart = chart;
  }

  constructor(report: IReportResponse = {} as IReportResponse) {
    super(report.id);
    this._title = report.title;
    this._description = report.description;
    this._image = report.image;
    this._reportCategory = report.report_category;
    this._academicPeriod = report.academic_period;
    this._academicPeriodId = report.academic_period_id;
    if (report.report_metas !== undefined) {
      this._reportMetas = report.report_metas.map((reportMeta) => new ReportMeta(reportMeta));
    }
    if (report.report_datas !== undefined) {
      this._reportDatas = report.report_datas.map((reportData) => new ReportData(reportData));
    }
  }

  public toJsonResponse(): IReportResponse {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      image: this.image,
      report_category: this.reportCategory,
      report_metas: this.reportMetas as ReportMeta[],
      report_datas: this.reportDatas as ReportData[],
    };
  }
}
