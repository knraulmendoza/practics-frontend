import { ISelect } from '@/interfaces/interfaces';
import { Type, TypeText } from '../question/Type';
import { CheckTypeText } from '../question/ChechkType';
import { GenderText, Gender } from '../person/Gender';
import { DocumentTypeText, DocumentType } from '../person/DocumentType';
import { Status } from '../patient/Status';
import { AcademicLevelText, AcademicLevel } from '../patient/AcademicLevel';
import { TypeTeacher, TypeTextTeacher } from '../teacher/TypeTeacher';

export class Select {

    public static select(enumType: any, enumText: any): ISelect[] {
        const select: ISelect[] = [];
        for (const n in enumType) {
            if (typeof enumType[n] === 'number') {
            select.push({value: enumType[n], text: enumText[n]});
            }
        }
        return select;
    }
    public static get itemsTypeQuestions(): ISelect[] {
        return [
            {text: TypeText.CHEQUEO, value: Type.CHEQUEO},
            {text: TypeText.ENCUESTA, value: Type.ENCUESTA},
        ];
    }
    public static get itemsTypeChecks(): ISelect[] {
        return [
            {text: CheckTypeText.STUDENTS, value: 1},
            {text: CheckTypeText.TEACHERS, value: 2},
        ];
    }
    public static get itemsGenders(): ISelect[] {
        return [
            { text: GenderText.MASCULINE, value: Gender.MASCULINE },
            { text: GenderText.FEMININE, value: Gender.FEMININE },
            { text: GenderText.OTHER, value: Gender.OTHER },
        ];
    }

    public static get itemsTypeDocuments(): ISelect[] {
        return [
            { text: DocumentTypeText.CEDULA, value: DocumentType.CEDULA },
            { text: DocumentTypeText.TARJETA_IDENTIDAD, value: DocumentType.TARJETA_IDENTIDAD },
            { text: DocumentTypeText.CEDULA_EXTRANJERA, value: DocumentType.CEDULA_EXTRANJERA },
        ];
    }

    public static get itemsStatus(): ISelect[] {
        return [
            { text: Status.STATUS1.toString(), value: Status.STATUS1 },
            { text: Status.STATUS2.toString(), value: Status.STATUS2 },
            { text: Status.STATUS3.toString(), value: Status.STATUS3 },
            { text: Status.STATUS4.toString(), value: Status.STATUS4 },
            { text: Status.STATUS5.toString(), value: Status.STATUS5 },
            { text: Status.STATUS6.toString(), value: Status.STATUS6 },
        ];
    }

    public static get itemsAcademicLevel(): ISelect[] {
        return [
            { text: AcademicLevelText.PRIMARYSCHOOL, value: AcademicLevel.PRIMARYSCHOOL },
            { text: AcademicLevelText.HIGHSCHOOL, value: AcademicLevel.HIGHSCHOOL },
            { text: AcademicLevelText.UNDERGRADUATE, value: AcademicLevel.UNDERGRADUATE },
        ];
    }

    public static get itemsTypesTeachers(): ISelect[] {
        return [
            { text: TypeTextTeacher.OCASIONAL, value: TypeTeacher.OCASIONAL },
            { text: TypeTextTeacher.PLANTA, value: TypeTeacher.PLANTA },
        ];
    }
}
