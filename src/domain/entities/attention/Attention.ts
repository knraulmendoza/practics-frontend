import { Entity } from '@/domain/base/BaseEntity';
import { Patient } from '../patient/Patient';
import { IAttention } from '@/interfaces/interfaces';

export class Attention extends Entity {
  private _patient: Patient;
  private _procedures: number[];
  private _state: number;
  private _date: Date;
  private _encuesta: string;

  public get patient(): Patient {
    return this._patient;
  }

  public set patient(patient: Patient) {
    this._patient = patient;
  }

  public get procedures(): number[] {
    return this._procedures;
  }

  public set procedures(procedures: number[]) {
    this._procedures = procedures;
  }

  public get state(): number {
    return this._state;
  }

  public set state(state: number) {
    this._state = state;
  }

  public get date(): Date {
    return this._date;
  }

  public set date(date: Date) {
    this._date = date;
  }

  public get encuesta(): string {
    return this._encuesta;
  }

  constructor(attention: IAttention = {} as IAttention) {
    super(attention.id);
    this._patient = new Patient(attention.patient);
    this._procedures = attention.procedures;
    this._date = attention.date;
    this._state = attention.state;
    this._encuesta = attention.encuesta;
  }

  // public toJson(): IAttention {
  //     return {
  //         patient: this.patient.toJson(),
  //         procedures: this.procedures,
  //         date: this.date,
  //     };
  // }
}
