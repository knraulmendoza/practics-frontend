import { Entity } from '@/domain/base/BaseEntity';
import { ISubject, ISubjectUpc } from '@/interfaces/interfaces';

export class Subject extends Entity {
  private _name: string;
  private _code: string;

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get code(): string {
    return this._code;
  }

  public set code(code: string) {
    this._code = code;
  }

  constructor(subject: ISubject = {} as ISubject) {
    super(subject.id);
    this._name = subject.name;
    this._code = subject.code;
  }

  public fromJson(subject: ISubjectUpc): Subject {
    return new Subject({
      code: subject.codigo,
      name: subject.nombre,
    });
  }

  public toJson(): ISubject {
    return {
      name: this.name,
      code: this.code,
    };
  }
}
