abstract class BaseEntity {
  // tslint:disable-next-line:variable-name
  private _createdAt: Date;
  // tslint:disable-next-line:variable-name
  private _updatedAt: Date;

  public get createdAt(): Date {
    return this._createdAt;
  }

  public get updatedAt(): Date {
    return this._updatedAt;
  }

  constructor(createdAt: Date = new Date(), updatedAt: Date = new Date()) {
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }
}

// tslint:disable-next-line:max-classes-per-file
export abstract class Entity extends BaseEntity {
  // tslint:disable-next-line:variable-name
  private readonly _id: number | undefined;

  public get id(): number | undefined {
    return this._id;
  }

  constructor(id?: number) {
    super();
    this._id = id;
  }
}
