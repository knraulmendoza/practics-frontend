import { api } from '@/helpers/api';
import { ISelect, ISubject } from '@/interfaces/interfaces';
import { ResponseApi } from '@/helpers/ResponseApi';
import { Subject } from '@/domain/entities/subject/Subject';
import { globalSer } from './globalService';
class SubjectService {
  public async save(subject: Subject): Promise<ResponseApi> {
    return await api.post('subject', subject.toJson());
  }

  public async update(subject: Subject): Promise<ResponseApi> {
    return await api.put(`subject/${subject.code}`, subject.toJson());
  }

  public async getAll(): Promise<ResponseApi> {
    return await api.get('subject');
  }

  public async getSubjectSelect(): Promise<ISelect[]> {
    const subjects: ISelect[] = [];
    const response = await api.get(`career/subjects`);
    response.data.data.subjects.forEach((res: ISubject) => {
      subjects.push({
        text: res.name as string,
        value: res.code,
      });
    });
    return subjects;
  }
  public async getSubjects(): Promise<ResponseApi> {
    return await api.get(`career/subjects`);
  }
  public async getAllUpc() {
    const career = globalSer.getCurrentUser().career;
    const code = career !== null && career !== undefined ? career.code : '';
    if (process.env.VUE_APP_ENV === 'local') {
      const resp = await api.get(`/asignaturas?carrera=${code}`, { upc: true });
      return resp !== null && resp !== undefined ? resp[0] : resp;
    }
    return await api.get(`upc/subjectAll/${code}`, { upc: true });
  }

  public async getModuleBySubject(subject: string): Promise<ResponseApi> {
    return await api.get(`subject/modules/${subject}`);
  }

  public async getAsignature(code) {
    if (process.env.VUE_APP_ENV === 'local') {
      const resp = await api.get(`/asignatura?codigo=${code}`, { upc: true });
      return resp !== null && resp !== undefined ? resp[0] : resp;
    }
    return await api.get(`upc/subject/${code}`, { upc: true });
  }
}
export const SubjectSer = new SubjectService();
