import { api } from '@/helpers/api';
import { EventNew } from '@/domain/entities/event/Event';
import { ResponseApi } from '@/helpers/ResponseApi';
class EventService {
  public async post(event): Promise<ResponseApi> {
    return await api.post(`event`, event);
  }

  public async getAll(params?: any): Promise<ResponseApi> {
    return await api.get(`event`, { params });
  }

  public async getEvent(id: number): Promise<ResponseApi> {
    return await api.get(`event/${id}`);
  }

  public async put(event: EventNew): Promise<ResponseApi> {
    return await api.put(`event/${event.id}`, event.toJson());
  }
}
export const eventService = new EventService();
