import { Career } from '@/domain/entities/career/Career';
import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';
import { ISelect } from '@/interfaces/interfaces';

class CareerService {
  public async getSelectCareers() {
    return await api.getSelect('career');
  }
  public async getAll() {
    return await api.get(`career`);
  }
  public async getCareer(code) {
    if (process.env.VUE_APP_ENV === 'local') {
      const resp = await api.get(`upc/programa?codigo=${code}`);
      return resp !== null && resp !== undefined ? resp[0] : resp;
    }
    return await api.get(`upc/program/${code}`, { upc: true });
  }
  public async getAllFacultiesSelect() {
    const select: ISelect[] = [];
    const response = await api.get('faculty');
    response.data.data.faculties.forEach((data: any) => {
      select.push({ text: data.name, value: data.code });
    });
    return select;
  }
  public async getAllFaculties() {
    return await api.get('faculty');
  }
  public async getAllByFaculty(faculty: any) {
    return await api.get(`career/faculty/${faculty}`);
  }
  public async getAllUpc(facultad: any) {
    if (process.env.VUE_APP_ENV === 'local') {
      const resp = await api.get(`programas?facultad=${facultad}`);
      return resp !== null && resp !== undefined ? resp[0] : resp;
    }
    return await api.get(`upc/programs/${facultad}`, { upc: true });
  }
  public async getAllFacultiesUpc() {
    return await api.get('upc/faculties', { upc: true });
  }
  public async post(career: Career, faculty: string) {
    return await api.post(`career`, {
      ...career.toJson(),
      faculty,
    });
  }
  public async postFaculties(faculty: ISelect) {
    return await api.post(`faculty`, {
      name: faculty.text,
      code: faculty.value,
    });
  }
  public async update(career: Career, id: number): Promise<ResponseApi> {
    return await api.put(`career/${id}`, career.toJson());
  }
}

export const careerService = new CareerService();
