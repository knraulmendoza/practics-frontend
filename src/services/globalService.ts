import sweetalert from 'sweetalert2';
import { ICareer, ISelect } from '@/interfaces/interfaces';
import { Roles } from '@/route/Roles';
import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';

class GlobalServices {
  public extensionApiUpc(): string {
    return process.env.VUE_APP_ENV === 'local' ? '' : '.jsp';
  }

  public alert(title: string, icon: 'success' | 'error', text?: string) {
    sweetalert.fire({
      title,
      html: text,
      customClass: {
        content: 'htmlContentAlert',
      },
      icon,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return true;
      },
    });
  }

  public loading(message: string) {
    sweetalert.fire({
      width: 600,
      padding: '3em',
      html: '<img src="../../assets/images/loading.gif" alt="">',
      background: '#fff url(../../assets/images/loading.gif)',
      title: message,
      showConfirmButton: false,
    });
  }

  public closeAlert() {
    sweetalert.close();
  }

  public getCurrentUser(): ICurrentUSer {
    const currentUser: any = localStorage.getItem('currentUser');
    return JSON.parse(currentUser) as ICurrentUSer;
  }

  public getNameRol() {
    switch (this.getCurrentUser().rol) {
      case Roles.ADMIN:
        return 'Administrador';
      case Roles.COORDINATOR:
        return 'Coordinador';
      case Roles.TEACHER:
        return 'Profesor de planta';
      case Roles.TEACHER_PRACTICT:
        return 'Profesor de práctica';
      case Roles.STUDENT:
        return 'Estudiante';
      default:
        return '';
    }
  }

  public getNameCareer() {
    const careers = localStorage.getItem('careers');
    return (
      (JSON.parse(careers as string) ?? []).find((career: any) => career.id === globalSer.getCurrentUser().career?.id)
        ?.name ?? ''
    );
  }

  public select(enumValue: any, enumText: any): ISelect[] {
    const select: ISelect[] = [];
    for (const n in enumValue) {
      if (typeof enumValue[n] === 'number') {
        select.push({ value: enumValue[n], text: enumText[n] });
      }
    }
    return select;
  }

  public async getPeriodAcademic(): Promise<ResponseApi> {
    return await api.get('periodAcademic');
  }

  public errorValidation(errors: string[], show = true) {
    let text = '';
    errors.forEach((error) => {
      text += `${error} <br />`;
    });
    if (show) {
      this.error(text);
    }
    return text;
  }

  public error(message: string) {
    this.alert('Hubo un error', 'error', message);
  }

  public success(message: string) {
    this.alert('Excelente', 'success', message);
  }
}

export const globalSer = new GlobalServices();
export interface ICurrentUSer {
  id: number;
  user: string;
  user_parent: number;
  career?: ICareer;
  rol?: number;
}
