import { api } from '@/helpers/api';

class AssistenceService {
  public async assistenceBySubject(subject: number, academicPeriod: number) {
    return await api.post(`assistence/assistencesBySubject`, { subject, academicPeriod });
  }
  public async assistenceByCareer(academicPeriod: number) {
    return await api.post(`assistence/assistencesByCareer`, { academicPeriod });
  }
}

export const assistenceService = new AssistenceService();
