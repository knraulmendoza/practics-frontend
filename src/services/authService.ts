import { api } from '@/helpers/api';
import { User } from '@/domain/entities/user/User';
import { Roles } from '@/route/Roles';
import { globalSer } from './globalService';

class AuthService {
  public async authenticate(user: User) {
    return await api.post(`auth/authenticate`, user.toJson());
  }

  public async getUser() {
    return await api.get('auth/getToken');
  }

  public async logout() {
    return await api.get(`auth/logout`);
  }

  public async findUser(user: string, rol?: number) {
    if (rol !== Roles.TEACHER_PRACTICT) {
      user += '@unicesar.edu.co';
    }
    return await api.get(`auth/find/${user}`);
  }

  public async securityQuestions() {
    return await api.getSelect(`securityQuestion/getAll`);
  }

  public async addSecurityQuestions(question: string) {
    return await api.post(`securityQuestion/add`, { question });
  }

  public async updateSecurityQuestions(question: string, id: number) {
    return await api.put(`securityQuestion/update/${id}`, { question });
  }

  public async deleteSecurityQuestions(id: number) {
    return await api.delete(`securityQuestion/delete/${id}`);
  }

  public async add(user: User) {
    user.user = user.user != null && !user.user.includes('@') ? (user.user += '@unicesar.edu.co') : user.user;
    return await api.post('auth/create', user.toJson());
  }

  public async updatedPassword(
    newPassword: string,
    securityAnswer: string,
    securityQuestionId?: number,
    userId?: number
  ) {
    return await api.put(`auth/${userId !== undefined ? userId : globalSer.getCurrentUser().id}`, {
      password: newPassword,
      security_question_id: securityQuestionId,
      security_answer: securityAnswer,
    });
  }

  public async verifyUser(user: string, document: string, securityQuestionId: number, securityAnswer: string) {
    return await api.post(`auth/verifyUser`, {
      user,
      document,
      security_question_id: securityQuestionId,
      security_answer: securityAnswer,
    });
  }
  public async verifyUserUpc(user: string, rol?: Roles | undefined) {
    try {
      let response: any;
      let verify = false;
      if (Roles.STUDENT !== rol) {
        user = !user.includes('@') ? (user += '@unicesar.edu.co') : user;
      }
      if (process.env.VUE_APP_ENV === 'local') {
        response = await api.get(`users?user=${user}`, { upc: true });
        response = response !== null && response !== undefined ? response[0] : response;
      } else {
        response = await api.get(`upc/verifyUser/${user}`, { upc: true });
      }
      if (response !== undefined && response !== null) {
        verify = response.respuesta;
      }
      return verify;
    } catch (error) {
      return false;
    }
  }
}

export const authSer = new AuthService();
