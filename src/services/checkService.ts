import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';
import { Roles } from '@/route/Roles';

class CheckService {
  public async getAll(): Promise<ResponseApi> {
    return await api.get('check');
  }
  public async getAllPaginate(page = 1): Promise<ResponseApi> {
    return await api.get(`check/paginate?page=${page}`);
  }

  public async checks(evaluated): Promise<ResponseApi> {
    return await api.get(`check/checks/${evaluated}`);
  }

  public async checksByRotation(body: any): Promise<ResponseApi> {
    return await api.post(`check/checksByRotation`, body);
  }

  public async checksByRol(rol: number, body: any): Promise<ResponseApi> {
    return await api.post(`check/checksBy${rol === Roles.STUDENT ? 'Student' : 'Teacher'}`, body);
  }

  public async search(search: any): Promise<ResponseApi> {
    return await api.post(`check/search`, search);
  }
}

export const checkService = new CheckService();
