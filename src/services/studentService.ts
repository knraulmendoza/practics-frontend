import { DocumentType } from '@/domain/entities/person/DocumentType';
import { Gender } from '@/domain/entities/person/Gender';
import { api } from '@/helpers/api';
import { IStudentResponse } from '@/interfaces/InterfaceResponse';
import { IStudentUpc } from '@/interfaces/interfaces';
export interface IstudentGroup {
  student: IStudentResponse;
  group: any;
  edit: boolean;
}
class StudentService {
  public async getAll(page = 1) {
    return await api.get(`career/students?page=${page}`);
  }

  public async getAllUpc(subject: string): Promise<IstudentGroup[]> {
    const students: IstudentGroup[] = [];
    let response: any;
    if (process.env.VUE_APP_ENV === 'local') {
      response = await api.get(`estudiantesAsignaturas?asignatura=${subject}`);
      response = response !== null && response !== undefined ? response[0] : response;
    } else {
      response = await api.get(`upc/studentSubject/${subject}`, { upc: true });
    }
    if (response) {
      console.log(response);

      response.data.forEach((res: IStudentUpc) => {
        students.push({
          student: {
            person: {
              document: res.identificacion,
              document_type: res.tipo_documento ? res.tipo_documento.toString() : DocumentType.CEDULA.toString(),
              first_name: res.primer_nombre,
              second_name: res.segundo_nombre,
              first_last_name: res.primer_apellido,
              second_last_name: res.segundo_apellido,
              gender: res.genero?.toString() || Gender.MASCULINE.toString(),
              address: res.correo,
              user_id: -1,
            },
          },
          group: null,
          edit: false,
        });
      });
    }

    return students;
  }

  public async saveStudents(students: IstudentGroup[]) {
    return await api.post(`student/studentsSave`, { students }); // objeto students
  }

  public async getFullData(id, academicPeriod) {
    return await api.get(`student/assignment/${id}/${academicPeriod}`);
  }

  public async studentsByScenario(scenarioId: number, page?: number) {
    return await api.get(`student/scenario/${scenarioId}?page=${page}`);
  }

  public async studentWithPoll(module: number, academicPeriod: number) {
    return await api.post(`student/patientPoll`, { module, academic_period: academicPeriod });
  }

  public async postListStudent(students: IstudentGroup[]) {
    return await api.post(`student/studentsSave`, students);
  }
}
export const studentService = new StudentService();
