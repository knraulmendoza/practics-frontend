import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';
import Rotation from '@/domain/entities/rotation/Rotation';
import { IRotation, ISelect } from '@/interfaces/interfaces';
class RotationService {
  public async save(rotations: Rotation[]): Promise<ResponseApi> {
    return await api.post('rotation', { rotations: rotations.map((rotation) => rotation.toJson()) });
  }
  public async getAll(): Promise<ResponseApi> {
    return await api.get('rotation');
  }

  public async getRotationsByRol(personId: number): Promise<ISelect[]> {
    const rotations: ISelect[] = [];
    const response = await api.get(`rotation/getRotationsByRol/${personId}`);
    response.data.data.rotations.forEach((res: IRotation, i) => {
      rotations.push({
        text: `Rotación ${i + 1} (${res.start_date} a ${res.end_date})`,
        value: res.id as number,
      });
    });
    return rotations;
  }

  public async update(rotations: Rotation[]): Promise<ResponseApi> {
    return await api.put('rotation', { rotations: rotations.map((rotation) => rotation.toJson()) });
  }

  public async deletingRotations(): Promise<ResponseApi> {
    return await api.delete('rotation/deletingRotations');
  }
}
export const RotationSer = new RotationService();
