import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';

class AssignStudentGroupService {
  public async post(assignStudentsGroupsArray: any) {
    return await api.post(`assignStudentGroup`, { assignStudentsGroupsArray });
  }

  public async getAll(): Promise<ResponseApi> {
    return await api.get(`assignment`);
  }
}

export const assignStudentGroupSer = new AssignStudentGroupService();
