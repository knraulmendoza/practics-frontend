import { IAttention } from '@/interfaces/interfaces';
import { api } from '@/helpers/api';

class AttentionService {
  public async post(attention: IAttention) {
    return await api.post(`attention`, attention);
  }

  public async attentionByProcedures(subject: any, academicPeriod: any) {
    return await api.post(`attention/by-activities`, { subject, academicPeriod });
  }

  public async attentionByModule(subject: any, module: any, academicPeriod: any) {
    return await api.post(`attention/by-module`, { subject, module, academicPeriod });
  }

  public async attentionByScenario(scenario: any, academicPeriod: any) {
    return await api.post(`attention/by-scenario`, { scenario, academicPeriod });
  }

  public async getAll() {
    return await api.get(`attention`);
  }

  public async getAllpaginate(page = 1) {
    return await api.get(`attention/getAllAttentions?page=${page}`);
  }
  public async search(search: any) {
    return await api.post('attention/search', search);
  }
}

export const attentionSer = new AttentionService();
