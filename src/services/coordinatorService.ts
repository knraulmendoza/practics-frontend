import { api } from '@/helpers/api';
import { Teacher } from '@/domain/entities/teacher/Teacher';
import { Coordinator } from '@/domain/entities/coordinator/Coordinator';
class CoordinatorService {
  public async post(teacher: Teacher) {
    return await api.post(`teacher`, teacher.toJson());
  }

  public async getAll() {
    return await api.get(`teacher`);
  }

  public async put(coordinator: Coordinator) {
    return await api.put(`coordinator/${coordinator.id}`, coordinator.toJsonUpdate());
  }

  public async getPerson() {
    return await api.get(`teacher/person`);
  }

  public async getPersons() {
    return await api.get(`coordinator/people`);
  }
}
export const coordinatorService = new CoordinatorService();
