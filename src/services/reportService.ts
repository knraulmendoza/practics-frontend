import { api } from '@/helpers/api';
class ReportService {
  public async getRotation(moduleId: number) {
    return await api.getPdf(`pdf/boxRotations?module=${moduleId}`);
  }

  public async postReportDownloadPdf(ruta: string, body: FormData) {
    return await api.postDowloadPdf(`pdf/${ruta}`, body);
  }

  public async postReportAttentionsGetPdf(ruta: string, body: FormData) {
    return await api.postGetPdf(`pdf/${ruta}`, body);
  }

  public async getAll(reportCategory: number, academicPeriod: number) {
    return await api.get(`report/by-category/${reportCategory}/${academicPeriod}`);
  }

  public async saveReport(body: FormData) {
    return await api.post('report', body);
  }
}
export const reportService = new ReportService();
