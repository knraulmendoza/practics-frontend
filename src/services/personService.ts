import { api } from '@/helpers/api';
import { Person } from '@/domain/entities/person/Person';

class PersonService {
  public async get() {
    return await api.get('person/getPerson');
  }

  public async post(person: Person, titles?: number[]) {
    return await api.post('person', { person: person.toJson(), titles });
  }
  public async search(search: any) {
    return await api.post('person/search', search);
  }
}

export const personSer = new PersonService();
