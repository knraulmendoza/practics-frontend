import { api } from '@/helpers/api';
import Assignment from '@/domain/entities/assignment/Assignment';
import { ResponseApi } from '@/helpers/ResponseApi';
import { IArea, ISelect } from '@/interfaces/interfaces';
import { IAssignmentResponse } from '@/interfaces/InterfaceResponse';

class AssignmentService {
  public async post(assignment: Assignment) {
    return await api.post(`assignment`, assignment.toJson());
  }

  public async update(assignment: Assignment) {
    return await api.put(`assignment/${assignment.id}`, assignment.toJson());
  }

  public async getAll(): Promise<ResponseApi> {
    return await api.get(`assignment`);
  }

  public async getByModule(moduleId: number): Promise<ISelect[]> {
    const assignments: ISelect[] = [];
    const response = await api.get(`assignment/assignmentByModule/${moduleId}`);
    response.data.data.assignments.forEach((res: IAssignmentResponse) => {
      assignments.push({
        text: `${res.teacher.person.first_name} ${
          res.teacher.person.first_last_name
        } - ${res.scenario.name.toLowerCase()} - ${(res.scenario.areas as IArea[])[0].name}`,
        value: res.id,
      });
    });
    return assignments;
  }
}

export const assignmentSer = new AssignmentService();
