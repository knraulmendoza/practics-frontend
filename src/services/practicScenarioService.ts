import { Area } from '@/domain/entities/practiceScenario/Area';
import { PracticeScenario } from '@/domain/entities/practiceScenario/PracticeScenario';
import { api } from '@/helpers/api';
import { IArea, ISelect } from '@/interfaces/interfaces';

class PracticScenarioService {
  public async post(scenario: PracticeScenario, scenarioTypeText: string) {
    scenario.name = `${scenarioTypeText} ${scenario.name}`;
    return await api.post(`scenario`, scenario.toJson());
  }
  public async saveArea(area: Area) {
    return await api.post(`area`, area.toJson());
  }

  public async updateArea(area: Area, id: any) {
    return await api.put(`area/${id}`, area.toJson());
  }

  public async deleteArea(area: Area) {
    return await api.delete(`area/${area.id}`);
  }

  public async areasInScenario(areas: number[], scenarioId: number) {
    return await api.post(`area/areasInScenario`, {
      areas,
      scenarioId,
    });
  }

  public async areaInModules(areaId: number, scenarioId: number, modulesId: number[]) {
    return await api.post(`area/areaInModules`, {
      areaId,
      scenarioId,
      modulesId,
    });
  }
  public async searchArea(area: string) {
    return await api.post('area/search', { area });
  }
  public async get() {
    return await api.get(`scenario`);
  }
  public async getAllSelect() {
    return await api.getSelect(`scenario`);
  }
  public async getPaginate(page = 1) {
    return await api.get(`scenario/paginate?page=${page}`);
  }
  public async getSelectPracticScenario() {
    return await api.getSelect(`escenariosTypes`);
  }

  public async getSelectScenariosByModule(moduleId: number) {
    return await api.getSelect(`scenario/modules/${moduleId}`);
  }
  public async getSelectScenariosByType(type: number) {
    return await api.getSelect(`scenario/by-type/${type}`);
  }

  public async getSelectAreas(scenarioId: number, moduleId: number) {
    return await api.getSelect(`scenario/areasByModule/${scenarioId}/${moduleId}`);
  }

  public async getSelectAreasWithModule(subject: string, area: number, scenario: number) {
    return await api.get(`subject/modules/modulesArea/${subject}/${area}/${scenario}`);
  }

  public async getAreas(scenarioId: number) {
    return await api.get(`scenario/areas/${scenarioId}`);
  }

  public async getAreasWithoutScenarios(scenarioId: number) {
    return await api.getSelect(`area/areasWithoutScenario/${scenarioId}`);
  }

  public async getAreasPaginate(page = 1) {
    return await api.get(`area?page=${page}`);
  }
  public async searchScenario(scenario: string) {
    return await api.get(`scenario/search/${scenario.trim()}`);
  }
  public async getSelectAreasAsignacion(id: number) {
    const data = await api.get(`areasAsignacion/` + id).then((response) => {
      const select: ISelect[] = [];
      response.data.data.forEach((dataAux: any) => {
        select.push({ text: dataAux.id, value: dataAux.area_id });
      });
      return select;
    });
    return data;
  }

  public async scenarioInCareer(scenarioId: number) {
    return await api.post('scenario/scenarioInCareer', { scenarioId });
  }

  public async saveAreas(areas: IArea[], practiceScenario: number) {
    return await api.post('area', { areas, practiceScenario });
  }

  public async asignarAreaModule(area: number, scenarioId: number, modulesId: number[]) {
    return await api.post('area/areaInModules', { area, scenarioId, modulesId });
  }
  public async asignarAreaInScenario(areas: number[], scenarioId: number) {
    return await api.post('area/areaInScenario', { areas, scenarioId });
  }

  public async put(item: PracticeScenario) {
    return await api.put(`scenario/${item.id}`, item.toJsonUpdate());
  }

  public async assignQuotas(scenario: PracticeScenario) {
    return await api.post('scenario/assignQuotas', { practicScenarioId: scenario.id, quotas: scenario.quotas });
  }

  public async deleteScenario(scenarioId: number) {
    return await api.delete(`scenario/${scenarioId}`);
  }
}

export const practicScenarioService = new PracticScenarioService();
