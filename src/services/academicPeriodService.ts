import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';
import { ISelect } from '@/interfaces/interfaces';

class AcademicPeriodService {
  public async get(): Promise<ResponseApi> {
    return await api.get(`academicPeriod/getAcademicPeriod`);
  }
  public async getAcademicPeriodByDate(): Promise<ResponseApi> {
    return await api.get(`academicPeriod/getAcademicPeriodByDate`);
  }
  public async getAll(): Promise<ISelect[]> {
    const periodAcademics: ISelect[] = [];
    const response = await api.get('academicPeriod');
    response.data.data.academicPeriods.forEach((res) => {
      periodAcademics.push({
        text: `${res.year} ${res.period}`,
        value: res.id,
      });
    });
    return periodAcademics;
  }
  public async getAllUpc() {
    return await api.get('upc/periodo', { upc: true });
  }

  public async activeAcademicPeriod(academicPeriodId: number) {
    return await api.post(`academicPeriod/activeAcademicPeriod`, { academic_period_id: academicPeriodId });
  }

  public async addAcademicPeriod(body: any) {
    return await api.post(`academicPeriod`, body);
  }

  public async updateAcademicPeriod(body: any, id: number) {
    return await api.put(`academicPeriod/` + id, body);
  }

  public async updDates(body: any, id: number) {
    return await api.put(`academicPeriod/updateAcademicPeriodCareer/` + id, body);
  }
}

export const academicPeriodService = new AcademicPeriodService();
