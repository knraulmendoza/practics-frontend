import { api } from '@/helpers/api';
import { Teacher } from '@/domain/entities/teacher/Teacher';
import { ResponseApi } from '@/helpers/ResponseApi';
import { ISelect } from '@/interfaces/interfaces';
import { ITeacherResponse } from '@/interfaces/InterfaceResponse';
class TeacherService {
  public async post(teacher: Teacher) {
    return await api.post(`teacher`, teacher.toJson());
  }

  public async getAll() {
    return await api.get(`teacher`);
  }

  public async getAllSelect(): Promise<ISelect[]> {
    const data = await api.get(`teacher/people`).then((response: ResponseApi) => {
      const select: ISelect[] = [];
      response.data.data.teachers.forEach((teacher: ITeacherResponse) => {
        const teacherAux = new Teacher(teacher);
        select.push({ text: teacherAux.person.fullName, value: teacherAux.id as number });
      });
      return select;
    });
    return data;
  }

  public async put(teacher: Teacher) {
    return await api.put(`teacher/${teacher.id}`, teacher.toJsonUpdate());
  }

  public async getPerson() {
    return await api.get(`teacher/person`);
  }

  public async getPeople() {
    return await api.get('teacher/people');
  }
  public async getPeoplePaginate(page = 1) {
    return await api.get(`teacher/people-paginate?page=${page}`);
  }
  public async teacherByScenario(scenarioId: number, page?: number) {
    return await api.get(`teacher/scenario/${scenarioId}?page=${page}`);
  }
}
export const teacherService = new TeacherService();
