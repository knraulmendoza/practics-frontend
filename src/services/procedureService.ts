import { IProcedure, ISelect } from '@/interfaces/interfaces';
import { api } from '@/helpers/api';
import { Procedure } from '@/domain/entities/procedure/Procedure';
class ProcedureService {
  public async getAll(moduleId: number) {
    return await api.get(`module/procedures/${moduleId}`);
  }
  public async getAllPaginate(moduleId: number, page = 1) {
    return await api.get(`module/procedures-paginate/${moduleId}?page=${page}`);
  }
  public async getProcedureSelect(moduleId: number) {
    const data = await api.get(`module/procedures/${moduleId}`).then((response) => {
      const select: ISelect[] = [];
      response.data.data.procedures.forEach((procedure: IProcedure) => {
        select.push({ text: procedure.name, value: procedure.id as number });
      });
      return select;
    });
    return data;
  }
  public async post(procedure: IProcedure) {
    return await api.post(`procedure`, procedure);
  }
  public async put(procedure: Procedure) {
    return await api.put(`procedure/${procedure.id}`, procedure.toJsonResponse());
  }
  public async delete(id: number) {
    return await api.delete(`procedure/${id}`);
  }
}

export const procedureSer = new ProcedureService();
