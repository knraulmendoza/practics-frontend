import { Page } from '@/domain/entities/page/Page';
import { api } from '@/helpers/api';

class PageService {
  public async get() {
    return await api.get(`page`);
  }

  public async getAll() {
    return await api.get(`page/getAll`);
  }

  public async updatePage(page: Page) {
    return await api.put(`page/${page.id}`, page.toJson());
  }
}

export const pageSer = new PageService();
