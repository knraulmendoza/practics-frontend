import { api } from '@/helpers/api';

class PatientService {
  public async getAll() {
    return await api.get(`patient`);
  }
}

export const patientService = new PatientService();
