import { IQuestion } from '@/interfaces/interfaces';
import { api } from '@/helpers/api';
class QuestionService {
  public async save(question: IQuestion) {
    return await api.post('question', question);
  }
  public async getAll() {
    return await api.get('question');
  }
  public async getQuestions() {
    return await api.get(`career/questions`);
  }
  public async getQuestionsPaginate(page = 1) {
    return await api.get(`career/questions-paginate?page=${page}`);
  }
  public async searchQuestion(question: string) {
    return await api.post('question/search', { question });
  }

  public async filterQuestion(filter: any, page = 1) {
    return await api.post(`question/questionByCheckType?page=${page}`, filter);
  }

  public async getQuestionsCheck() {
    return await api.get(`career/questionsCheck`);
  }

  public async update(question: IQuestion) {
    return await api.put(`question/${question.id}`, question);
  }
}
export const QuestionSer = new QuestionService();
