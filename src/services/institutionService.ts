import { ISelect } from '@/interfaces/interfaces';
import { api } from '@/helpers/api';
import { Institution } from '@/domain/entities/institution/Institution';

class InstitutionService {
    public async getAll() {
        return await api.get(`institution`);
    }

    public async getSeletBySubject(subjectId: number): Promise<ISelect[]> {
        return await api.getSelect(`subjects/modules/${subjectId}`);
    }

    public async post(institution: Institution) {
        return await api.post(`institution`, institution.toJson());
    }
    public async put(institution: Institution) {
        return await api.put(`institution/${module.id}`, institution.toJson());
    }
}

export const institutionSer = new InstitutionService();
