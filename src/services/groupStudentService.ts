import { api } from '@/helpers/api';
import { Group } from '@/domain/entities/student/Group';

class GroupStudentService {
  public async post(groupStudents: Group[]) {
    return await api.post(`groupStudent`, { groups: groupStudents.map((group) => group.toJson()) });
  }

  public async groupsByModule(moduleId: number) {
    return await api.get(`groupStudent/studentsGroupsByModule/${moduleId}`);
  }

  public async put(groupStudents: Group[]) {
    return await api.post(`groupStudent/updateGroup`, { groups: groupStudents.map((group) => group.toJsonUpdate()) });
  }
}

export const groupStudentSer = new GroupStudentService();
