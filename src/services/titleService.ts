import { Title } from '@/domain/entities/title/Title';
import { api } from '@/helpers/api';
import { ResponseApi } from '@/helpers/ResponseApi';
import { ISelect } from '@/interfaces/interfaces';

class TitleService {
  public async getSelectTitles() {
    const select: ISelect[] = [];
    const response = await api.get('title');
    response.data.data.titles.forEach((data: any) => {
      select.push({ text: data.name, value: data.id });
    });
    return select;
  }

  public async post(title: Title): Promise<ResponseApi> {
    return await api.post('title', title.toJson());
  }

  public async put(title: Title): Promise<ResponseApi> {
    return await api.put(`title/${title.id}`, title.toJson());
  }

  public async addTitle(titles: number[], personId: number) {
    await api.post('title/', { titls: titles, person_id: personId });
  }
}

export const titleService = new TitleService();
