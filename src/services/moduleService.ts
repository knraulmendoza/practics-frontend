import { ISelect } from '@/interfaces/interfaces';
import { api } from '@/helpers/api';
import { Module } from '@/domain/entities/module/Module';

class ModuleService {
  public async getAll() {
    return await api.get(`career/modules`);
  }

  public async getAllPaginate(page = 1) {
    return await api.get(`career/modules-paginate?page=${page}`);
  }

  public async getSeletBySubjectWithoutArea(subject: any): Promise<ISelect[]> {
    return await api.getSelect(`subject/modulesArea/${subject}`);
  }

  public async getSeletBySubject(subject: string): Promise<ISelect[]> {
    return await api.getSelect(`subject/modules/${subject}`);
  }

  public async getAllSelet(): Promise<ISelect[]> {
    return await api.getSelect(`career/modules`);
  }

  public async post(module: Module) {
    return await api.post(`module`, module.toJson());
  }
  public async put(module: Module) {
    return await api.put(`module/${module.id}`, module.toJson());
  }
}

export const moduleSer = new ModuleService();
