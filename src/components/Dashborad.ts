import { Component, Vue } from 'vue-property-decorator';
import Drawer from './drawer/AppDrawerComponent';
import Vista from './vista';
import template from './DashboardView.vue';
import { globalSer } from '@/services/globalService';
import { Roles } from '@/route/Roles';
@Component({
  mixins: [template],
  components: { Drawer, Vista },
})
export default class Dashboard extends Vue {
  public created(): void {
    if (globalSer.getCurrentUser().rol === Roles.ADMIN && this.$route.path === '/dashboard') {
      // tslint:disable-next-line:no-empty
      this.$router.push('/dashboard/configuracion');
    } else {
      if (this.$route.path === '/dashboard') {
        // tslint:disable-next-line:no-empty
        this.$router.push('/dashboard/events');
      }
    }
  }
}
