import { Component, Emit, Prop } from 'vue-property-decorator';
import Vue from 'vue';
import template from './Procedure.vue';
import { Procedure } from '@/domain/entities/procedure/Procedure';
import { procedureSer } from '@/services/procedureService';
import { IProcedure } from '@/interfaces/interfaces';
import { Module } from '@/domain/entities/module/Module';
import { globalSer } from '@/services/globalService';
import { Status } from '@/helpers/Status';

@Component({
  mixins: [template],
})
export default class ProcedureComponent extends Vue {
  @Prop({ type: Object, default: new Module() }) public readonly module!: Module;
  public loading = true;
  public editedIndexPro = -1;
  public procedures: Procedure[] = [];
  public procedure: Procedure = new Procedure();
  public valid = false;
  public lazy = false;
  public perPage = 1;
  public page = 1;
  public pageCount = 0;
  public totalProcedures = 0;
  public headersPro = [
    { text: 'procedimiento', value: 'name' },
    { text: 'Descripción', value: 'description' },
    { text: 'Acción', value: 'action', sortable: false },
  ];
  public validateProcedure = {
    name: [(v: unknown): string | boolean => !!v || 'Este campo es obligatorio'],
  };
  get formTitle(): string {
    return this.editedIndexPro === -1 ? 'Nuevo Procedimiento' : 'Editar Procedimiento';
  }
  public async showProcedures(show = false): Promise<void> {
    const resp = await procedureSer.getAllPaginate(this.module.id as number, this.page);
    if (resp.data.state) {
      this.procedures = (resp.data.data.procedures as IProcedure[]).map((procedure) => new Procedure(procedure));
      this.perPage = resp.data.data.per_page;
      this.pageCount = Math.ceil(resp.data.data.total / this.perPage);
      if (show) {
        this.page = this.pageCount;
        this.showProcedures();
      }
      this.totalProcedures = resp.data.data.total;
    }
    this.loading = false;
  }

  public clearProcedure(): void {
    this.procedure = new Procedure();
    this.editedIndexPro = -1;
    (this.$refs.formProcedure as any).reset();
  }

  public editProcedure(procedure: Procedure): void {
    const proc = Object.assign<unknown, IProcedure>({}, procedure.toJsonResponse());
    this.procedure = new Procedure(proc);
    this.editedIndexPro = 1;
  }
  public async deleteProcedure(procedure: Procedure): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Eliminando procedimiento' });
    const resp = await procedureSer.delete(procedure.id as number);
    this.$store.commit('setLoading', { loading: false, msg: '' });
    if (resp.data.state) {
      this.page = this.pageCount;
      await this.showProcedures(true);
      globalSer.success('Se ha eliminado correctamente');
    } else {
      globalSer.error(resp.data.message);
    }
  }
  public async created(): Promise<void> {
    this.showProcedures();
  }
  @Emit('closeDialogPro')
  public closeDialogPro(): void {
    this.procedures = [];
  }

  public nextPage(): void {
    this.hablePaginate();
  }
  public previous(): void {
    this.hablePaginate();
  }
  public async hablePaginate(): Promise<void> {
    this.procedures = [];
    this.loading = true;
    const resp = await procedureSer.getAllPaginate(this.module.id as number, this.page);
    if (resp.data.state) {
      this.procedures = (resp.data.data.procedures as IProcedure[]).map((procedure) => new Procedure(procedure));
    }
    this.loading = false;
  }

  public async addProcedure(): Promise<void> {
    this.procedure.moduleId = this.module.id as number;
    this.$store.commit('setLoading', { loading: true, msg: 'Cargando...' });
    if (this.editedIndexPro === -1) {
      const resp = await procedureSer.post(this.procedure.toJson());
      if (resp.data.state) {
        this.page = this.pageCount;
        await this.showProcedures(true);
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.alert('Procedimiento', 'success', 'Se registro el procedimiento correctamente');
      } else {
        this.$store.commit('setLoading', { loading: false, msg: '' });
        globalSer.alert('Procedimiento', 'error', resp.data.message);
      }
    } else {
      const resp = await procedureSer.put(this.procedure);
      if (resp.data.state) {
        this.$store.commit('setLoading', { loading: false, msg: 'Cargando' });
        this.hablePaginate();
        globalSer.alert('Procedimiento', 'success', 'Se actualizo el procedimiento correctamente');
      } else {
        if (resp.status === Status.REQUESTVALIDATIONS) {
          this.$store.commit('setLoading', { loading: false, msg: '' });
          globalSer.errorValidation(resp.dataError);
        } else {
          this.$store.commit('setLoading', { loading: false, msg: '' });
          globalSer.alert('Procedimiento', 'error', resp.data.message);
        }
      }
    }
    this.clearProcedure();
  }
}
