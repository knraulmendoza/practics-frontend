import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './ViewComponent.vue';

@Component({
  name: 'ViewComponent',
  mixins: [template],
})
export default class ViewComponent extends Vue {}
