import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import template from './AppDrawerView.vue';
import { authSer } from '@/services/authService';
import { globalSer } from '@/services/globalService';
import { Role } from '@/domain/entities/role/Rol';
import { Career } from '@/domain/entities/career/Career';
import { Person } from '@/domain/entities/person/Person';
import { IPersonResponse } from '@/interfaces/InterfaceResponse';
import { pageSer } from '@/services/pageService';
import { Page } from '@/domain/entities/page/Page';
import { IPage } from '@/interfaces/interfaces';
import Loading from '@/components/loading.vue';
import { careerService } from '@/services/careerService';
@Component({
  name: 'AppBar',
  mixins: [template],
  components: { Loading },
})
export default class AppDrawerComponent extends Vue {
  //   @Prop({ required: true, type: [] }) readonly datos: [];
  public drawer = false;
  public group = null;
  public careers: any;
  public roles: any;
  public subMenuAssignment = false;
  public links: any[] = [];
  public linksAux: any[] = [];
  public names = {
    person: '',
    rol: '',
    career: '',
  };
  public careerId = 0;
  public get title(): string {
    return this.$route.meta?.name;
  }
  public get faculties(): any[] {
    return this.$store.state.faculties;
  }
  public filter(): void {
    const links: any[] = [];
    const rutas: any = this.$router;
    rutas.options.routes.forEach((route: any) => {
      if (route.path === '/dashboard') {
        route.children.forEach((ruta: any) => {
          const authorize: number[] = ruta.meta.authorize;
          if (authorize != null) {
            if (authorize.includes(globalSer.getCurrentUser().rol as number)) {
              links.push(`/dashboard/${ruta.path}`);
            }
          }
        });
      }
    });
    this.links = this.linksAux.filter((link) => links.includes(link.path));
  }

  public async getFaculties(): Promise<void> {
    const faculties = await careerService.getAllFacultiesSelect();
    this.$store.commit('setFaculties', faculties);
  }

  public async created(): Promise<void> {
    this.careerId = globalSer.getCurrentUser().career?.id as number;
    this.careers = localStorage.getItem('careers');
    this.careers = JSON.parse(this.careers) ?? [];
    this.roles = localStorage.getItem('roles') ?? [];
    this.roles = JSON.parse(this.roles);
    this.roles = this.roles.filter((rol: any) => rol.id !== globalSer.getCurrentUser().rol);
    this.names.rol = globalSer.getNameRol();
    this.names.career = globalSer.getNameCareer();
    this.$store.commit('setLoading', { loading: true, msg: 'Cargando datos ...' });
    const res = await pageSer.get();
    this.links = (res.data.data.pages as IPage[]).map((page) => new Page(page));
    this.linksAux = this.links;
    this.filter();
    const currentUser = await authSer.getUser();
    this.$store.commit('setLoading', { loading: false, msg: '' });
    this.names.person = currentUser.data.data.person
      ? new Person(currentUser.data.data.person as IPersonResponse).fullName
      : 'Administrador';
    if (!currentUser.data.data.person) {
      await this.getFaculties();
    }
  }

  public selectRole(role: Role): void {
    const currentUser = globalSer.getCurrentUser();
    currentUser.rol = role.id as number;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.filter();
  }
  public selectCareer(career: Career): void {
    const currentUser = globalSer.getCurrentUser();
    currentUser.career = career;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.careerId = globalSer.getCurrentUser().career?.id as number;
    this.$router.go(0);
  }

  public changeFaculty(): void {
    const currentUser = globalSer.getCurrentUser();
    currentUser.career = undefined;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.$router.push('/selectRole');
  }
  public mounted(): void {
    this.onResponsiveInverted();
    window.addEventListener('resize', this.onResponsiveInverted);
  }
  public beforeDestroy(): void {
    window.removeEventListener('resize', this.onResponsiveInverted);
  }
  public onResponsiveInverted(): void {
    if (window.innerWidth < 991) {
      if (!this.drawer) {
        this.drawer = false;
      }
    } else {
      this.drawer = true;
    }
  }
  public async salir(): Promise<void> {
    this.$store.commit('setLoading', { loading: true, msg: 'Cerrando sesión ...' });
    await authSer.logout();
    this.$router.push('/');
    this.$store.commit('setLoading', { loading: false, msg: '' });
  }
}
