export enum Status {
    OK = 200,
    BADREQUEST = 401,
    REQUESTVALIDATIONS = 422,
    ERRORSERVER = 500,
}
