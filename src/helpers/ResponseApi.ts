import { Status } from './Status';
import { AxiosResponse } from 'axios';

interface IData {
  state: boolean;
  message: string;
  data: any;
}
export class Data {
  private readonly _state: boolean;
  private readonly _message: string;
  private _data: any;

  public get state(): boolean {
    return this._state;
  }

  public get message(): string {
    return this._message;
  }

  public get data(): any {
    return this._data;
  }

  public set data(data: any) {
    this._data = data;
  }

  constructor(data: IData = {} as IData) {
    this._state = data.state;
    this._message = data.message;
    this._data = data.data;
  }
}
// tslint:disable-next-line:max-classes-per-file
export class ResponseApi {
  private readonly _status?: Status;
  private readonly _data: Data;
  private _dataError: string[];

  public get status(): Status | undefined {
    return this._status;
  }

  public get data(): Data {
    return this._data;
  }

  public get dataError(): string[] {
    return this._dataError;
  }

  public set dataError(dataError: string[]) {
    this._dataError = dataError;
  }

  constructor(response: AxiosResponse = {} as AxiosResponse) {
    this._status = response.status;
    this._data = new Data(response.data);
    this._dataError = [];
    if (response.status === Status.REQUESTVALIDATIONS) {
      this._dataError = response.data;
    }
  }
}
