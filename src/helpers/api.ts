import Axios, { AxiosResponse, AxiosRequestConfig } from 'axios';
import { ISelect } from '@/interfaces/interfaces';
import { ResponseApi } from './ResponseApi';
import { globalSer } from '@/services/globalService';
import download from 'downloadjs';
class ApiBaseHelper {
  public baseUrl = process.env.VUE_APP_BASE_URL;
  public config(param?: any) {
    return {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        career: globalSer.getCurrentUser() !== null ? globalSer.getCurrentUser().career?.id : 0,
        rol: globalSer.getCurrentUser() !== null ? globalSer.getCurrentUser().rol : 0,
      },
      params: param,
    };
  }
  public configPdf() {
    return {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
        career: globalSer.getCurrentUser() !== null ? globalSer.getCurrentUser().career?.id : 0,
        rol: globalSer.getCurrentUser() !== null ? globalSer.getCurrentUser().rol : 0,
      },
      responseType: 'blob',
    } as AxiosRequestConfig;
  }
  public async getPdf(url: string) {
    const response = await Axios.get(this.baseUrl + '/api/' + url, this.configPdf())
      .then((data: AxiosResponse) => {
        const content = data.headers['content-type'];
        if (data.data.size === 0) {
          return null;
        }
        download(data.data, 'cuadro rotacion.pdf', content);
      })
      .catch((error) => {
        return error.response;
      });
    return response;
  }
  public async postGetPdf(url: string, body: any) {
    const response = await Axios.post(this.baseUrl + '/api/' + url, body, this.config())
      .then((data: AxiosResponse) => {
        return data.data;
      })
      .catch((error) => {
        return error.response;
      });
    return response;
  }
  public async postDowloadPdf(url: string, body: any) {
    const response = await Axios.post(this.baseUrl + '/api/' + url, body, this.configPdf())
      .then((data: AxiosResponse) => {
        const content = data.headers['content-type'];
        download(data.data, `${body.get('title')}.pdf`, content);
      })
      .catch((error) => {
        return error.response;
      });
    return response;
  }
  public async get(url: string, dataParams?: { params?: any; upc?: boolean }): Promise<ResponseApi | any> {
    const response = await Axios.get(this.baseUrl + '/api/' + url, this.config(dataParams?.params))
      .then((data: AxiosResponse) => {
        return dataParams?.upc ? data.data : data;
      })
      .catch((error) => {
        return error.response;
      });
    return dataParams?.upc ? response : new ResponseApi(response);
  }
  public async getSelect(url: string): Promise<ISelect[]> {
    const data = await this.get(url).then((response: ResponseApi) => {
      if (response.data.state) {
        return Object.values<any>(response.data.data)[0].map((dataObject: any) => {
          return { text: dataObject.name || dataObject.module || dataObject.question, value: dataObject.id } as ISelect;
        });
      }
      return [];
    });
    return data;
  }

  public async post(url: string, body: any): Promise<ResponseApi> {
    const response = await Axios.post(this.baseUrl + '/api/' + url, body, this.config())
      .then((data: AxiosResponse) => {
        return data;
      })
      .catch((error) => {
        return error.response;
      });
    return new ResponseApi(response);
  }
  public async put(url: string, body: any): Promise<ResponseApi> {
    const response = await Axios.put(this.baseUrl + '/api/' + url, body, this.config())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        return error.response;
      });
    return new ResponseApi(response);
  }

  public async delete(url: string) {
    const response = await Axios.delete(this.baseUrl + '/api/' + url, this.config())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        return error.response;
      });
    return new ResponseApi(response);
  }
}

export const api = new ApiBaseHelper();
