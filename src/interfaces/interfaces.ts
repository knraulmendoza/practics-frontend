import { CheckType } from '@/domain/entities/question/ChechkType';
import { Type } from '@/domain/entities/question/Type';
import { DocumentType } from '@/domain/entities/person/DocumentType';
import { TypesInstitutions } from '@/domain/entities/institution/TypesInstitutions';
import { ClassInstitutions } from '@/domain/entities/institution/ClassInstitutions';
import { StateAcademicPeriod } from '@/domain/entities/academicPeriod/State';

export interface IRole {
  id?: number;
  name: string;
  description?: string;
}

export interface IAcademicPeriodCareer {
  id?: number;
  career_id: number;
  academic_period_id: number;
  start_date: string;
  end_date: string;
  state: StateAcademicPeriod;
}

export interface IAcademicPeriod {
  id?: number;
  year: number;
  period: number;
  start_date: string;
  end_date: string;
  state: StateAcademicPeriod;
}

export interface IUser {
  id?: number;
  user: string;
  password: string;
  user_parent?: number;
  rol: number;
}
export interface IPerson {
  id?: number;
  document: string;
  document_type: number;
  first_name: string;
  second_name?: string;
  first_last_name: string;
  second_last_name: string;
  phone?: string;
  birthday?: Date;
  address?: string;
  gender?: number;
  user_id: number;
  career_id?: number;
}
export interface ITeacher {
  id?: number;
  person: IPerson;
  state?: number;
  types?: number[];
  titles: number[];
}

export interface IStudent {
  id?: number;
  person: IPerson;
}

export interface IStudentUpc {
  identificacion: string;
  tipo_documento: number;
  primer_nombre: string;
  segundo_nombre?: string;
  primer_apellido: string;
  segundo_apellido: string;
  genero?: number;
  correo: string;
}
export interface IGroup {
  id?: number;
  code?: string;
  name: string;
  module_id: number;
  students: string[];
}
export interface IGroupStudent {
  id?: number;
  module_id: number;
  groups: IGroup[];
}

export interface ICoordinator {
  id?: number;
  person: IPerson;
  state?: number;
}

export interface IAsistencia {
  idAsistencia?: number;
  idEstudiante?: number;
  fecha: Date;
  asistencia: boolean;
}

export interface ISelect {
  value: number | string;
  text: string;
}
export interface ISubject {
  id?: number;
  name: string;
  code: string;
}

export interface ISubjectUpc {
  id?: number;
  nombre: string;
  codigo: string;
}

export interface ITitle {
  id?: number;
  name: string;
  description?: string;
}

export interface IModule {
  id?: number;
  name: string;
  code: string;
  description?: string;
  subject_code: string;
}

export interface IProcedure {
  id?: number;
  module?: IModule;
  module_id: number;
  name: string;
  description?: string;
}

export interface IQuestion {
  id?: number;
  type: Type;
  question: string;
  check_type: CheckType;
}
export interface IPatient {
  id?: number;
  document_type: DocumentType;
  document: string;
  name: string;
  age?: number;
  gender?: number;
  phone?: string;
  status?: number;
  academic_level?: number;
  employment_situation?: number;
  origin?: string;
  attentions: IAttention[];
  created_at: Date;
}

export interface IAttention {
  id?: number;
  patient: IPatient;
  procedures: number[];
  date: Date;
  state: number;
  encuesta: string;
}

export interface ICheck {
  id?: number;
  date: Date;
  evaluated_id: number;
  evaluated?: IPerson;
  evaluator_id: number;
  evaluator?: IPerson;
  observation: string;
}

export interface IObservation {
  observation: string;
  check_id?: number;
  check: ICheck;
}

export interface IAnswersChecks {
  id?: number;
  answer: number;
  question_id: number;
  question?: string;
  observation: string;
}

export interface ICareer {
  id?: number;
  name: string;
  code: string;
  faculty?: any;
  description?: string;
  faculty_id?: number;
  faculty_code?: string;
}
export interface IFaculty {
  id?: number;
  name: string;
  code: string;
}
export interface ICareerUpc {
  nombre: string;
  id: string;
}

export interface IPracticeScenario {
  id?: number;
  name: string;
  description?: string;
  complexity_level?: number;
  scene_type: number;
  entity: number;
  areas?: IArea[];
  quotas: number;
}

export interface IArea {
  id?: number;
  name: string;
  description?: string;
  moduleId?: number;
}

export interface IInstitucion {
  id?: number;
  code: string;
  name: string;
  type: TypesInstitutions;
  class: ClassInstitutions;
  description?: string;
}

export interface IEvent {
  id?: number;
  title: string;
  description: string;
  date: Date;
  hour: string;
  address: string;
  state: number;
  image: string;
}

export interface IAssignment {
  module_id: number;
  teacher_id: number;
  area_id: number;
  scenario_id: number | null;
}

export interface IAssignmentStudentGroup {
  rotation_id: number;
  assignment_id: any;
  group_module_id: number;
  working_day: string;
}

export interface IRotation {
  id?: number;
  start_date: string;
  end_date: string;
  type: number;
  created_at?: string;
  text?: string;
}

export interface IEvent {
  id?: number;
  title: string;
  description: string;
  date: Date;
  hour: string;
  address: string;
  state: number;
}

export interface IPage {
  id?: number;
  title: string;
  path: string;
  icon: string;
  type: number;
  state: number;
  subGroup: IPage[];
  parent_page?: number;
}

export interface IReportMeta {
  key: string;
  value: number;
  tabla: string;
}
