import { CheckType } from '@/domain/entities/question/ChechkType';
import { Type } from '@/domain/entities/question/Type';
import { IRole, ICareer, IPracticeScenario, IRotation, IAnswersChecks } from './interfaces';

export interface IAssignmentResponse {
  id: number;
  module: IModuleResponse;
  teacher: ITeacherResponse;
  scenario: IPracticeScenario;
}

export interface IAssignmentStudentGroupResponse {
  id: number;
  rotation: IRotation;
  assignment: IAssignmentResponse;
  group: IGroupResponse;
}

export interface ILoginResponse {
  token: string;
  user: IUserResponse;
  careers: ICareer[];
}

export interface IGroupResponse {
  id?: number;
  code?: string;
  name: string;
  module_id: number;
  students: IStudentResponse[];
  assign: any;
}
export interface IGroupStudentResponse {
  id?: number;
  module_id: number;
  groups: IGroupResponse[];
}

export interface IUserResponse {
  id?: number;
  user: string;
  password: string;
  user_parent?: number;
  roles?: IRole[];
  state_password: number;
}

export interface IPersonResponse {
  id?: number;
  document: string;
  document_type: string;
  first_name: string;
  second_name?: string;
  first_last_name: string;
  second_last_name: string;
  phone?: string;
  birthday?: Date;
  address?: string;
  gender?: string;
  user?: IUserResponse;
  user_id: number;
}

export interface ICoordinatorResponse {
  id?: number;
  person: IPersonResponse;
  career?: ICareer;
  state: string;
}

export interface ITeacherResponse {
  id?: number;
  person: IPersonResponse;
  types: number[];
  state: string;
  titles?: ITitleResponse[];
  titles_id?: number[];
}

export interface IStudentResponse {
  id?: number;
  person: IPersonResponse;
}

export interface ITitleResponse {
  id?: number;
  name: string;
  description?: string;
}

export interface IModuleResponse {
  id?: number;
  code: string;
  module: string;
  name?: string;
  description: string;
  subject_code: string;
  subject?: string;
}

export interface IPatientResponse {
  id?: number;
  document_type: DocumentType;
  document: string;
  name: string;
  age?: number;
  gender?: number;
  phone?: string;
  status?: number;
  academic_level?: number;
  employment_situation?: number;
  origin?: string;
}

export interface IAttentionResponse {
  id?: number;
  patient: IPatientResponse;
  procedures: number[];
  date: Date;
}

export interface IReportMetaResponse {
  key: string;
  value: number;
  tabla?: string;
}
export interface IReportDataResponse {
  name: string;
  total: number;
  percentage: number;
}
export interface IReportResponse {
  id?: number;
  title: string;
  image: string;
  report_metas: IReportMetaResponse[];
  report_datas: IReportDataResponse[];
  report_category: number;
  description?: string;
  academic_period?: string;
  academic_period_id?: number;
}

export interface ICheckResponse {
  id?: number;
  date: Date;
  evaluated: IPersonResponse;
  evaluator: IPersonResponse;
  observation: string;
}

export interface IQuestionResponse {
  id?: number;
  type: Type;
  question: string;
  check_type: CheckType;
  answers: IAnswersChecks[];
}
